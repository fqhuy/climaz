# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 21:27:15 2013

@author: phan
"""

import matplotlib.pyplot as plt
import math
import numpy as np
import pylab as pl
import matplotlib as mpl

def visualize_gmm(gmm,X_train,rect):
    x = np.linspace(rect[0],rect[2])
    y = np.linspace(rect[1],rect[3])
    X, Y = np.meshgrid(x, y)
    XX = np.c_[X.ravel(), Y.ravel()]
    #Z = np.zeros_like(X)
    Z = np.log(-gmm.score_samples(XX)[0])
    Z = Z.reshape(X.shape)
    
    CS = pl.contour(X, Y, Z)
    CB = pl.colorbar(CS, shrink=0.8, extend='both')
    if X_train is not None:
        pl.scatter(X_train[:, 0], X_train[:, 1], .8)
    
    pl.axis('tight')
    pl.show()
    
def make_ellipses(gmm, ax):
    for n, color in enumerate('rgb'):
        v, w = np.linalg.eigh(gmm._get_covars()[n][:2, :2])
        u = w[0] / np.linalg.norm(w[0])
        angle = np.arctan2(u[1], u[0])
        angle = 180 * angle / np.pi  # convert to degrees
        v *= 9
        ell = mpl.patches.Ellipse(gmm.means_[n, :2], v[0], v[1],
                                  180 + angle, color=color)
        ell.set_clip_box(ax.bbox)
        ell.set_alpha(0.5)
        ax.add_artist(ell)
        
def visualize(textons,shape,figure, \
root='/home/phan/workspace/climaz/data/plots/'):
    if len(textons) < 10:
        cols = len(textons)
        rows = 1
    else: 
        cols = 10
        rows = len(textons) / cols
        if rows * cols < len(textons): rows += 1
            
    plt.ioff()
    fig = plt.figure(figure) 
    for r in range(rows):
        for c in range(cols):
            idx = r * cols + (c + 1)
            if idx <= len(textons):
                plt.subplot(rows,cols,idx)             # the first subplot in the first figure
                if shape[2] > 1:
                    plt.imshow(textons[idx-1].reshape(shape))
                else: 
                    plt.imshow(textons[idx-1].reshape(shape[0],shape[1]))

    #plt.title('Number of textons is %d' % len(textons))   # subplot 211 title
    plt.savefig(root + 'image_%s.jpg' % figure)
    plt.close(fig)
    
    