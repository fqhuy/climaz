#!/usr/bin/env python
# Copyright (c) 2008 Qtrac Ltd. All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 2 of the License, or
# version 3 of the License, or (at your option) any later version. It is
# provided for educational purposes and is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from future_builtins import *
from PIL import ImageQt
from Dataset import *
import numpy as np
import re
import functools
import random
import sys
import os.path 
from PyQt4.QtCore import (QByteArray, QDataStream, QFile, QFileInfo,
        QIODevice, QPoint, QPointF, QRectF, QString, Qt, SIGNAL, QRect)
from PyQt4.QtGui import (QApplication, QCursor, QDialog,
        QDialogButtonBox, QFileDialog, QFont, QFontComboBox,
        QFontMetrics, QGraphicsItem, QGraphicsPixmapItem,
        QGraphicsScene, QGraphicsTextItem, QGraphicsView, QGridLayout,
        QHBoxLayout, QLabel, QMatrix, QMenu, QMessageBox, QPainter,
        QPen, QPixmap, QPrintDialog, QPrinter, QPushButton, QSpinBox,
        QStyle, QTextEdit, QVBoxLayout, QListWidget,QBrush, QColor)

MAC = True
try:
    from PyQt4.QtGui import qt_mac_set_native_menubar
except ImportError:
    MAC = False

# A4 in points
#PageSize = (595, 842)
# US Letter in points
PageSize = (612, 792)
PAGESIZE_LST            = [612, 792]
POINT_SIZE              = 5
CURSOR_SIZE             = 6 * POINT_SIZE
BORDER_WIDTH            = 0 * POINT_SIZE

MagicNumber = 0x70616765
FileVersion = 1

Dirty = False

def Cursor_Pos(item):
    # Get mouse position relative to this item
    cursor_pos          = item.scene().parent().mapFromGlobal(QCursor.pos())
    cursor_pos          = item.scene().views()[0].mapToScene(cursor_pos)
    cursor_pos          = item.mapFromScene(cursor_pos)
    return cursor_pos
    
class BoxItem(QGraphicsItem):
    WIDTH               = 128 #128 * POINT_SIZE
    HEIGHT              = 128 #128 * POINT_SIZE
    RECT                = QRectF(0, 0, WIDTH, HEIGHT)
    CORNER_RADIUS       = 0 * POINT_SIZE
    FONT_SIZE           = 4 * POINT_SIZE
    BORDERRECT_PAD      = 1 * POINT_SIZE
    
    def __init__(self, position, scene, style=Qt.SolidLine,
                 rect=None, matrix=QMatrix()):
        super(BoxItem, self).__init__()
        self.setFlags(  self.flags()                    |
                        QGraphicsItem.ItemIsSelectable  |
                        QGraphicsItem.ItemIsMovable     |
                        QGraphicsItem.ItemIsFocusable   )
                        
        self.resize_sections= None
        self.resize_pos     = None
        if rect is None:
            self.rect       = QRectF(BoxItem.RECT)
        else:
            self.rect = QRectF(rect)
            
        self.handle = QPointF(rect.bottomRight())
        self.style = style
        self.setPos(position)
        self.setMatrix(matrix)
        scene.clearSelection()
        scene.addItem(self)
        global Dirty
        Dirty = True
        
    def setRect(self,rect):
        self.rect = QRectF(rect)

    def parentWidget(self):
        return self.scene().views()[0]


    def boundingRect(self):
        return self.rect.adjusted(-BORDER_WIDTH/2,-BORDER_WIDTH/2,BORDER_WIDTH/2,BORDER_WIDTH/2)

    def paint(self, painter, option, widget):
        pen = QPen(self.style)
        pen.setColor(Qt.yellow)
        pen.setWidth(0.5)
        painter.setPen(pen)
        #painter.drawRect(self.rect)
        '''
        outer_rect  = self.boundingRect().adjusted(-BoxItem.BORDERRECT_PAD, \
        -BoxItem.BORDERRECT_PAD, CURSOR_SIZE + BoxItem.BORDERRECT_PAD, \
        CURSOR_SIZE + BoxItem.BORDERRECT_PAD)
        
        inner_bound = (BORDER_WIDTH/2 + (BoxItem.CORNER_RADIUS/2)) # Get corner offset due to rounded corners
        inner_rect  = self.boundingRect().adjusted((CURSOR_SIZE + inner_bound) \
        + BoxItem.BORDERRECT_PAD, (CURSOR_SIZE + inner_bound) + BoxItem.BORDERRECT_PAD, \
        - inner_bound-BoxItem.BORDERRECT_PAD, -inner_bound-BoxItem.BORDERRECT_PAD)
        
        painter.drawRect(outer_rect)
        painter.drawRect(inner_rect)

        #brush   = QBrush(QColor(100, 100, 100, 100))
        painter.setBrush(brush) 
        '''
        painter.drawRoundedRect(self.rect, BoxItem.CORNER_RADIUS, BoxItem.CORNER_RADIUS)
        #brush   = QBrush(QColor(100, 100, 100, 100))
        #painter.setBrush(brush) 
        #painter.drawRect(self.handle.x(),self.handle.y(),2 * POINT_SIZE, 2 * POINT_SIZE)
        

    def itemChange(self, change, variant):
        if change != QGraphicsItem.ItemSelectedChange:
            global Dirty
            Dirty = True
        return QGraphicsItem.itemChange(self, change, variant)
        
    def Cursor_OnEdge(self):
        
        outer_rect  = self.boundingRect().adjusted(-BoxItem.BORDERRECT_PAD, \
        - BoxItem.BORDERRECT_PAD,CURSOR_SIZE + BoxItem.BORDERRECT_PAD, \
        CURSOR_SIZE + BoxItem.BORDERRECT_PAD)
        
        inner_bound = (BORDER_WIDTH/2 + (BoxItem.CORNER_RADIUS/2)) # Get corner offset due to rounded corners
        inner_rect  = self.boundingRect().adjusted((CURSOR_SIZE + inner_bound) \
        + BoxItem.BORDERRECT_PAD, (CURSOR_SIZE + inner_bound) + BoxItem.BORDERRECT_PAD, \
        - inner_bound - BoxItem.BORDERRECT_PAD, -inner_bound - BoxItem.BORDERRECT_PAD)

        cursor_pos  = Cursor_Pos(self)
        on_edge     = False
        if outer_rect.contains(cursor_pos) and not inner_rect.contains(cursor_pos):
            on_edge = True
        #area = QRectF(self.handle.x(),self.handle.y(),self.handle.x() + 2 * POINT_SIZE, \
        #self.handle.y() + 2 * POINT_SIZE)
        #if area.contains(cursor_pos):
        #    on_edge = True
        return on_edge


    def mousePressEvent(self, event):
        # check if user has clicked this object's border
        if self.Cursor_OnEdge():
            self.Setup_Resize() 
        super(BoxItem, self).mousePressEvent(event)


    def mouseMoveEvent(self, event):
        if self.resize_pos != None:
            self.Resize()
        else:       
            super(BoxItem, self).mouseMoveEvent(event)


    def mouseReleaseEvent(self, event):
        if self.resize_pos != None:
            self.Done_Resize()
        super(BoxItem, self).mouseReleaseEvent(event)


    def Setup_Resize(self):     
        self.resize_sections= []
        self.setCursor(Qt.ClosedHandCursor)
        cursor_pos          = Cursor_Pos(self)
        self.resize_pos     = cursor_pos
        # Determine drag from sections
        if cursor_pos.y() > self.boundingRect().height()/2:
            # If on bottom half
            self.resize_sections.append('BOTTOM')
        else:
            self.resize_sections.append('TOP')
        if cursor_pos.x() < self.boundingRect().width()/2:
            # If on left half
            self.resize_sections.append('LEFT')
        else:
            self.resize_sections.append('RIGHT')    


    def Resize(self):
        updated         = False
        # From cursor_pos to resize_pos
        cursor_pos      = Cursor_Pos(self)  
        move_x          = cursor_pos.x() - self.resize_pos.x()
        move_y          = cursor_pos.y() - self.resize_pos.y()
        child_rect      = self.childrenBoundingRect()
        # If y not within child items rect
        if not child_rect.contains(child_rect.left()+1, cursor_pos.y()):
            if self.resize_sections[0] == 'TOP':
                self.rect.setTop(self.rect.top() + move_y)  
            else:
                self.rect.setBottom(self.rect.bottom() + move_y)
            updated     = True
        # If x not within child items rect
        if not child_rect.contains(cursor_pos.x(), child_rect.top()+1):
            if self.resize_sections[1] == 'LEFT':
                self.rect.setLeft(self.rect.left() + move_x)    
            else:
                self.rect.setRight(self.rect.right() + move_x)
            updated     = True
        # Check if updated      
        if updated:
            self.scene().update()
            #self.handle = QPointF(self.rect.bottomRight())
            self.resize_pos = cursor_pos


    def Done_Resize(self):
   
        self.resize_pos = None
        self.setCursor(Qt.ArrowCursor)  
        
class GraphicsView(QGraphicsView):

    def __init__(self, parent=None):
        super(GraphicsView, self).__init__(parent)
        self.setDragMode(QGraphicsView.RubberBandDrag)
        self.setRenderHint(QPainter.Antialiasing)
        self.setRenderHint(QPainter.TextAntialiasing)
        self.setViewportUpdateMode(QGraphicsView.BoundingRectViewportUpdate)    


    def wheelEvent(self, event):
        factor = 1.41 ** (-event.delta() / 240.0)
        self.scale(factor, factor)

    #def mouseReleaseEvent(self, event):
    #    self.parent().setFocus()

        
class MainForm(QDialog):
    def __init__(self, parent=None):
        super(MainForm, self).__init__(parent)
        self.root = QString()
        self.save_root = QString()
        self.filename = QString()
        self.prevPoint = QPoint()
        self.addOffset = 5        
        self.view = GraphicsView()
        self.scene = QGraphicsScene(self)
        self.scene.setBackgroundBrush(QBrush(Qt.black,Qt.SolidPattern))
        self.scene.setSceneRect(0, 0, PageSize[0], PageSize[1])
        self.view.setScene(self.scene)
        self.saved = False
        
        self.classList = QListWidget()
        self.label1 = QLabel("Click Root to choose..")
        buttonLayout = QVBoxLayout()
        for text, slot in (
                ("Add &Box", self.addBox),
                ("&Next", self.nextPixmap),
                ("&Back", self.prevPixmap),
                ("&Open Root...", self.open),
                ("Open Save Roo&t...", self.chooseSaveRoot),
                ("&Save", self.save),
                ("Sa&ve Pos..", self.savePos),
                ("&Quit", self.accept)):
            button = QPushButton(text)
            if not MAC:
                button.setFocusPolicy(Qt.NoFocus)
            self.connect(button, SIGNAL("clicked()"), slot)
            #if text == "&Quit":
            #    buttonLayout.addStretch(1)
            buttonLayout.addWidget(button)
        
        buttonLayout.addWidget(self.label1)
        buttonLayout.addWidget(self.classList)
        self.connect(self.classList, SIGNAL("itemSelectionChanged()"), self.listChanged)
        buttonLayout.addStretch()

        layout = QHBoxLayout()
        layout.addWidget(self.view, 1)
        layout.addLayout(buttonLayout)
        self.setLayout(layout)
        
        self.boxes = []
        self.createPixmapItem(QPixmap(1,1),QPoint(0,0))
        for i in range(21):
            self.boxes.append(self.addBox())
        #self.boxes[0].setVisible(False)

        fm = QFontMetrics(self.font())
        self.resize(self.scene.width() + 50,
                    self.scene.height() + 50)
        self.setWindowTitle("ClipTool v0.1")
        
        
    def reject(self):
        self.accept()

    
    def accept(self):
        self.offerSave()
        QDialog.accept(self)


    def offerSave(self):
        if (Dirty and QMessageBox.question(self,
                            "Page Designer - Unsaved Changes",
                            "Save unsaved changes?",
                            QMessageBox.Yes|QMessageBox.No) ==
            QMessageBox.Yes):
            #self.save()
            pass
    
    def listChanged(self):
        if hasattr(self,'data'):
            if self.data[self.classList.currentRow()].curr_index() == 0:
                pixmap = self.data[self.classList.currentRow()].next()
                self.current_image.setPixmap(pixmap)
        
    def position(self):
        point = self.mapFromGlobal(QCursor.pos())
        if not self.view.geometry().contains(point):
            coord = 0 #random.randint(36, 144)
            point = QPoint(coord, coord)
        else:
            if point == self.prevPoint:
                point += QPoint(self.addOffset, self.addOffset)
                self.addOffset += 5
            else:
                self.addOffset = 5
                self.prevPoint = point
        return self.view.mapToScene(point)

    def addBox(self):
        #return BoxItem(self.position(), self.scene,rect = QRectF(0, 0, 100,100))
        return BoxItem(self.position(), self.scene,rect = QRectF(0, 0, BoxItem.WIDTH,BoxItem.HEIGHT))


    def nextPixmap(self):
        #if self.saved == False:
        #    self.save()
        #    self.saved = True
        pixmap = self.data[self.classList.currentRow()].next()
        url = self.data[self.classList.currentRow()].curr_url()
        url = url.replace(".pgm",".txt")
        f = open(url,"r")
        for idx,line in enumerate(list(f)[0:-1]):
            rec = [int(i) for i in line.replace("\n","").split(" ")]
            self.boxes[idx+1].rect = QRectF(0,0,BoxItem.WIDTH,BoxItem.HEIGHT)
            self.boxes[idx+1].setPos(rec[0],rec[1])
        self.boxes[0].rect = QRectF(0,0,pixmap.width(),pixmap.height())
        self.boxes[0].setPos(0,0)
        
        #r = self.boxes[0].rect
        #print(str(r.x()) + ' ' + str(r.y()) + ' ' + str(r.width()) + ' ' + str(r.height()))
        self.current_image.setPixmap(pixmap)
        '''
        path = self.data[self.classList.currentRow()].curr_url()+".txt"
        
        if os.path.isfile(path):
            rect = np.loadtxt(path)
            rect = QRectF(rect[0],rect[1],rect[2],rect[3])
            self.boxes[0].rect = rect
            self.scene.update()
        '''
        #self.createPixmapItem(QPixmap(fname), QPoint(0,0))

 
    def prevPixmap(self):
        #if self.saved == False:
        #    self.save()
        #    self.saved = True
            
        pixmap = self.data[self.classList.currentRow()].back()
        self.current_image.setPixmap(pixmap)            

    def createPixmapItem(self, pixmap, position, matrix=QMatrix()):
        item = QGraphicsPixmapItem(pixmap)
        #item.setFlags(QGraphicsItem.ItemIsSelectable|
        #              QGraphicsItem.ItemIsMovable)
        item.setPos(position)
        item.setMatrix(matrix)
        self.scene.clearSelection()
        self.scene.addItem(item)
        #item.setSelected(True)
        self.current_image = item
        global Dirty
        Dirty = True


    def selectedItem(self):
        items = self.scene.selectedItems()
        if len(items) == 1:
            return items[0]
        return None
        
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Left:
            self.prevPixmap()
        elif event.key() == Qt.Key_Right:
            self.nextPixmap()
        elif event.key() == Qt.Key_Up:
            if self.classList.currentRow() < self.classList.count() - 1:
                self.classList.setCurrentRow(self.classList.currentRow()+1)
        elif event.key() == Qt.Key_Down:
            if self.classList.currentRow() > 0:
                self.classList.setCurrentRow(self.classList.currentRow()-1)
        elif event.key() == Qt.Key_Enter:
            self.save()
        elif event.key() == Qt.Key_Space:
            self.save()
            self.nextPixmap()

        global Dirty
        Dirty = True
        
    def savePos(self):
        
        if os.path.isdir(self.save_root):
            root = str(self.save_root + self.classList.currentItem().text())
        tf =  open(root + "/image_" + str(self.data[self.classList.currentRow()].curr_index()) \
            + ".txt",'a')
        #text = tf.readlines()
        text = ""
        for idx, box in enumerate(self.boxes):
            p = box.pos()
            rect = QRect(p.x(), p.y(), box.rect.width(), \
            box.rect.height())
            tf.write(str(rect.x()) + ' ' + str(rect.y()) + \
            ' ' + str(rect.x() + rect.width()) + ' ' + str(rect.y() + rect.height()) + '\n'
            )
            #text = text + str(rect.x()) + ' ' + str(rect.y()) + \
            #' ' + str(rect.x() + rect.width()) + ' ' + str(rect.y() + rect.height())
        #tf.seek(0)
        #tf.write(text)
        tf.close()
        self.saved = True            
        '''
        import shutil
        for (cdx, c) in enumerate(self.data):
            root = str(self.save_root + c)
            for (udx, url) in enumerate(self.data[cdx].urls):
                shutil.copyfile(url, root + "/image_" + str(udx) + ".jpg")
        '''    
        #box = self.boxes[0]
        #p = box.pos()
        #rect = np.array([p.x(), p.y(), box.rect.width(), box.rect.height()],dtype=np.int)
        #np.savetxt(self.data[self.classList.currentRow()].curr_url()+".txt",rect)
        
        
    def save(self):
        
        if os.path.isdir(self.save_root):
            root = str(self.save_root + self.classList.currentItem().text())
            if not os.path.exists(root):
                os.makedirs(root)
            url = self.data[self.classList.currentRow()].curr_url()
            prefix = url.split("/")[-1].replace(".jpg","")
            tf =  open(root + "/"+ prefix + ".txt",'w')
            for idx, box in enumerate(self.boxes):
                
                p = box.pos()
                '''
                if p.x() < 0:
                    p.setX(0)
                if p.y() < 0:
                    p.setY(0)
                    
                if (-p.x() + self.boxes[0].rect.width() - BoxItem.WIDTH) < 0:
                    p.setX(self.boxes[0].rect.width() - BoxItem.WIDTH)
                    
                if (-p.y() + self.boxes[0].rect.height() - BoxItem.HEIGHT) < 0:
                    p.setY(self.boxes[0].rect.height() - BoxItem.HEIGHT)
                '''    
                rect = QRect(p.x(), p.y(), box.rect.width(), \
                box.rect.height())
                tf.write(str(rect.x()) + ' ' + str(rect.y()) + \
                ' ' + str(rect.x() + rect.width()) + ' ' + str(rect.y() + rect.height()) + '\n'
                )
                img = self.current_image.pixmap().copy(rect)

                img_file = prefix + "_" + str(idx) + ".jpg"
                #f = QFile(root + "/image_" + str(self.data[self.classList.currentRow()].curr_index()) \
                #+ "_" + str(idx) + ".jpg")
                f = QFile(root + "/" + img_file)
                f.open(QIODevice.WriteOnly);
                img.save(f,"JPG")
            tf.close()
        self.saved = True
        

            
    def open(self):
        #self.offerSave()
        path = (QFileInfo(self.filename).path()
                if not self.filename.isEmpty() else ".")
        root = QFileDialog.getExistingDirectory(self, "CropTool v0.1", \
        path, QFileDialog.ShowDirsOnly)
        if root.isEmpty():
            return
        self.root = root + "/"
        classes = [d for d in os.listdir(self.root) \
        if os.path.isdir(self.root + d) and d != 'output']
            
        if classes is not None:
            self.classList.addItems(classes)
            self.classList.setCurrentRow(0)
            
        image_pattern = re.compile(r'image_([0-9]+).pgm')
        sort_key = lambda img: int(image_pattern.match(img).group(1))
            
        repo = DirRepo(str(self.root),classes,image_pattern, sort_key=sort_key)
        retrv = QtImageRetriever()
        ds = Dataset(classes,None,repo,retrv,True)
        #pixmap = ds[0].next()
        #self.current_image.setPixmap(pixmap)
        self.data = ds
        self.nextPixmap()
        
    def chooseSaveRoot(self):
        if self.save_root == '':
            path = "."
            fname = QFileDialog.getExistingDirectory(self,
                    "CropTool v0.1 - Choose Root", path,  QFileDialog.ShowDirsOnly)
            if fname.isEmpty():
                return
            self.save_root = fname + "/"
            self.label1.setText(fname)


app = QApplication(sys.argv)
form = MainForm()
rect = QApplication.desktop().availableGeometry()
form.resize(int(rect.width() * 0.6), int(rect.height() * 0.9))
form.show()
app.exec_()