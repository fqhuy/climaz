# -*- coding: utf-8 -*-
"""
Created on Sun Sep 22 11:32:47 2013

@author: phan
"""
import gensim

class LDA(gensim.models.ldamodel.LdaModel):
    """ A modified LDA model for image categorization.
                 |-----------------------------|
    (eta)--------|-->(c)                     L |
                 |    |    |----------------|  |
    |---------|  |    |    |              N |  |  |-----------|  
    |C (theta)|--|--(pi)---|-->(z)--->(x)<--|--|--|--(beta) K |
    |---------|  |         |----------------|  |  |-----------|
                 |-----------------------------|
    """