# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 22:08:13 2013

@author: phan
"""
from os.path import join
import os


def configuration(parent_package='', top_path=None):
    from numpy.distutils.misc_util import Configuration

    config = Configuration('climaz', parent_package, top_path)

    config.add_subpackage('viz')
    config.add_subpackage('features')
    config.add_subpackage('fast')
    config.add_subpackage('tests')

    return config

if __name__ == "__main__":
    from numpy.distutils.core import setup

    config = configuration(top_path='').todict()
    setup(**config)