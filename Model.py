# -*- coding: utf-8 -*-
from Dataset import *
import numpy as np
from features import *
import sys
import logging as lgg
from sklearn import neighbors
from fast import histogram as h

class ClassifierModel(object):
    def __init__(self,classes, feature_extractors=[],color_mode='L'):
        self.n_classes = len(classes)
        self.classes = classes
        self.n_samples_ = dict(zip(classes,[1] * self.n_classes))
        self.feature_extractors = feature_extractors
        self.color_mode = color_mode
    
    def test(self,dataset):
        result = []
        scores = np.zeros((dataset.n_classes,dataset.shape[0],dataset.n_classes))
        for cx,c in enumerate(self.classes):
            lgg.info('running test on class %s' % c)
            right = 0
            scrs = []
            for tx,t in enumerate(dataset[c]):
                s =  'image ' + dataset.repo[c][tx][-35:]
                cc, scr = self.classify(t)
                scrs.append(scr)
                if cc == cx: 
                    right += 1
                    s += ' - OK'
                else:
                    s += ' - FAILED - ' + self.classes[cc]
                #print s
                lgg.info(s)
            if np.array(scrs).shape == scores[cx].shape:
                scores[cx] = np.array(scrs)
            
            lgg.info("correct classification: " + str(right))
            #results.append('%d/%d' %(dataset.n_samples[c],right))
            result.append(right)
        accuracy = sum(result) / float(sum(dataset.n_samples.values()))

        return result, accuracy, scores
        
    def fit(self, dataset):
        raise NotImplementedError()
        
    def classify(self,image):
        raise NotImplementedError()
    
    def _extract_features(self,image):
        features = []
        for fx in self.feature_extractors:
            features.append(fx.extract(image))
        try:
            features_ = np.hstack(features)
        except ValueError:
            #print "Features aren't stackable"
            lgg.error("Features aren't stackable")
            features_ = features[0]
        return features_
        
    def dim(self):
        if len(self.feature_extractors) == 1:
            return self.feature_extractors[0].size() \
            * color_dict.n_channels(self.color_mode)
            
        return reduce(lambda x,y: x.size() * y.size(), \
        self.feature_extractors) * color_dict.n_channels(self.color_mode) 
        
    def hist_classify(self,image):
        """
        classify a new image using the internal statistics
        """
        if isinstance(self.n_samples, int):
            hnew = self.calc_histogram(image)
      
            n_neighbors = self.n_classes
            #mahhatan distance perform best. (p = 1)
            clf = neighbors.KNeighborsClassifier(n_neighbors, \
            weights='distance', algorithm='auto', p = 1)
            X = np.asfarray(self.histogram_).reshape( \
            (self.n_classes * self.n_samples,self.n_textons))
            y = np.array(range(self.n_classes)).repeat(self.n_samples)
            clf.fit(X, y)
            Z = clf.predict(np.array([hnew]))
            
            return Z[0], []
        else:
            raise ValueError("n_samples must be an int")
        
    def calc_histogram(self,image,index=(-1,-1)):
        """
        annotate an image with the dictionary 
        then generate a histogram of textons
        index: is to used internally 
        """
        if index == (-1,-1):
            extracted = self._extract_features(image)
        else:
            extracted = self._extracted_data[index[0]][index[1]]

        #limage: to keep the texton labels.
        #TODO: can be done with K-nearest Neighbour ?
        '''
        limage = np.zeros(reduce(lambda x,y: x*y, image.shape))
        
        for fx,f in enumerate(extracted):
            mx = sys.maxint
            ix = 0
            for idx, texton in enumerate(self.dict_):
                norm = np.linalg.norm(f - texton,1)
                if mx > norm:
                    mx = norm
                    ix = idx + 1
            limage[fx] = ix
        
        hist,_ = np.histogram(limage,range(1, self.n_textons + 2 ),density=True)
        '''
        #lgg.info("processing image...%s" % str(np.linalg.norm(hist1 - hist)))
        return h.calc_histogram(extracted,np.array(self.dict_),1)
        #return hist
        
    def load(self):
        raise NotImplementedError()
        
    def save(self):
        raise NotImplementedError()
        
    def save_texton_as_image(self):
        for dx,d in enumerate(self.dict_):
            result = Image.fromarray((d * 255).astype(np.uint8).reshape(9,9))
            result.save("texton_%d.bmp" % dx)
        
        