# -*- coding: utf-8 -*-
"""
Created on Sun Mar 23 09:23:19 2014

@author: phan
"""
import numpy as np
from skimage.io import imread, imsave
from matplotlib import pyplot as plt
from features import IFV
import os
from os.path import join
from skimage.draw import line
from sklearn.decomposition import PCA
import cv2
from skimage.transform import pyramid_reduce
#from skimage.io import imsave
def drawBoxC(im, x, y, w, h, color=[0,255,0],thickness=5):
    def line_(x1,y1,x2,y2):
        rr, cc = line(y1, x1, y2, x2)
        im[rr,cc] = color
    #TOP
    for t in range(thickness):
        line_(x, y + t, x + w, y + t)
        #BOTTOM
        line_(x, y + h - t, x + w, y + h - t)
        #LEFT
        line_(x + t, y, x + t, y + h)
        #RIGHT
        line_(x + w - t, y, x + w - t, y + h)
  
def drawBox(im, x, y, w, h):
    #TOP
    im[y, x: x + w] = 0
    im[y-1, x: x + w] = 255
    im[y+1, x: x + w] = 255
    #BOTTOM
    im[y + h, x: x + w] = 0
    im[y + h -1, x: x + w] = 255
    im[y + h +1, x: x + w] = 255
    #LEFT
    im[y: y + h, x] = 0
    im[y: y + h, x - 1] = 255
    im[y: y + h, x + 1] = 255
    #RIGHT
    im[y: y + h, x + w] = 0
    im[y: y + h, x + w - 1] = 255
    im[y: y + h, x + w + 1] = 255

def line2nums_t(text):
    return tuple([float(num) for num in text.split(' ')])
    
def line2nums_l(text):
    return [float(num) for num in text.split(' ')]

def extract_ifv(filename):
    f = open(filename, 'r')
    line = f.readline()
    ifvs = []
    while line:
        if "1 3200" in line:
            line = f.readline()
            nums = [float(n) for n in line.split(' ')]
            ifvs.append(nums)
        else:   
            line = f.readline()
    return np.vstack(ifvs)
    
def extract_hog(filename):
    import numpy as np
    f = open(filename, 'r')
    line = f.readline()
    hogs = []
    while line:
        if "8 8 32" in line:
            filt = np.zeros((8,8,32),dtype='float64')
            for i in range(8):
                line = f.readline()
                nums = [float(n) for n in line.split(' ')]
                filt[i] = np.asarray(nums).reshape(8,32)
            hogs.append(filt)
        else:   
            line = f.readline()
    f.close()
    return hogs

class Model:
    def __init__(self, n_parts, n_texs, bias):
        self.n_parts = n_parts;
        self.n_texs = n_texs
        self.bias = bias
        
class Mixture:
    def __init__(self, filename, name,  has_tex = True, has_shape = True):
        if not has_tex and not has_shape:
            return
            
        f = open(filename, 'r')
        self.n_components = int(f.readline())
        self.models = []
        self.name = name
        self.has_tex = has_tex
        self.has_shape = has_shape
        
        for i in range(self.n_components):
            n_parts, bias = line2nums_t(f.readline())
            n_parts = int(n_parts)
            filters = []
            
            if has_shape:
                for p in range(n_parts):
                    py, px, feat_length = tuple(line2nums_l(f.readline())[0:3])
                    py = int(py)
                    filt = np.zeros((py, px, feat_length),dtype='f')
                    for y in range(py):
                        filt[y, :] = np.asarray(line2nums_l(f.readline())).reshape(px, feat_length)
                    filters.append(filt)

            n_texs = 0    
            texs = []
            if has_tex:
                n_texs, tex_length = line2nums_t(f.readline())
                n_texs = int(n_texs)
                for t in range(n_texs):
                    tex = np.asarray(line2nums_l(f.readline()))
                    texs.append(tex)
            f.readline()
            m = Model(n_parts, n_texs, bias)
            m.filters = filters
            m.texs = texs
            self.models.append(m)
            
    def extract(self, root, filename, gmm, pos, scale, scores):
        oroot = root + '_OUTPUT/' + filename
        mr8_image = np.load(oroot + '_l0.npy')
        mr8_image = mr8_image.reshape(1000, 1000, 8)
        padding = 1
        #scale = round(mr8_image.shape[0] / float(hog_image.shape[0] - 2 * padding))
        feature = []
        mr8s = []            
        
        for mx, model in enumerate(self.models):
            if self.has_shape:
                for fx, filt in enumerate(model.filters):
                    patch_size = filt.shape[0]
                    best = pos[int(mx)][fx]
                    by = (best[0] - padding) * scale
                    if by < 0:
                        by = 0
                    bx = (best[1] - padding) * scale
                    if bx < 0:
                        bx = 0
                    ps = patch_size * scale
                    patch = mr8_image[by: by + ps, bx: bx + ps]
                    patch = patch.reshape(patch.shape[0] * patch.shape[1], patch.shape[2])
                    mr8s.append( patch )
            else:
                for fx in range(model.n_parts):
                    #patch_size = filt.shape[0]
                    patch_size = 8
                    
                    best = pos[int(mx)][fx]
                    by = (best[0] - padding) * scale
                    if by < 0:
                        by = 0
                    bx = (best[1] - padding) * scale
                    if bx < 0:
                        bx = 0
                    ps = patch_size * scale
                    patch = mr8_image[by: by + ps, bx: bx + ps]
                    patch = patch.reshape(patch.shape[0] * patch.shape[1], patch.shape[2])
                    mr8s.append( patch )                
        mr8s = np.vstack(mr8s)
        feature.append( IFV.extract_feature_ifv_once(mr8s, gmm) )
        feature = np.concatenate(feature)                  
        return feature
                    
    def detect(self, root, filename, viz = True, extract_feat = False, gmm=None):
        if not self.has_shape and not self.has_tex:
            return None
            
        saveroot = root + '_OUTPUT/'
        oroot = root + '_OUTPUT/' + filename
        iroot = root + '/' + filename
        allposs = []
        for level in range(0,1):
            hog_image = np.load(oroot + '_l%d_hog_2048.npy' % level)
            ifv_image = np.load(oroot + '_l%d_ifv_65.npy' % level)
            
            if extract_feat:
                mr8_image = np.load(oroot + '_l%d.npy' % level)
                sz = np.sqrt ( sum(mr8_image.shape) / 8 )
                mr8_image = mr8_image.reshape(sz, sz, 8)
                padding = 1
                scale = round(mr8_image.shape[0] / float(hog_image.shape[0] - 2 * padding))
                feature = []
                mr8s = []  
            
            allscores = []
            allpos = []
    
            for model in self.models:
                positions = []
                scores = []
                scoremap = np.zeros((ifv_image.shape[0:2]),dtype='f')
                for tex in model.texs:
                    for row in range(ifv_image.shape[0]):
                        for col in range(ifv_image.shape[1]):
                            #m[row,col] = ifv.dot(ifv_image[row,col].transpose()).sum()
                            scoremap[row,col] += 1 * tex.dot(ifv_image[row,col].flatten())
                if viz:
                    plt.imsave(oroot + '_scoremap_ifv.png', scoremap)
                    
                if self.has_shape:
                    for filt in model.filters:
                        newscoremap = scoremap.copy()
                        if viz: 
                            hogscoremap = np.zeros((ifv_image.shape[0:2]),dtype='f')
                            
                        assert(filt.shape[0] == filt.shape[1])
                        patch_size = filt.shape[0]
                        filt = filt.flatten()
                        
                        for row in range(hog_image.shape[0] - patch_size):
                            for col in range(hog_image.shape[1] - patch_size):
                                newscoremap[row,col] += 1 * filt.dot(hog_image[row : row +  patch_size, col : col + patch_size].flatten())
                                if viz:
                                    hogscoremap[row,col] += 1 * filt.dot(hog_image[row : row +  patch_size, col : col + patch_size].flatten())
                        
                        #if viz:
                        #    plt.imsave(oroot + '_scoremap_hog.png', hogscoremap)
                        #    plt.imsave(oroot + '_scoremap_combined.png', newscoremap)
                            
                        best = np.unravel_index(newscoremap.argmax(),newscoremap.shape)
                        positions.append(best)
                        scores.append(newscoremap.max())
                        
                else:
                    for tex in model.texs:
                        best = np.unravel_index(scoremap.argmax(),scoremap.shape)
                        positions.append(best)
                        scores.append(scoremap.max())
                    
                allscores.append(np.array(scores))
                allpos.append(positions)
            
            
            #scores = np.load(saveroot + '%s/test_image_%d_%s_results.npz' % (self.name, i, classes[mx]))['arr_0']
            #allpos = np.load(saveroot + '%s/test_image_%d_%s_results.npz' % (test, i, classes[mx]))['arr_2']
            
            maxScore = -999 
            scores = []
            for model, m in enumerate(self.models):
                total = sum(allscores[model]) + m.bias
                #totalScore += total
                scores.append((total, model))
                
                if total > maxScore:
                    maxScore = total
                    argMax = model
                    argPos = allpos[model]
                
            #sorted_scores = sorted(scores)
            #maxScore = np.sum(sorted_scores[-1:])
            if extract_feat:
                for scr, modl in sorted_scores[-5:]:
                    best = allpos[modl][0]
                    #feature.append( hog_image[best[0] : best[0] +  patch_size, best[1] : best[1] + patch_size].flatten() )
                    #feature.append( ifv_image[best[0] , best[1]].flatten() )
                    
                    by = (best[0] - padding) * scale
                    if by < 0:
                        by = 0
                    bx = (best[1] - padding) * scale
                    if bx < 0:
                        bx = 0
                    ps = patch_size * scale
                    patch = mr8_image[by: by + ps, bx: bx + ps]
                    patch = patch.reshape(patch.shape[0] * patch.shape[1], patch.shape[2])
                    mr8s.append( patch )
    
                mr8s = np.vstack(mr8s)
                feature = IFV.extract_feature_ifv_once(mr8s, gmm)
                #feature.append( IFV.extract_feature_ifv_once(mr8s, gmm))
                #feature = np.concatenate(feature) 
    
            if viz:
                padding = 1
                image = imread(iroot + '.jpg')
                scale = image.shape[0] / (ifv_image.shape[0] - 2 * padding)
                if self.has_shape:
                    s = self.models[0].filters[0].shape[0]
                else:
                    s = 8
                    
                for model, m in enumerate(self.models):
                    for part in allpos[model]:
                        drawBoxC(image, (part[1] - padding) *  scale, (part[0] - padding) * scale, s * scale, s * scale)
                #fig = pyplot.figure(1)
                #pyplot.imsave(oroot + '.png', image)
                imsave(oroot + '_detected.png', image)
                #fig.save(oroot + '.png')
                #pyplot.show()
            
        if extract_feat:        
            return scores, maxScore, allpos, feature
        else:
            return scores, maxScore, allpos #totalScore
    
    def visualize(self):
        image = imread(iroot + '.pgm')
        scale = image.shape[0] / ifv_image.shape[0]
        s = self.models[0].filters[0].shape[0]
        for part in argPos:
            drawBox(image, part[1] * scale, part[0] * scale, s* scale, s * scale)
        pyplot.imshow(image)
        pyplot.show()
        
def test(class_name, mixture_file, split):
    print 'processing class ' + class_name
    m = Mixture(mixture_file, has_tex=True)
    scores = []
    for i in range(split[0],split[0] + split[1]):
        _, maxScore, totalScore = m.detect(root,'%s/image_%d' % (test, i), False, False, gmm)
        scores.append(maxScore < 0)
        #print 'image %d :' % i + str(maxScore)
    return sum(scores)
    
def worker(N, C, classes, test_classes, gmm, root, saveroot):
    #test_classes = ['denim','corduroy', 'leather', 'boucle', 'fur', 'knit1', 'lace', 'fleece']
    #denim - 20, leather 17, lace 15, corduroy 7, boucle 17, fur 14, knit1 16, fleece 13
    #        15          16       10           8         17      16        11         11
    zclasses = dict(zip(classes, range(8)))
    irange = (0, 60)
    mixtures = []    
    for cc in classes:
        m = Mixture('/home/phan/workspace/climaz/climaz/dpm/ffld/build/selected_models/%s_model_N%s_C%s.txt'  \
        % ( cc , N , C), name = cc, has_tex=True, has_shape=True)
        mixtures.append(m)
        
    padding = 1    
    scale = round(1000 / float(65 - 2 * padding))
    for test in test_classes:
        scores = np.zeros((8, irange[1] - irange[0]),dtype='float64')
        posss = []
        #allMixScores = []
        #output = saveroot + 'test_' + test + '_bestM_0_60.npz'
        output = saveroot + 'test_' + test + '_bestM_N%s_C%s.npz' % (N, C)
        #if not os.path.isfile(output):
        for mx, m in enumerate(mixtures):
            print 'fitting mixture ' + classes[mx]
            poss = []
            for i in range(irange[0], irange[1]):
                #if not os.path.isfile(saveroot + '%s/image_%d_%s_test_results.npz' % (test, i, classes[mx])):
                mixScores, maxScore, allPos = m.detect(root,'%s/image_%d' % (test, i), False, False, gmm)
                np.savez(saveroot + '%s/image_%d_%s_test_results.npz' % (test, i, classes[mx]), mixScores, maxScore, allPos)
                
                #else:
                #    data = np.load(saveroot + '%s/image_%d_%s_test_results.npz' % (test, i, classes[mx]))
                #    mixScores = data['arr_0'] 
                #    maxScore = data['arr_1']
                #    allPos = data['arr_2']
                        
                scores[mx, i - irange[0]] = maxScore
                poss.append(allPos)
            posss.append(poss)
            
        bestM = scores.argmax(axis = 0)
        np.savez(output,bestM,posss,scores)
        #else:
        #    bestM = np.load(output)['arr_0']
            #bestM = [zclasses[test]] * (irange[1] - irange[0]) #[[i] * 35 for i in range(8) ]
            #bestM = np.array(bestM,dtype='int32')
            #posss = np.load(output)['arr_1']
            
        print 'best mixtures ' + str(bestM)
        #feats = []
        for i in range(irange[0], irange[1]):
            print 'extracting features. image ' + str(i)
            fname = saveroot + '%s/image_%d' % (test, i) + '_features_N%s_C%s_Standard.npy' % (N, C)
            #if os.path.isfile(fname):
            #    continue
            
            mixScores = np.load(saveroot + '%s/image_%d_%s_test_results.npz' % \
            (test, i, classes[bestM[i - irange[0]]]))['arr_0']
            
            feat = mixtures[bestM[i - irange[0]]].extract(root,'%s/image_%d' % (test, i), \
            gmm, posss[bestM[i - irange[0]]][i - irange[0]], scale, mixScores )
            #feats.append(feat)
            #feat = np.concatenate([feat.flatten(), scores[:, i - irange[0]]])
            #feat =  scores[:, i - irange[0]]
            np.save(fname, feat.flatten())
            #np.save(saveroot + '%s/image_%d' % (test, i) + '_features_best.npy', feat.flatten())
        
        #pca = PCA(n_components = 1600)
        #pca.fit_transform(feats)
            
def worker_categorize(N, C, classes, test_classes, gmm, root, saveroot):
    #C = '1000'
    #N = 25
    zclasses = dict(zip(classes, range(8)))
    irange = (0, 60)
    mixtures = []    
    for cc in classes:
        m = Mixture('/home/phan/workspace/climaz/climaz/dpm/ffld/build/%s_model_N%s_C%s.txt'  \
        % ( cc , N , C), name = cc, has_tex=True, has_shape=True)
        mixtures.append(m)    
    accuracies = []   
    for test in test_classes:
        scores = np.zeros((8, irange[1] - irange[0]),dtype='float64')
        posss = []
        #output = saveroot + 'test_' + test + '_bestM_0_60.npz'
        output = saveroot + 'test_' + test + '_bestM_N%s_C%s.npz' % (N, C)
        if not os.path.isfile(output):
            for mx, m in enumerate(mixtures):
                print 'fitting mixture ' + classes[mx]
                poss = []
                for i in range(irange[0], irange[1]):
                    pos, maxScore, _ = m.detect(root,'%s/image_%d' % (test, i), False, False, gmm)
                    scores[mx, i - irange[0]] = maxScore
                    poss.append(pos)
                posss.append(poss)
                
            bestM = scores.argmax(axis = 0)
            np.savez(output,bestM,posss,scores)
        else:
            bestM = np.load(output)['arr_0']
            posss = np.load(output)['arr_1']
        
        trueM = np.array([zclasses[test]] * (irange[1] - irange[0]),dtype='int32')
        accuracy = (bestM == trueM).sum() / float(len(trueM))
        accuracies.append(accuracy)
        
def detect_only(root, classes, test_classes, gmm):
    oroot = root + '_OUTPUT_IMG/'
    iroot = root + '/'
    #test = 'fur'
    #iid = 42
    N = 35
    C = 1
    irange = (N, 60)
    
    mixtures = []    
    for cc in classes:
        m = Mixture('/home/phan/workspace/climaz/climaz/dpm/ffld/build/selected_models/%s_model_N%s_C%s.txt'  \
        % ( cc , N , C), name = cc, has_tex=True, has_shape=True)
        mixtures.append(m)
        
    #m = Mixture('/home/phan/workspace/climaz/climaz/dpm/ffld/build/selected_models/%s_model_N%s_C%s.txt'  \
    #m = Mixture('/home/phan/workspace/climaz/climaz/dpm/ffld/build/models_IFV/%s_model_N%s_C%s.txt'  \
    #    % ( test , N , C), has_tex=True, name = test, has_shape=False)
    for test in test_classes:
        scores = np.zeros((8, irange[1] - irange[0]),dtype='float64')
        posss = []
        for mx, m in enumerate(mixtures):
            print 'fitting mixture ' + classes[mx]
            poss = []
            for i in range(irange[0], irange[1]):
                mixScores, maxScore, allPos = m.detect(iroot[:-1],'%s/image_%d' % (test, i), False, False, gmm)
                scores[mx, i - irange[0]] = maxScore
                poss.append(allPos)
            posss.append(poss)

        bestM = scores.argmax(axis = 0)
        
        for iid in range(irange[0],irange[1]):
            img = '%s/image_%d' % (test, iid)
            image = imread(iroot + img + '.jpg')
        
            txt = open(iroot + img + '.txt', 'r')
            txt.readline()
            for line in txt.readlines():
                rect = [int(i) for i in line.split(' ')]
                if rect[0] + 128 > 999:
                    rect[0] = 999 - 128
                if rect[1] + 128 > 999:
                    rect[1] = 999 - 128
                    
                drawBoxC(image, rect[0], rect[1], 128, 128)
                
            image = pyramid_reduce(image, 4)
            imsave(oroot + img + '_manual.png', image)
            txt.close()
            
            pos = posss[bestM[iid - irange[0]]][iid - irange[0]]
            image = imread(iroot + img + '.jpg')
            padding = 1            
            scale = 1000 / (65 - 2 * padding)            
            
            for rect in pos:
                x, y = rect[0]
                y =  (y - padding) * scale
                x =  (x - padding) * scale
                if x + 128 > 999:
                    x = 999 - 128
                if y + 128 > 999:
                    y = 999 - 128
                    
                drawBoxC(image,y, x, 128, 128)
                #drawBoxC(image, x, y, 128, 128)
                
            image = pyramid_reduce(image, 4)
            imsave(oroot + img + '_machine.jpg', image)
        
def sift_detect(classes, test_classes, gmm, root, saveroot):
    zclasses = dict(zip(classes, range(8)))
    irange = (0, 60)
    sift = cv2.SIFT(20, 5)
    for test in test_classes:
        
        for i in range(irange[0], irange[1]):
            output = saveroot + '%s/image_%d_features_SIFT.npy' % (test, i)
            #if os.path.isfile(output):
            #    continue
            
            img = imread(root + '/%s/image_%d.pgm' % (test, i)) 
            mr8_image = np.load(saveroot + '%s/image_%d_l0.npy' % (test, i) )
            mr8_image = mr8_image.reshape(1000, 1000, 8)
            img = np.asarray(pyramid_reduce(img, 4) * 255, dtype='uint8')
            kps = sift.detect(img)
            mr8s = []
            for kp in kps:
                x, y = kp.pt
                s = kp.size
                x, y, s = int(x*4), int(y*4), int(s*4)
                patch = mr8_image[y - s: y + s, x - s : x + s]
                if patch.shape == (s * 2,  s * 2, 8):
                    patch = patch.reshape(s * 2 * s * 2, 8)
                    mr8s.append(patch)
                    
            if len(mr8s) > 0:
                mr8s = np.vstack(mr8s)
                #feature.append( IFV.extract_feature_ifv_once(mr8s, gmm) )
                #feature = np.concatenate(feature)   
                feat = IFV.extract_feature_ifv_once(mr8s, gmm)
            else:
                feat = np.zeros(3200, dtype='f')
            np.save(output, feat.flatten())
            print 'extracting features. image ' + str(i)     
            
def objectness_detect(classes, test_classes, gmm, root, saveroot):
    zclasses = dict(zip(classes, range(8)))
    irange = (0, 60)
    for test in test_classes:
        for i in range(irange[0], irange[1]):
            output = saveroot + '%s/image_%d_features_obj.npy' % (test, i)
            
            img = imread(root + '/%s/image_%d.pgm' % (test, i)) 
            mr8_image = np.load(saveroot + '%s/image_%d_l0.npy' % (test, i) )
            mr8_image = mr8_image.reshape(1000, 1000, 8)

            txt = open(root + '/%s/image_%d_obj.txt' % (test, i), 'r')
            txt.readline()
            mr8s = []
            for line in txt.readlines():
                rect = [float(j) for j in line.split(' ')]
                rect = np.asarray(rect, dtype='int32')
                patch = mr8_image[rect[1] : rect[3], rect[0] : rect[2]]
                patch = patch.reshape(-1, 8)
                mr8s.append(patch)
                                    
            if len(mr8s) > 0:
                mr8s = np.vstack(mr8s)
                feat = IFV.extract_feature_ifv_once(mr8s, gmm)
            else:
                feat = np.zeros(3200, dtype='f')
            np.save(output, feat.flatten())
            print 'extracting features. image ' + str(i)

def dense_detect(classes, test_classes, gmm, root, saveroot):
    zclasses = dict(zip(classes, range(8)))
    irange = (0, 60)
    for test in test_classes:
        for i in range(irange[0], irange[1]):
            output = saveroot + '%s/image_%d_features_dense.npy' % (test, i)
            mr8_image = np.load(saveroot + '%s/image_%d_l0.npy' % (test, i) )
            feat = IFV.extract_feature_ifv_once(mr8_image[0::2], gmm)

            np.save(output, feat.flatten())
            print 'extracting features. image ' + str(i)    

def plot_all_detection(path,classes):
    rng = (35,60)
    ncols = 5
    latex = \
    '''
    \\begin{longtable}
    \\begin{tabular}{ll|ll}
    \\hline
    Our & Manual & Our & Manual \\\\
    \\hline
    '''
    
    for cl in classes:
        page = 0
        for i in range(rng[0],rng[1]):
            img1 = join(path, cl,'image_%d_machine.jpg' % i)
            img2 = join(path, cl,'image_%d_manual.png' % i)
            
            if (i - rng[0]) % 2 == 0:
                latex += '\\\\\n'
            else:
                latex += ' & '

            latex += '\\includegraphics[scale=0.32]{%s}' % img1
            latex += ' & \\includegraphics[scale=0.32]{%s}' % img2
                
    latex += \
    '''
    \\hline 
    \\end{tabular}
    \\end{longtable}
    '''
    fl = open('/home/phan/Dropbox/climaz/ACCV2014/supp/plot_detections.tex','w')
    fl.write(latex)
    fl.close()
    
    '''
    command = 'pdfjoin'
    for cl in classes:
        page = 0
        fig = plt.figure()
        fig.suptitle('Detection Result vs Ground Truth (%s Page %d)' % (cl, page))
        for i in range(rng[0],rng[1]):
            img1 = imread(join(path, cl,'image_%d_machine.jpg' % i))
            img2 = imread(join(path, cl,'image_%d_manual.png' % i))
            newpage =  (i - rng[0]) % (ncols * 2) == 0
            if (newpage and i > rng[0]):
                fig = plt.figure()
                fig.suptitle('Detection Result vs Ground Truth (%s Page %d)' % (cl, page + 1))
                page += 1
                
            ax1 = fig.add_subplot(ncols, 4,(i - page * (ncols * 2) - rng[0]) * 2 + 1)
            ax1.axis('off')
            ax1.imshow(img1)
            ax2 = fig.add_subplot(ncols, 4,(i - page * (ncols * 2) - rng[0]) * 2 + 2)
            ax2.axis('off')
            ax2.imshow(img2)

            if ((i - rng[0] + 1) % (ncols * 2) == 0 and i > rng[0]) or (i == rng[1] - 1):
                fig.tight_layout(pad=0.1, w_pad=0.1, h_pad=0.1)
                fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
                fig.savefig(join(path,'detections_class_%s_page_%d.pdf' % (cl,page)))
                command += ' ' + join(path,'detections_class_%s_page_%d.pdf' % (cl,page))
    import subprocess
    subprocess.call(command,shell=True)
    '''
            
if __name__ == '__main__':
    import multiprocessing
    root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b'
    saveroot = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b_OUTPUT/'
    classes = ['fleece', 'fur', 'corduroy', 'denim','knit1', 'leather', 'boucle', 'lace']
    test_classes = [['fleece', 'fur'], ['corduroy', 'denim'],['knit1', 'leather'], ['boucle', 'lace']]

    gmm = {}    
    N = 35
    C = '1'
    gmm_data = np.load(saveroot + 'gmm_all.npz')
    gmm['weights'] = gmm_data['arr_0']
    gmm['means'] = gmm_data['arr_1']
    gmm['covars'] = gmm_data['arr_2']
    gmm['ncoms'] = 200
    jobs = []
    #detect_only(root)
    #for N in [15, 25, 35]:
    #    for C in ['1', '0.5']:
    #detect_only(root,classes,test_classes[0],gmm)
    '''
    for test in test_classes:
        #p = multiprocessing.Process(target=worker_categorize,args=(N, C, classes, test, gmm, root,saveroot))
        #p = multiprocessing.Process(target=worker,args=(N, C, classes, test, gmm, root,saveroot))
        #p = multiprocessing.Process(target=sift_detect,args=( classes, test, gmm, root,saveroot))
        #p = multiprocessing.Process(target=objectness_detect,args=(classes, test, gmm, root,saveroot))
        #p = multiprocessing.Process(target=dense_detect,args=(classes, test, gmm, root,saveroot))
        p = multiprocessing.Process(target=detect_only,args=(root,classes,test,gmm))
        #sift_detect( classes, test, gmm, root,saveroot)
        #worker(N, C, classes, test, gmm, root,saveroot)
        jobs.append(p)
        p.start()
    '''
    plot_all_detection(saveroot[:-1] + '_IMG',classes)
    #for job in jobs:
    #    job.join()
    