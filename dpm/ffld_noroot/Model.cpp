//--------------------------------------------------------------------------------------------------
// Implementation of the papers "Exact Acceleration of Linear Object Detectors", 12th European
// Conference on Computer Vision, 2012 and "Deformable Part Models with Individual Part Scaling",
// 24th British Machine Vision Conference, 2013.
//
// Copyright (c) 2013 Idiap Research Institute, <http://www.idiap.ch/>
// Written by Charles Dubout <charles.dubout@idiap.ch>
//
// This file is part of FFLDv2 (the Fast Fourier Linear Detector version 2)
//
// FFLDv2 is free software: you can redistribute it and/or modify it under the terms of the GNU
// Affero General Public License version 3 as published by the Free Software Foundation.
//
// FFLDv2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with FFLDv2. If
// not, see <http://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------------------------------

#include "Model.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <limits>
#include <iostream>
#include "Common.h"
#include "cnpy/cnpy.h"
#include "IFV.h"
#include "JPEGImage.h"

using namespace Eigen;
using namespace FFLD;
using namespace std;

//int Model::bi;


void drawx(JPEGImage & image, const Rectangle & rect, uint8_t r, uint8_t g, uint8_t b, int linewidth)
{
    if (image.empty() || rect.empty() || (image.depth() < 3))
        return;

    const int width = image.width();
    const int height = image.height();
    const int depth = image.depth();
    uint8_t * bits = image.bits();

    // Draw 2 horizontal lines
    const int top = min(max(rect.top(), 1), height - linewidth - 1);
    const int bottom = min(max(rect.bottom(), 1), height - linewidth - 1);

    for (int x = max(rect.left() - 1, 0); x <= min(rect.right() + linewidth, width - 1); ++x) {
        if ((x != max(rect.left() - 1, 0)) && (x != min(rect.right() + linewidth, width - 1))) {
            for (int i = 0; i < linewidth; ++i) {
                bits[((top + i) * width + x) * depth    ] = r;
                bits[((top + i) * width + x) * depth + 1] = g;
                bits[((top + i) * width + x) * depth + 2] = b;
                bits[((bottom + i) * width + x) * depth    ] = r;
                bits[((bottom + i) * width + x) * depth + 1] = g;
                bits[((bottom + i) * width + x) * depth + 2] = b;
            }
        }

        // Draw a white line below and above the line
        if ((bits[((top - 1) * width + x) * depth    ] != 255) &&
                (bits[((top - 1) * width + x) * depth + 1] != 255) &&
                (bits[((top - 1) * width + x) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[((top - 1) * width + x) * depth + i] = 255;

        if ((bits[((top + linewidth) * width + x) * depth    ] != 255) &&
                (bits[((top + linewidth) * width + x) * depth + 1] != 255) &&
                (bits[((top + linewidth) * width + x) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[((top + linewidth) * width + x) * depth + i] = 255;

        if ((bits[((bottom - 1) * width + x) * depth    ] != 255) &&
                (bits[((bottom - 1) * width + x) * depth + 1] != 255) &&
                (bits[((bottom - 1) * width + x) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[((bottom - 1) * width + x) * depth + i] = 255;

        if ((bits[((bottom + linewidth) * width + x) * depth    ] != 255) &&
                (bits[((bottom + linewidth) * width + x) * depth + 1] != 255) &&
                (bits[((bottom + linewidth) * width + x) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[((bottom + linewidth) * width + x) * depth + i] = 255;
    }

    // Draw 2 vertical lines
    const int left = min(max(rect.left(), 1), width - linewidth - 1);
    const int right = min(max(rect.right(), 1), width - linewidth - 1);

    for (int y = max(rect.top() - 1, 0); y <= min(rect.bottom() + linewidth, height - 1); ++y) {
        if ((y != max(rect.top() - 1, 0)) && (y != min(rect.bottom() + linewidth, height - 1))) {
            for (int i = 0; i < linewidth; ++i) {
                bits[(y * width + left + i) * depth    ] = r;
                bits[(y * width + left + i) * depth + 1] = g;
                bits[(y * width + left + i) * depth + 2] = b;
                bits[(y * width + right + i) * depth    ] = r;
                bits[(y * width + right + i) * depth + 1] = g;
                bits[(y * width + right + i) * depth + 2] = b;
            }
        }

        // Draw a white line left and right the line
        if ((bits[(y * width + left - 1) * depth    ] != 255) &&
                (bits[(y * width + left - 1) * depth + 1] != 255) &&
                (bits[(y * width + left - 1) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[(y * width + left - 1) * depth + i] = 255;

        if ((bits[(y * width + left + linewidth) * depth    ] != 255) &&
                (bits[(y * width + left + linewidth) * depth + 1] != 255) &&
                (bits[(y * width + left + linewidth) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[(y * width + left + linewidth) * depth + i] = 255;

        if ((bits[(y * width + right - 1) * depth    ] != 255) &&
                (bits[(y * width + right - 1) * depth + 1] != 255) &&
                (bits[(y * width + right - 1) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[(y * width + right - 1) * depth + i] = 255;

        if ((bits[(y * width + right + linewidth) * depth    ] != 255) &&
                (bits[(y * width + right + linewidth) * depth + 1] != 255) &&
                (bits[(y * width + right + linewidth) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[(y * width + right + linewidth) * depth + i] = 255;
    }
}

Model::Model(): parts_(1), bias_(bi),nbTexs_(0)
{

}

/*
Model::Model(const string& name) : parts_(1), bias_(10.0),nbTexs_(0),name_(name)
{
    //parts_[0].offset.setZero();
    //parts_[0].deformation.setZero();
}
*/
Model::Model( int nbTexs, int nbParts,  pair<int, int> partSize) : parts_(1), bias_(bi)
{
    // Create an empty model if any of the given parameters is invalid
    if ((nbParts < 0) || (nbParts && ((partSize.first <= 0) || (partSize.second <= 0)))) {
        cerr << "Attempting to create an empty model" << endl;
        return;
    }

    parts_.resize(nbParts);

    nbTexs_ = nbTexs;

    ifvs_.resize(nbTexs);
    for(int i = 0; i < nbTexs; ++i)
        ifvs_[i] = FeaturePyramid::Cell::Zero();

    for (int i = 0; i < nbParts; ++i) {
        parts_[i].filter = HOGPyramid::Level::Constant(partSize.first, partSize.second, HOGPyramid::Cell::Zero());
    }

}

Model::Model(const vector<Part> & parts,const std::vector<FeaturePyramid::Cell> & texFeatures, double bias) : ifvs_(texFeatures), parts_(parts), bias_(bias)
{
    // Create an empty model if the parts are empty
    if (parts.empty()) {
        parts_.resize(1);
        //parts_[0].offset.setZero();
        //parts_[0].deformation.setZero();
    }
}

bool Model::empty() const
{
    return !parts_[0].filter.size() && (parts_.size() == 1);
}

const vector<Model::Part> & Model::parts() const
{
    return parts_;
}

vector<Model::Part> & Model::parts()
{
    return parts_;
}

double Model::bias() const
{
    return bias_;
}

double & Model::bias()
{
    return bias_;
}

pair<int, int> Model::rootSize() const
{
    return pair<int, int>(static_cast<int>(parts_[0].filter.rows()),
                          static_cast<int>(parts_[0].filter.cols()));
}

pair<int, int> Model::partSize() const
{
    if (parts_.size() > 1)
        return pair<int, int>(static_cast<int>(parts_[1].filter.rows()),
                              static_cast<int>(parts_[1].filter.cols()));
    else
        return pair<int, int>(0, 0);
}

void Model::initializeParts(const std::vector<HOGPyramid::Level> & parts, pair< int, int > partSize, const std::vector<FeaturePyramid::Cell>& texs) {
    const int nbParts = parts.size();

    parts_.resize(nbParts);
    for(int i = 0; i < nbParts; ++i) {
        //parts_[i] = parts[i];
        parts_[i].filter = parts[i];
    }

#ifndef NO_TEXTURE
    nbTexs_ = texs.size();

    ifvs_.resize(texs.size());
    for (int i = 0; i < texs.size(); i++) {
        ifvs_[i] = texs[i];
    }
#endif
}

void Model::initializeSample(const Object& obj,const vector<double>& scores, const HOGPyramid & pyramid, const FeaturePyramid & fpyramid, int z, Model & sample,
                             const vector<vector<Position> > * positions) const
{
    // All the constants relative to the model and the pyramid
    const int nbFilters = static_cast<int>(parts_.size());
    const int nbParts = nbFilters;
    const int padx = pyramid.padx();
    const int pady = pyramid.pady();
    const int interval = pyramid.interval();
    const int nbLevels = static_cast<int>(pyramid.levels().size());

    // Invalid parameters
    if (empty() || (z < 0) || (z >= nbLevels) || (nbParts && (!positions || (positions->size() != nbParts)))) {
        sample = Model();
        cerr << "Attempting to initialize an empty sample" << endl;
        return;
    }

    // Resize the sample to have the same number of filters as the model
    sample.parts_.resize(nbFilters);
    for (int i = 0; i < nbParts; ++i) {


        // Position of the part
        if ((z >= (*positions)[i].size())) {
            sample = Model();
            cerr << "Attempting to initialize an empty sample" << endl;
            return;
        }

        const Position position = (*positions)[i][z];

        // Level of the part
        if ((position(2) < 0) || (position(2) >= nbLevels) ) {
            sample = Model();
            cerr << "Attempting to initialize an empty sample" << endl;
            return;
        }
        const HOGPyramid::Level & level = pyramid.levels()[position(2)];

        if ((position(0) < 0) || (position(1) < 0) ||
                (position(0) + partSize().second > level.cols()) ||
                (position(1) + partSize().first > level.rows())) {
            sample = Model();
            cerr << "Attempting to initialize an empty sample. Invalid part positions!" << endl;
            return;
        }

        // Extract the part filter
        sample.parts_[i].filter = level.block(position(1), position(0), partSize().first,
                                              partSize().second);
    }

#ifndef NO_TEXTURE

    string root = fpyramid.root();
    if(!obj.empty()) {
        int pos = root.find_last_of("/\\");
        string file = "/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/" + Object::NameMap[ obj.name() ] + "/" + root.substr(pos, root.length());
        //int from = root.find_first_of("_OUTPUT");
        //int to = root.find_first_of("image_");
        //string file = root.substr(0, from) + root.substr(to, root.size()-1);
        JPEGImage image = JPEGImage(file + ".jpg");
        image = image.rescale(0.50);
        const double scale1 = 500 / pyramid.levels()[z].cols();//pow(2.0, -static_cast<double>(z) / interval);

        for(int i = 0; i < nbParts; ++i) {
            const Position position = (*positions)[i][z];
            const int x = position(0) * scale1;
            const int y = position(1) * scale1;
            Rectangle rec(x,y,8 * scale1, 8 * scale1);
            drawx(image, rec, 0, 0 , 255, 1);
        }
        image.save( root + "_detection.jpg");
    }
    //Use the features collected from highly scored regiions to build the IFV vector.
    
    string mr8_file = fpyramid.root() + "_l0.npy";
    cnpy::NpyArray mr8 =  cnpy::npy_load(mr8_file);
    float* data = (float *) mr8.data;
    IFV::MR8s  vec = IFV::MR8s::Constant(1000, 1000, IFV::MR8::Zero());

    for(int i = 0; i < 1000; ++i) {
        for(int j = 0; j < 1000; ++j) {
            for(int k = 0; k < 8; ++k) {
                vec(i, j)(k) = data[i* 1000 * 8 + j * 8 + k];
            }
        }
    }
    mr8.destruct();
    const double scale = 1000 / pyramid.levels()[z].cols();//pow(2.0, -static_cast<double>(z) / interval);

    vector<IFV::MR8s> patches;
    for(int i = 0; i < nbParts; ++i) {
        if(scores[i + 1] > 1.0e-5) {
            const Position position = (*positions)[i][z];
            //Just discard out-of-screen regions
            const int x = position(0) * scale;
            const int y = position(1) * scale;

            const int boundx = x + 128 > 1000 ? x + 128 - 1000: 128;
            const int boundy = y + 128 > 1000 ? y + 128 - 1000: 128;
            const IFV::MR8s p =  vec.block(y, x, boundy, boundx);
            patches.push_back(p);
        } else {
            cout << "skip part, score = " << scores[i + 1] << endl;
        }
    }

    FeaturePyramid::Cell feat;
    IFV::Instance.encode(patches, feat);
    /*
    //FeaturePyramid::Cell feat;
    cnpy::NpyArray arr =  cnpy::npy_load(root + "_allparts_ifv.npy");
    float* data = (float*) arr.data;
    //for(int i = 0; i < 3200; ++i){
    Eigen::Map<FeaturePyramid::Cell> feat(data);
    //}
    */
    for(int i = 0; i < ifvs_.size(); ++i)
        sample.ifvs_.push_back(feat);

#endif

    sample.bias_ = 1.0;
}

//New convolution
void Model::convolve(const HOGPyramid & pyramid, const FeaturePyramid & fpyramid, vector<vector<double> > & scores,
                     vector<vector<Position> > * positions,
                     vector<vector<HOGPyramid::Matrix> > * convolutions) const
{
    // Invalid parameters
    if (empty() || pyramid.empty() || (convolutions && (convolutions->size() != parts_.size()))) {
        scores.clear();
        if (positions)
            positions->clear();

        return;
    }

    // All the constants relative to the model and the pyramid
    const int nbFilters = static_cast<int>(parts_.size());
    const int nbParts = nbFilters;
    const int padx = pyramid.padx();
    const int pady = pyramid.pady();
    const int interval = pyramid.interval();
    const int nbLevels = static_cast<int>(pyramid.levels().size());

    // Convolve the pyramid with all the filters
    vector<vector<HOGPyramid::Matrix> > tmpConvolutions;

    if (convolutions) { //IF PATCHWORK IS WORKING
        for (int i = 0; i < nbFilters; ++i) {
            if ((*convolutions)[i].size() != nbLevels) {
                scores.clear();

                if (positions)
                    positions->clear();

                return;
            }
        }
    }
    else { //OR NOT
        tmpConvolutions.resize(nbFilters);
        #pragma omp parallel for
        for (int i = 0; i < nbFilters; ++i)
            pyramid.convolve(parts_[i].filter, tmpConvolutions[i]);

        convolutions = &tmpConvolutions;
    }
    // Resize the positions
    if (positions) {
        positions->resize(nbParts);

        for (int i = 0; i < nbParts; ++i)
            (*positions)[i].resize(nbLevels);
    }

    //convolve the ifv pyramid with the "filter"
    int maxConv = 0;
	vector<HOGPyramid::Matrix> tmpx;
	
    if(!fpyramid.empty()) {
#ifndef NO_TEXTURE

        //#pragma omp parallel for
        for (int j = 0; j < ifvs_.size(); ++j)
            fpyramid.convolve(ifvs_[j], tmpx);

        if(ifvs_.size() > 0)
            #pragma omp parallel for
            ///ADD SCORE FROM THE TEXTURE COMPONENTS

            for (int i = 0; i < nbParts; ++i)
                for (int z = 0; z < nbLevels; ++z)
                    for (int y = 0; y < (*convolutions)[i][z].rows(); ++y)
                        for (int x = 0; x < (*convolutions)[i][z].cols(); ++x) {
                            const float tm = 3 * tmpx[z](y,x);

                            //const float cnv = (*convolutions)[i][z](y,x);
                            //if(maxConv < cnv)
                            //	maxConv = cnv;
							//if((*convolutions)[i][z](y,x) > 1.0e-4)
                            (*convolutions)[i][z](y,x) += tm; //TODO: Should have a suitable weight here.
                        }
#endif
    }
    scores.resize(nbLevels);
    //Find best positions for parts
    for (int z = 0; z < nbLevels; ++z) {
        scores[z].resize(nbParts + 1);
        scores[z][0] = 0;
        for (int i = 0; i < nbParts; ++i) {

            //if (positions)
            //   (*positions)[i][z] = 0;

            int maxX = -1;
            int maxY = -1;
            double maxScore = -999;
            for (int y = 0; y < (*convolutions)[i][z].rows(); ++y) {
                for (int x = 0; x < (*convolutions)[i][z].cols(); ++x) {
                    if( (*convolutions)[i][z](y,x) > maxScore)
                    {
                        maxX = x;
                        maxY = y;
                        maxScore = (*convolutions)[i][z](y,x);
                    }
                }
            }
            scores[z][0] += maxScore ;//+ 1 * tmpx[z](maxY, maxX);
            scores[z][i + 1] = maxScore;
            (*positions)[i][z] << maxY, maxX, z;
        }

    }

    if (bias_) {
        #pragma omp parallel for
        for (int i = interval; i < nbLevels; ++i)
            scores[i][0] += bias_;
    }
}

double Model::dot(const Model & sample) const

{

    double d = bias_ * sample.bias_;

    if (parts_.size() != sample.parts_.size())
        return numeric_limits<double>::quiet_NaN();

    for (int i = 0; i < parts_.size(); ++i) {
        if ((parts_[i].filter.rows() != sample.parts_[i].filter.rows()) ||
                (parts_[i].filter.cols() != sample.parts_[i].filter.cols()))
            return numeric_limits<double>::quiet_NaN();

        for (int y = 0; y < parts_[i].filter.rows(); ++y)
            d += HOGPyramid::Map(parts_[i].filter).row(y).dot(
                     HOGPyramid::Map(sample.parts_[i].filter).row(y));

        //if (i)
        //    d += parts_[i].deformation.dot(sample.parts_[i].deformation);
    }
#ifndef NO_TEXTURE
    //if(ifvs_.size() > 0 && sample.ifvs_.size() > 0)
    for (int i = 0; i < ifvs_.size(); ++i) {
        d+= ifvs_[i].dot( sample.ifvs_[i] );
    }
#endif
    return d;
}

double Model::norm() const
{

    double n = 0.0;

    for (int i = 0; i < parts_.size(); ++i) {
        n += HOGPyramid::Map(parts_[i].filter).squaredNorm();


        //if (i)
        //    n += 10.0 * parts_[i].deformation.squaredNorm();
    }
#ifndef NO_TEXTURE
    //TODO: How many times of texture ?
    for (int i = 0; i < ifvs_.size(); ++i) {
        n += 20.0 * ifvs_[i].squaredNorm();
    }
#endif
    return sqrt(n);
}

Model & Model::operator+=(const Model & sample)
{
    if (parts_.size() != sample.parts_.size())
        return *this;

    Model copy(*this);

    for (int i = 0; i < parts_.size(); ++i) {
        if ((parts_[i].filter.rows() != sample.parts_[i].filter.rows()) ||
                (parts_[i].filter.cols() != sample.parts_[i].filter.cols())) {
            *this = copy; // Restore the copy
            return *this;
        }

        parts_[i].filter += sample.parts_[i].filter;
        //parts_[i].deformation += sample.parts_[i].deformation;
    }

#ifndef NO_TEXTURE
    for (int i = 0; i < ifvs_.size(); ++i) {
        ifvs_[i] += sample.ifvs_[i];
    }
#endif
    bias_ += sample.bias_;

    return *this;
}

Model & Model::operator-=(const Model & sample)
{
    if (parts_.size() != sample.parts_.size())
        return *this;

    Model copy(*this);

    for (int i = 0; i < parts_.size(); ++i) {
        if ((parts_[i].filter.rows() != sample.parts_[i].filter.rows()) ||
                (parts_[i].filter.cols() != sample.parts_[i].filter.cols())) {
            *this = copy; // Restore the copy
            return *this;
        }

        parts_[i].filter -= sample.parts_[i].filter;
        //parts_[i].deformation -= sample.parts_[i].deformation;
    }
#ifndef NO_TEXTURE
    for (int i = 0; i < ifvs_.size(); ++i) {
        ifvs_[i] -= sample.ifvs_[i];
    }
#endif
    bias_ -= sample.bias_;

    return *this;
}

Model & Model::operator*=(double a)
{
    for (int i = 0; i < parts_.size(); ++i) {
        HOGPyramid::Map(parts_[i].filter) *= a;
        //parts_[i].deformation *= a;
    }
#ifndef NO_TEXTURE
    for (int i = 0; i < ifvs_.size(); ++i) {
        ifvs_[i] *= a;
    }
#endif
    bias_ *= a;

    return *this;
}

Model Model::flip() const
{
    Model model;

    if (!empty()) {
        model.parts_.resize(parts_.size());

        // Flip the root
        model.parts_[0].filter = HOGPyramid::Flip(parts_[0].filter);
        //model.parts_[0].offset = parts_[0].offset;
        //model.parts_[0].deformation = parts_[0].deformation;

        // Flip the parts
        for (int i = 1; i < parts_.size(); ++i) {
            //model.parts_[i].filter = HOGPyramid::Level::Constant(parts_[i].filter.rows(), parts_[i].filter.cols(), HOGPyramid::Cell::Zero());
            model.parts_[i].filter = HOGPyramid::Flip(parts_[i].filter);
            //model.parts_[i].offset(0) = 2 * static_cast<int>(parts_[0].filter.cols()) -
            //                            static_cast<int>(parts_[i].filter.cols()) -
            //                            parts_[i].offset(0);
            //model.parts_[i].offset(1) = parts_[i].offset(1);
            //model.parts_[i].offset(2) = parts_[i].offset(2);
            //model.parts_[i].deformation = parts_[i].deformation;
            //model.parts_[i].deformation(1) = -model.parts_[i].deformation(1);
        }
    }

    model.bias_ = bias_;

#ifndef NO_TEXTURE
    model.ifvs_.resize(ifvs_.size());
    for(int i = 0; i < ifvs_.size(); ++i) {
        model.ifvs_[i] = ifvs_[i];
    }
#endif

    model.name_ = name_;
    return model;
}

template <typename Scalar>
static void dt1d(const Scalar * x, int n, Scalar a, Scalar b, Scalar * z, int * v, Scalar * y,
                 int * m, const Scalar * t, int incx, int incy, int incm)
{
    assert(x && (y || m));
    assert(n > 0);
    assert(a < 0);
    assert(z && v);
    assert(t);
    assert(incx && incy && (m ? incm : true));

    z[0] =-numeric_limits<Scalar>::infinity();
    z[1] = numeric_limits<Scalar>::infinity();
    v[0] = 0;

    // Use a lookup table to replace the division by (a * (i - v[k]))
    int k = 0;
    Scalar xvk = x[0];

    for (int i = 1; i < n;) {
        const Scalar s = (x[i * incx] - xvk) * t[i - v[k]] + (i + v[k]) - b / a;

        if (s <= z[k]) {
            --k;
            xvk = x[v[k] * incx];
        }
        else {
            ++k;
            v[k] = i;
            z[k] = s;
            xvk = x[i * incx];
            ++i;
        }
    }

    z[k + 1] = numeric_limits<Scalar>::infinity();

    if (y && m) {
        for (int i = 0, k = 0; i < n; ++i) {
            while (z[k + 1] < 2 * i)
                ++k;

            y[i * incy] = x[v[k] * incx] + (a * (i - v[k]) + b) * (i - v[k]);
            m[i * incm] = v[k];
        }
    }
    else if (y) {
        for (int i = 0, k = 0; i < n; ++i) {
            while (z[k + 1] < 2 * i)
                ++k;

            y[i * incy] = x[v[k] * incx] + (a * (i - v[k]) + b) * (i - v[k]);
        }
    }
    else {
        for (int i = 0, k = 0; i < n; ++i) {
            while (z[k + 1] < 2 * i)
                ++k;

            m[i * incm] = v[k];
        }
    }
}


void Model::DT2D(HOGPyramid::Matrix & matrix, const Part & part, HOGPyramid::Matrix & tmp,
                 Positions * positions)
{
    // Nothing to do if the matrix is empty
    if (!matrix.size())
        return;

    const int rows = static_cast<int>(matrix.rows());
    const int cols = static_cast<int>(matrix.cols());

    if (positions)
        positions->resize(rows, cols);

    tmp.resize(rows, cols);

    // Temporary vectors
    vector<HOGPyramid::Scalar> z(max(rows, cols) + 1);
    vector<int> v(max(rows, cols) + 1);
    vector<HOGPyramid::Scalar> t(max(rows, cols));

    t[0] = numeric_limits<HOGPyramid::Scalar>::infinity();

    for (int x = 1; x < cols; ++x)
        t[x] = 1 / (part.deformation(0) * x);

    // Filter the rows in tmp
    for (int y = 0; y < rows; ++y)
        dt1d<HOGPyramid::Scalar>(matrix.row(y).data(), cols, part.deformation(0),
                                 part.deformation(1), &z[0], &v[0], tmp.row(y).data(),
                                 positions ? positions->row(y).data()->data() : 0, &t[0], 1, 1, 3);

    for (int y = 1; y < rows; ++y)
        t[y] = 1 / (part.deformation(2) * y);

    // Filter the columns back to the original matrix
    for (int x = 0; x < cols; ++x)
        dt1d<HOGPyramid::Scalar>(tmp.data() + x, rows, part.deformation(2), part.deformation(3),
                                 &z[0], &v[0],
#ifndef FFLD_MODEL_3D
                                 matrix.data() + x,
#else
                                 0,
#endif
                                 positions ? ((positions->data() + x)->data() + 1) : 0, &t[0], cols,
                                 cols, 3 * cols);

    // Re-index the best x positions now that the best y changed
    if (positions) {
        for (int y = 0; y < rows; ++y)
            for (int x = 0; x < cols; ++x)
                tmp(y, x) = (*positions)(y, x)(0);

        for (int y = 0; y < rows; ++y)
            for (int x = 0; x < cols; ++x)
                (*positions)(y, x)(0) = tmp((*positions)(y, x)(1), x);
    }
}

ostream & FFLD::operator<<(ostream & os, const Model & model)
{
    // Save the number of parts and the bias
    os << model.parts().size() << ' ' << model.bias() << endl;

    // Save the parts themselves
    for (int i = 0; i < model.parts().size(); ++i) {
        os << model.parts()[i].filter.rows() << ' ' << model.parts()[i].filter.cols() << ' '
           << HOGPyramid::NbFeatures
           /*<< ' ' << model.parts()[i].offset(0) << ' '
           << model.parts()[i].offset(1) << ' ' << model.parts()[i].offset(2) << ' '
           << model.parts()[i].deformation(0) << ' ' << model.parts()[i].deformation(1) << ' '
           << model.parts()[i].deformation(2) << ' ' << model.parts()[i].deformation(3) << ' '
           << model.parts()[i].deformation(4) << ' ' << model.parts()[i].deformation(5)
           */
           << endl;

        for (int y = 0; y < model.parts()[i].filter.rows(); ++y) {
            os << model.parts()[i].filter(y, 0)(0);

            for (int j = 1; j < HOGPyramid::NbFeatures; ++j)
                os << ' ' << model.parts()[i].filter(y, 0)(j);

            for (int x = 1; x < model.parts()[i].filter.cols(); ++x)
                for (int j = 0; j < HOGPyramid::NbFeatures; ++j)
                    os << ' ' << model.parts()[i].filter(y, x)(j);

            os << endl;
        }
    }
#ifndef NO_TEXTURE
    os << model.ifvs().size() << ' ' << FeaturePyramid::NbFeatures << endl;

    for( int i = 0; i < model.ifvs().size(); i++) {
        os << model.ifvs()[i](0);
        for( int j = 1; j < FeaturePyramid::NbFeatures; j++) {
            os << ' ' << model.ifvs()[i](j);
        }
        os << endl;
    }
#endif
    return os;
}

istream & FFLD::operator>>(istream & is, Model & model)
{
    int nbParts;
    double bias;

    is >> nbParts >> bias;

    if (!is) {
        cerr << "Failed to load model" << endl;
        model = Model();
        return is;
    }

    vector<Model::Part> parts(nbParts);

    for (int i = 0; i < nbParts; ++i) {
        int rows, cols, nbFeatures;

        is >> rows >> cols >> nbFeatures;
        /*>> parts[i].offset(0) >> parts[i].offset(1)
        >> parts[i].offset(2) >> parts[i].deformation(0) >> parts[i].deformation(1)
        >> parts[i].deformation(2) >> parts[i].deformation(3) >> parts[i].deformation(4)
        >> parts[i].deformation(5); */

        if (!is || (nbFeatures > HOGPyramid::NbFeatures)) {
            cerr << "Failed to load model" << endl;
            model = Model();
            return is;
        }

        // Always set the offset and deformation of the root to zero
        //if (!i) {
        //    parts[0].offset.setZero();
        //    parts[0].deformation.setZero();
        //}
        // Always set the z offset of a part to zero
        //else {
        //    parts[i].offset(2) = 0;
        //}

        parts[i].filter = HOGPyramid::Level::Constant(rows, cols, HOGPyramid::Cell::Zero());

        for (int y = 0; y < rows; ++y) {
            for (int x = 0; x < cols; ++x) {
                for (int j = 0; j < nbFeatures; ++j)
                    is >> parts[i].filter(y, x)(j);

                // Always put the truncation feature at the end
                if (nbFeatures < HOGPyramid::NbFeatures)
                    swap(parts[i].filter(y, x)(nbFeatures - 1),
                         parts[i].filter(y, x)(HOGPyramid::NbFeatures - 1));
            }
        }

        if (!is) {
            cerr << "Failed to load model" << endl;
            model = Model();
            return is;
        }
    }
    std::vector<FeaturePyramid::Cell> ifvs;
#ifndef NO_TEXTURE
    int nbTexs = 0;
    int nbIFV = 0;
    is >> nbTexs >> nbIFV;
    ifvs.resize(nbTexs);

    for (int i = 0; i < nbTexs ; ++i) {
        /*
        for( int y = 0; y < 400; ++y) {
            for( int x = 0; x < 64; ++x) {
                is >> ifvs[i](y, x)(j);
            }
        }
        */
        for(int j = 0; j < nbIFV; ++j) {
            is >> ifvs[i](j);
        }
    }
#endif
    model = Model(parts,ifvs, bias);
    return is;
}


