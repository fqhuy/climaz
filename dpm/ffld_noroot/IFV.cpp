#include "IFV.h"
#include "fisher.h"
#include "gmm.h"
#include "cnpy/cnpy.h"

using namespace Eigen;
using namespace FFLD;
using namespace std;

//IFV::IFV(vector< FeaturePyramid::Level > patches)
//{

//}

IFV IFV::Instance;
//int IFV::NbFeatures;

IFV::IFV(const string& filename, const string& name)
{
    const string gmm_file = filename + "/gmm_all.npz";
    cnpy::npz_t gmmdata =  cnpy::npz_load(gmm_file);
    //weights means covars
    gmm_.priors = Map<Matrix<float, NbClusters, 1> >((float*)gmmdata["arr_0"].data);
    gmm_.means = Map<Matrix<float, NbClusters, 8> >((float*)gmmdata["arr_1"].data);
    gmm_.covars = Map<Matrix<float, NbClusters, 8> >((float*)gmmdata["arr_2"].data);
}

void IFV::convolve(vector< FeaturePyramid::Level > patches)
{

}

void IFV::encode(const vector< IFV::MR8s >& patches, FeaturePyramid::Cell& vec)
{
    int nbFeatures = 8;
	vec = FeaturePyramid::Cell::Zero();
	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> X;
	
	int length = 0;
	for(int i = 0; i < patches.size(); ++i){
		length += patches[i].cols() * patches[i].rows();	
	}
	
	//Make up a long feature fector from all patches
	X = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>(length, nbFeatures);
	
    for(int i = 0; i < patches.size(); ++i) {
		for(int y = 0; y < patches[i].rows(); ++y){
			for(int x = 0; x < patches[i].cols(); ++x){
				for(int j = 0; j < nbFeatures; ++j)
					X(i * patches[i].rows() * patches[i].cols() +  y * patches[i].cols()   + x , j) = patches[i](y,x)(j);
			}
		}

    }
    vl_fisher_encode(vec.data(),VL_TYPE_FLOAT, gmm_.means.data(),
					 nbFeatures, NbClusters, gmm_.covars.data(),
					 gmm_.priors.data(),X.data(),
					 X.rows() ,VL_FISHER_FLAG_IMPROVED);
}
