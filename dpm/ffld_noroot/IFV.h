#ifndef IFV_H
#define IFV_H

#include <iostream>
#include <vector>
#include <Eigen/Core>
#include "FeaturePyramid.h"

namespace FFLD
{
class IFV {
public:
    static const int NbFeatures = 8 ;
    static const int NbClusters = 200;
    static const int NbClustersX2 = 400;
	static IFV Instance;
	
    typedef Eigen::Matrix<float, NbClustersX2, NbFeatures, Eigen::RowMajor> Data;
	typedef Eigen::Matrix<float, 8, 1> MR8;
	typedef Eigen::Matrix<MR8, Eigen::Dynamic, Eigen::Dynamic> MR8s;
	
	typedef struct _GMM {
		Eigen::Matrix<float, NbClusters, NbFeatures> means;
		Eigen::Matrix<float, NbClusters, NbFeatures> covars;
		Eigen::Matrix<float, NbClusters, 1> priors;
	} GMM;
	
	IFV() {}
	IFV(const std::string& filename, const std::string& name);
    //IFV(std::vector<FeaturePyramid::Level> patches);

    void convolve(std::vector<FeaturePyramid::Level> patches);
	
	void encode(const std::vector<MR8s>& patches, FeaturePyramid::Cell& vec);
private:
    //std::vector<int> dims_;
	GMM gmm_;
};

}
#endif
