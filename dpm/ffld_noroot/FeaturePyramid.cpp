#include "FeaturePyramid.h"
#include "dsift.h"

#include <iostream>
#include <limits>
#include "cnpy/cnpy.h"
#include <string>
#include <boost/lexical_cast.hpp>

using namespace Eigen;
using namespace FFLD;
using namespace std;

FeaturePyramid::FeaturePyramid(): padx_(1), pady_(1), interval_(5), step_(0), bin_size_(0), zero_(true)
{
}

FeaturePyramid::~FeaturePyramid()
{
}

FeaturePyramid::FeaturePyramid(string filename, int maxLevels, int padx, int pady, int interval, int step, int bin_size)
    : padx_(padx), pady_(pady), interval_(interval), step_(step), bin_size_(bin_size), zero_(false)
{
    string root = filename.substr(0,filename.find(".jpg"));
	root_ = root;
    vector<string> files;
    for(int i = 0; i <= maxLevels; i++) {
        files.push_back(root + "_l" + boost::lexical_cast<string>(i) + "_ifv_65.npy");
		//files.push_back(root + "_ifv.npy");
    }

    for(int i =0; i < files.size(); i++) {
        string filename = files[i];
        cnpy::NpyArray arr = cnpy::npy_load(filename);
        const int rows = arr.shape[0];
        const int cols = arr.shape[1];
        float * data = (float *) arr.data;

        //if(arr.shape.size() == 3) {
        //Cell zero(25600);
        FeaturePyramid::Level level = Level::Constant(rows,cols, Cell::Zero());
        for(int y = 0; y < rows; ++y) {
            for(int x = 0; x < cols; ++x) {
				//Eigen::Map<FeaturePyramid::Cell> m( &data[y * cols + x]) ;
				//level(y, x) = m;
                for(int z = 0; z < FeaturePyramid::NbFeatures; ++z) {
                    
					level(y,x)(z) = (float) data[y * cols * FeaturePyramid::NbFeatures + x * FeaturePyramid::NbFeatures + z];
                }
            }
        }
        levels_.push_back(level);
        //}
        arr.destruct();
    }
}

FeaturePyramid::FeaturePyramid(const JPEGImage& image, int padx, int pady, int interval, int step, int bin_size): padx_(padx),
    pady_(pady), interval_(interval), step_(step), bin_size_(bin_size)
{

}


void FeaturePyramid::SIFT(const JPEGImage& image, FeaturePyramid::Level& level, int padx, int pady)
{
    /*
    VlDsiftFilter* dsift = vl_dsift_new_basic(image.width(),image.height(),step_, bin_size_);
    vl_dsift_process(dsift,image.bits());
    int size = vl_dsift_get_descriptor_size(dsift);
    //	float * features = new float[size * 128];
    float * features  = vl_dsift_get_descriptors(dsift);
    vl_dsift_delete(dsift);
    */
}

void FeaturePyramid::convolve(const FeaturePyramid::Cell& filter, vector< FeaturePyramid::Matrix >& convolutions) const
{
    convolutions.resize(levels_.size());

    //#pragma omp parallel for
    for (int i = 0; i < levels_.size(); ++i)
        Convolve(levels_[i], filter, convolutions[i]);
}


void FeaturePyramid::Convolve(const FeaturePyramid::Level& x, const FeaturePyramid::Cell& y, FeaturePyramid::Matrix& z)
{
    z = Matrix::Zero(x.rows() , x.cols() );
    #pragma omp parallel for
    for(int i = 0; i < z.rows(); ++i) {
        for(int j = 0; j < z.cols(); ++j) {
            z(i,j) += x(i,j).dot(y);
        }
    }
}

FeaturePyramid::Level FeaturePyramid::Flip(const FeaturePyramid::Level& level)
{
	FeaturePyramid::Level result(level.rows(), level.cols());
    return result;
}


Eigen::Map< FeaturePyramid::Matrix, Eigen::Aligned > FeaturePyramid::Map(FeaturePyramid::Level& level)
{
    return Eigen::Map<Matrix, Aligned>(level.data()->data(), level.rows(),
									   level.cols() * FeaturePyramid::NbFeatures);
}

const Eigen::Map< const FeaturePyramid::Matrix, Eigen::Aligned > FeaturePyramid::Map(const FeaturePyramid::Level& level)
{
    return Eigen::Map<const Matrix, Aligned>(level.data()->data(), level.rows(),
											 level.cols() * FeaturePyramid::NbFeatures);
}

const vector< FeaturePyramid::Level >& FeaturePyramid::levels() const
{
    return levels_;
}


