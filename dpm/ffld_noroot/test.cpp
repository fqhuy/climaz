//--------------------------------------------------------------------------------------------------
// Implementation of the papers "Exact Acceleration of Linear Object Detectors", 12th European
// Conference on Computer Vision, 2012 and "Deformable Part Models with Individual Part Scaling",
// 24th British Machine Vision Conference, 2013.
//
// Copyright (c) 2013 Idiap Research Institute, <http://www.idiap.ch/>
// Written by Charles Dubout <charles.dubout@idiap.ch>
//
// This file is part of FFLDv2 (the Fast Fourier Linear Detector version 2)
//
// FFLDv2 is free software: you can redistribute it and/or modify it under the terms of the GNU
// Affero General Public License version 3 as published by the Free Software Foundation.
//
// FFLDv2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with FFLDv2. If
// not, see <http://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------------------------------

#include "SimpleOpt.h"

#include "Intersector.h"
#include "Mixture.h"
#include "Scene.h"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include "Common.h"

#ifndef _WIN32
#include <sys/time.h>

timeval Start, Stop;

inline void start()
{
    gettimeofday(&Start, 0);
}

inline int stop()
{
    gettimeofday(&Stop, 0);

    timeval duration;
    timersub(&Stop, &Start, &duration);

    return static_cast<int>(duration.tv_sec * 1000 + (duration.tv_usec + 500) / 1000);
}
#else
#include <time.h>
#include <windows.h>

ULARGE_INTEGER Start, Stop;

inline void start()
{
    GetSystemTimeAsFileTime((FILETIME *)&Start);
}

inline int stop()
{
    GetSystemTimeAsFileTime((FILETIME *)&Stop);
    Stop.QuadPart -= Start.QuadPart;
    return static_cast<int>((Stop.QuadPart + 5000) / 10000);
}
#endif

using namespace FFLD;
using namespace std;

template<class T1, class T2, class Pred = std::less<T2> >
struct sort_pair_second {
    bool operator()(const std::pair<T1,T2>&left, const std::pair<T1,T2>&right) {
        Pred p;
        return p(left.second, right.second);
    }
};

struct Detection : public Rectangle
{
    HOGPyramid::Scalar score;
    int x;
    int y;
    int z;

    Detection() : score(0), x(0), y(0), z(0)
    {
    }

    Detection(HOGPyramid::Scalar score, int x, int y, int z, Rectangle bndbox) : Rectangle(bndbox),
        score(score), x(x), y(y), z(z)
    {
    }

    bool operator<(const Detection & detection) const
    {
        return score > detection.score;
    }
};

// SimpleOpt array of valid options
enum
{
    OPT_INTERVAL, OPT_HELP, OPT_IMAGES, OPT_MODEL, OPT_NAME, OPT_PADDING, OPT_RESULT,
    OPT_THRESHOLD, OPT_OVERLAP, OPT_NB_NEG
};

CSimpleOpt::SOption SOptions[] =
{
    { OPT_INTERVAL, "-e", SO_REQ_SEP },
    { OPT_INTERVAL, "--interval", SO_REQ_SEP },
    { OPT_HELP, "-h", SO_NONE },
    { OPT_HELP, "--help", SO_NONE },
    { OPT_IMAGES, "-i", SO_REQ_SEP },
    { OPT_IMAGES, "--images", SO_REQ_SEP },
    { OPT_MODEL, "-m", SO_REQ_SEP },
    { OPT_MODEL, "--model", SO_REQ_SEP },
    { OPT_NAME, "-n", SO_REQ_SEP },
    { OPT_NAME, "--name", SO_REQ_SEP },
    { OPT_PADDING, "-p", SO_REQ_SEP },
    { OPT_PADDING, "--padding", SO_REQ_SEP },
    { OPT_RESULT, "-r", SO_REQ_SEP },
    { OPT_RESULT, "--result", SO_REQ_SEP },
    { OPT_THRESHOLD, "-t", SO_REQ_SEP },
    { OPT_THRESHOLD, "--threshold", SO_REQ_SEP },
    { OPT_OVERLAP, "-v", SO_REQ_SEP },
    { OPT_OVERLAP, "--overlap", SO_REQ_SEP },
    { OPT_NB_NEG, "-z", SO_REQ_SEP },
    { OPT_NB_NEG, "--nb-negatives", SO_REQ_SEP },
    SO_END_OF_OPTIONS
};

void showUsage()
{
    cout << "Usage: test [options] image.jpg, or\n       test [options] image_set.txt\n\n"
         "Options:\n"
         "  -e,--interval <arg>      Number of levels per octave in the HOG pyramid (default 5)"
         "\n"
         "  -h,--help                Display this information\n"
         "  -i,--images <folder>     Draw the detections to <folder> (default none)\n"
         "  -m,--model <file>        Read the input model from <file> (default \"model.txt\")\n"
         "  -n,--name <arg>          Name of the object to detect (default \"person\")\n"
         "  -p,--padding <arg>       Amount of zero padding in HOG cells (default 6)\n"
         "  -r,--result <file>       Write the detection result to <file> (default none)\n"
         "  -t,--threshold <arg>     Minimum detection threshold (default -1)\n"
         "  -v,--overlap <arg>       Minimum overlap in non maxima suppression (default 0.5)\n"
         "  -z,--nb-negatives <arg>  Maximum number of negative images to consider (default all)"
         << endl;
}

void draw(JPEGImage & image, const Rectangle & rect, uint8_t r, uint8_t g, uint8_t b, int linewidth)
{
    if (image.empty() || rect.empty() || (image.depth() < 3))
        return;

    const int width = image.width();
    const int height = image.height();
    const int depth = image.depth();
    uint8_t * bits = image.bits();

    // Draw 2 horizontal lines
    const int top = min(max(rect.top(), 1), height - linewidth - 1);
    const int bottom = min(max(rect.bottom(), 1), height - linewidth - 1);

    for (int x = max(rect.left() - 1, 0); x <= min(rect.right() + linewidth, width - 1); ++x) {
        if ((x != max(rect.left() - 1, 0)) && (x != min(rect.right() + linewidth, width - 1))) {
            for (int i = 0; i < linewidth; ++i) {
                bits[((top + i) * width + x) * depth    ] = r;
                bits[((top + i) * width + x) * depth + 1] = g;
                bits[((top + i) * width + x) * depth + 2] = b;
                bits[((bottom + i) * width + x) * depth    ] = r;
                bits[((bottom + i) * width + x) * depth + 1] = g;
                bits[((bottom + i) * width + x) * depth + 2] = b;
            }
        }

        // Draw a white line below and above the line
        if ((bits[((top - 1) * width + x) * depth    ] != 255) &&
                (bits[((top - 1) * width + x) * depth + 1] != 255) &&
                (bits[((top - 1) * width + x) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[((top - 1) * width + x) * depth + i] = 255;

        if ((bits[((top + linewidth) * width + x) * depth    ] != 255) &&
                (bits[((top + linewidth) * width + x) * depth + 1] != 255) &&
                (bits[((top + linewidth) * width + x) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[((top + linewidth) * width + x) * depth + i] = 255;

        if ((bits[((bottom - 1) * width + x) * depth    ] != 255) &&
                (bits[((bottom - 1) * width + x) * depth + 1] != 255) &&
                (bits[((bottom - 1) * width + x) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[((bottom - 1) * width + x) * depth + i] = 255;

        if ((bits[((bottom + linewidth) * width + x) * depth    ] != 255) &&
                (bits[((bottom + linewidth) * width + x) * depth + 1] != 255) &&
                (bits[((bottom + linewidth) * width + x) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[((bottom + linewidth) * width + x) * depth + i] = 255;
    }

    // Draw 2 vertical lines
    const int left = min(max(rect.left(), 1), width - linewidth - 1);
    const int right = min(max(rect.right(), 1), width - linewidth - 1);

    for (int y = max(rect.top() - 1, 0); y <= min(rect.bottom() + linewidth, height - 1); ++y) {
        if ((y != max(rect.top() - 1, 0)) && (y != min(rect.bottom() + linewidth, height - 1))) {
            for (int i = 0; i < linewidth; ++i) {
                bits[(y * width + left + i) * depth    ] = r;
                bits[(y * width + left + i) * depth + 1] = g;
                bits[(y * width + left + i) * depth + 2] = b;
                bits[(y * width + right + i) * depth    ] = r;
                bits[(y * width + right + i) * depth + 1] = g;
                bits[(y * width + right + i) * depth + 2] = b;
            }
        }

        // Draw a white line left and right the line
        if ((bits[(y * width + left - 1) * depth    ] != 255) &&
                (bits[(y * width + left - 1) * depth + 1] != 255) &&
                (bits[(y * width + left - 1) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[(y * width + left - 1) * depth + i] = 255;

        if ((bits[(y * width + left + linewidth) * depth    ] != 255) &&
                (bits[(y * width + left + linewidth) * depth + 1] != 255) &&
                (bits[(y * width + left + linewidth) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[(y * width + left + linewidth) * depth + i] = 255;

        if ((bits[(y * width + right - 1) * depth    ] != 255) &&
                (bits[(y * width + right - 1) * depth + 1] != 255) &&
                (bits[(y * width + right - 1) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[(y * width + right - 1) * depth + i] = 255;

        if ((bits[(y * width + right + linewidth) * depth    ] != 255) &&
                (bits[(y * width + right + linewidth) * depth + 1] != 255) &&
                (bits[(y * width + right + linewidth) * depth + 2] != 255))
            for (int i = 0; i < 3; ++i)
                bits[(y * width + right + linewidth) * depth + i] = 255;
    }
}

void prepareDB( vector<vector< string > > & database, vector<vector< string > > & queries, vector<Mixture> mixtures, const string& root,vector<string>& names, int splits[4]) {
    const int padding = 1;
    const string oroot = root + "_OUTPUT";

    vector<Scene> scenes;

    vector<string>::iterator it = names.begin();
    // Load all the scenes
    int maxRows = 0;
    int maxCols = 0;
    //READ POSITIVES ONLY
    while (it != names.end()) {
        ifstream mixin((oroot + "/" + (*it) + "_model.txt").c_str());
        Mixture mix;
        mixin >> mix;
        mixtures.push_back(mix);

        ifstream in1((root + "/" +  (*it) + ".txt").c_str());
        if (!in1.is_open()) {
            cerr << "Cannot open class file " << (root + "/" + (*it) + ".txt") << endl;
        } else {
            int count = 0;
            while(in1) {
                string line1;
                getline(in1,line1);

                if(line1 != "") {
                    vector<string> properties;
                    properties.push_back(line1);
                    properties.push_back((*it));

                    if(count >= splits[0] && count < splits[1])
                        database.push_back(properties);

                    if(count >= splits[2] && count < splits[3])
                        queries.push_back(properties);
                }
            }
            ++count;
        }
        in1.close();
        ++it;
    }

}

float initalizeSample(const Mixture& mixture, int width, int height, float threshold, float overlap, const HOGPyramid& pyramid, const FeaturePyramid & fpyramid,  Model& sample) {

    vector<Detection> detections;
    // Compute the scores
    vector<vector<double> > scores;
    vector<int> argmaxes;
    vector<vector<vector<Model::Position> > > positions;

    mixture.convolve(pyramid, fpyramid, scores, argmaxes, &positions);

    int argModel = -1;
    int argZ = -1;
    double maxScore = -numeric_limits<double>::infinity();

    // For each scale
    for (int z = 0; z < scores.size(); ++z) {
        const double score = scores[z][0];
        if (score > maxScore) {
            maxScore = score;
            argZ = z;
            argModel = argmaxes[z];
        }

    }
    Object obj;
    mixture.models()[argModel].initializeSample(obj, scores[argZ], pyramid, fpyramid, argZ, sample,&positions[argModel]);

    return scores[argZ][0];
}

void extractFeatures(const string& file, int padding, int interval,  const string& iroot, const string& oroot, HOGPyramid & pyramid, FeaturePyramid & fpyramid) {
    const JPEGImage image(iroot + "/" + file);

    if (image.empty()) {
        showUsage();
        cerr << "\nInvalid image " << file << endl;
        return ;
    }
    pyramid = HOGPyramid(image, padding, padding, interval);
#ifndef NO_TEXTURE
    fpyramid = FeaturePyramid(oroot + "/" + file);
#endif
    if (pyramid.empty()) {
        showUsage();
        cerr << "\nInvalid image " << file << endl;
        return ;
    }
}

/// database: [file name, material]
void retrieve(const vector<Mixture> mixtures, const vector<vector< string > > & queries, const vector<vector< string > > & database,  vector<float> & precisions, int take = 40) {
    precisions.resize(queries.size());
    const string& iroot = mixtures[0].iRoot;
    const string& oroot = mixtures[0].oRoot;


    for(int q = 0; q < queries.size(); ++q) {
        const string& query = queries[q][0];
        Model query_model;
        int bestscore = 0;
        int bestmix = 0;
        HOGPyramid pyramid;
        FeaturePyramid fpyramid;
        extractFeatures(query,1,5,iroot, oroot,pyramid, fpyramid);
        for(int m = 0; m < mixtures.size(); ++m) {
            //Cache mixtures' filters if necessary
            if(!mixtures[m].cached())
            {
                start();
                mixtures[m].cacheFilters();
                cout << "Transformed the filters in " << stop() << " ms" << endl;
            }
            //TODO: find best mixtures
            Model tmp;
            if(pyramid.empty()) {
                cerr << "Unable to extract HOG from " << query << endl;
            }
#ifndef NO_TEXTURE
            if(fpyramid.empty()) {
                cerr << "Unable to extract IFV from " << query << endl;
            }
#endif
            const int score = initalizeSample(mixtures[m], 1, 5, 1, 0.3, pyramid, fpyramid, tmp);
            if(score > bestscore) {
                bestscore = score;
                bestmix = m;
                query_model = tmp;
            }
        }

        vector<pair<int, float> > distances;
        for(int d = 0; d < database.size(); ++d) {
            Model sample;
            //TODO: load sample from file.
            ifstream in((database[d][0] + ".txt").c_str(),ios::in);

            if (!in.is_open()) {
                showUsage();
                cerr << "\nCannot open object list " << (database[d][0] + ".txt") << endl;
                return ;
            }
            in >> sample;
            const float distance = sample.dot(query_model);
            distances.push_back(make_pair<int,float>(d, distance));
        }
        //sort ID's by distances Small to Big
        sort(distances.begin(), distances.end(), sort_pair_second<int,float>());

        float corrects = 0;
        for(int d = 0; d < take; ++d) {
            if(queries[q][1] == database[distances[d].first][1]) {
                corrects++;
            }
        }
        precisions.push_back(corrects / (float) take);
    }
}

void detect(const Mixture & mixture, int width, int height, const HOGPyramid & pyramid, const FeaturePyramid & fpyramid,
            double threshold, double overlap, const string image, ostream & out,
            const string & images, vector<Detection> & detections, const Scene * scene = 0,
            Object::Name name = Object::UNKNOWN)
{
    // Compute the scores

    vector<vector<double> > scores;
    vector<int> argmaxes;
    vector<vector<vector<Model::Position> > > positions;
    mixture.convolve(pyramid, fpyramid, scores, argmaxes, &positions);

    // For each scale
	double maxScore = -9999;
	int argZ = -1;
    for (int z = 0; z < scores.size(); ++z) {
        const double score = scores[z][0];
        if (score > maxScore) {
			maxScore = score;
			argZ = z;
            //cout << "score(" << x << "," << y << "," << z << ") = " << score << endl;

        }
    }
    Rectangle bndbox(0,0, 250,250);
	detections.push_back(Detection(maxScore, 0, 0, argZ, bndbox));
	
// Non maxima suppression
    sort(detections.begin(), detections.end());

// Find the image id
    string id = image.substr(0, image.find_last_of('.'));

    if (id.find_last_of("/\\") != string::npos)
        id = id.substr(id.find_last_of("/\\") + 1);

// Draw the detections
    if (!images.empty()) {
        JPEGImage im(image);
        im = im.rescale(0.25);

        for (int i = 0; i < detections.size(); ++i) {
            const int argmax = argmaxes[detections[i].z];
            const int z = detections[i].z;
            //For 20 parts
            for (int j = 0; j < positions[argmax].size(); ++j) {
                const int xp = positions[argmax][j][z](0);
                const int yp = positions[argmax][j][z](1);
                const int zp = positions[argmax][j][z](2);

                const double scale = pow(2.0, static_cast<double>(zp) / pyramid.interval() + 2);

                const Rectangle bndbox((xp - pyramid.padx()) * scale + 0.5,
                                       (yp - pyramid.pady()) * scale + 0.5,
                                       mixture.models()[argmax].partSize().second * scale + 0.5,
                                       mixture.models()[argmax].partSize().first * scale + 0.5);

                draw(im, bndbox, 0, 0, 255, 2);
            }

            // Draw the root last
            //draw(im, detections[i], positive ? 0 : 255, positive ? 255 : 0, 0, 2);
        }

        im.save(images + '/' + id + ".jpg");
    }
}

int detectMany(const string& file, const string & model, const string & result, const string& root, const string& oroot, int interval, int overlap, int padding, int threshold, string images) {
    // Try to open the mixture
    ifstream mixin(model.c_str(), ios::binary);

    if (!mixin.is_open()) {
        showUsage();
        cerr << "\nInvalid model file " << model << endl;
        return -1;
    }

    Mixture mixture;
    mixin >> mixture;
    mixture.iRoot = root;
    mixture.oRoot = oroot;

    if (mixture.empty()) {
        showUsage();
        cerr << "\nInvalid model file " << model << endl;
        return -1;
    }

    // Try to open the result file
    ofstream out;

    if (!result.empty()) {
        out.open(result.c_str(), ios::binary);

        if (!out.is_open()) {
            showUsage();
            cerr << "\nInvalid result file " << result << endl;
            return -1;
        }
    }
    // Try to load the image

    ifstream in(file.c_str());
    if (!in.is_open()) {
        showUsage();
        cerr << "\nCannot open list of test images " << file << endl;
        return -1;
    }
    vector<string> test_images;
    while (in) {
        string line;
        getline(in, line);
        if(line != "")
            test_images.push_back(line);
    }
    in.close();

    for(int i = 0; i < test_images.size(); ++i) {
        const JPEGImage image(mixture.iRoot + "/" + test_images[i]);

        if (image.empty()) {
            showUsage();
            cerr << "\nInvalid image " << test_images[i] << endl;
            return -1;
        }

        // Compute the HOG features
        start();

        HOGPyramid pyramid(image, padding, padding, interval);

#ifndef NO_TEXTURE
        FeaturePyramid fpyramid(mixture.oRoot + "/" + test_images[i]);
#else
        FeaturePyramid fpyramid;
#endif

        if (pyramid.empty()) {
            showUsage();
            cerr << "\nInvalid image " << test_images[i] << endl;
            return -1;
        }

        cout << "Computed HOG features in " << stop() << " ms" << endl;

        // Initialize the Patchwork class
        start();

        if (!Patchwork::InitFFTW((pyramid.levels()[0].rows() - padding + 15) & ~15,
                                 (pyramid.levels()[0].cols() - padding + 15) & ~15)) {
            //if (!Patchwork::InitFFTW((maxRows + 15) & ~15, (maxCols + 15) & ~15)) {
            cerr << "\nCould not initialize the Patchwork class" << endl;
            return -1;
        }

        cout << "Initialized FFTW in " << stop() << " ms" << endl;

        start();

        mixture.cacheFilters();

        cout << "Transformed the filters in " << stop() << " ms" << endl;

        // Compute the detections
        start();

        vector<Detection> detections;

        detect(mixture, image.width(), image.height(), pyramid, fpyramid, threshold, overlap,mixture.iRoot + "/" + test_images[i], out,
               images, detections);

        cout << "Computed the convolutions and distance transforms in " << stop() << " ms" << endl;
    }
    return 0;
}

void makeNameList(const string& object_list, vector<string> names) {

    ifstream in(object_list.c_str());
    if (!in.is_open()) {
        showUsage();
        cerr << "\nCannot open object list " << endl;
        return;
    }

    while (in) {
        string line;
        getline(in, line);
        if(line != "")
            names.push_back(line);
    }
    in.close();
    Object::NameMap.setNames(names);
    vector<string> poses;
    poses.push_back(string("Frontal"));
    poses.push_back(string("Back"));
    Object::PoseMap.setNames(poses);

}

int main(int argc, char ** argv)
{
    // Default parameters
    int interval = 5;
    string images = ".";
    Object::Name name = Object::UNKNOWN;//Object::NameMap["corduroy"];
    string sname = "corduroy";
    string model(sname  + "_model.txt");
    int padding = 1;
    string result;
    double threshold = -1.0;
    double overlap = 0.3;
    int nbNegativeScenes = -1;

    // Parse the parameters
    CSimpleOpt args(argc, argv, SOptions);

    while (args.Next()) {
        if (args.LastError() == SO_SUCCESS) {
            if (args.OptionId() == OPT_INTERVAL) {
                interval = atoi(args.OptionArg());
                // Error checking
                if (interval <= 0) {
                    showUsage();
                    cerr << "\nInvalid interval arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_HELP) {
                showUsage();
                return 0;
            }
            else if (args.OptionId() == OPT_IMAGES) {
                images = args.OptionArg();
            }
            else if (args.OptionId() == OPT_MODEL) {
                model = args.OptionArg();
            }
            else if (args.OptionId() == OPT_NAME) {
                sname = args.OptionArg();
            }
            else if (args.OptionId() == OPT_PADDING) {
                padding = atoi(args.OptionArg());

                if (padding <= 1) {
                    showUsage();
                    cerr << "\nInvalid padding arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_RESULT) {
                result = args.OptionArg();
            }
            else if (args.OptionId() == OPT_THRESHOLD) {
                threshold = atof(args.OptionArg());
            }
            else if (args.OptionId() == OPT_OVERLAP) {
                overlap = atof(args.OptionArg());

                if ((overlap <= 0.0) || (overlap >= 1.0)) {
                    showUsage();
                    cerr << "\nInvalid overlap arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_NB_NEG) {
                nbNegativeScenes = atoi(args.OptionArg());

                if (nbNegativeScenes < 0) {
                    showUsage();
                    cerr << "\nInvalid nb-negatives arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
        }
        else {
            showUsage();
            cerr << "\nUnknown option " << args.OptionText() << endl;
            return -1;
        }
    }

    if (!args.FileCount()) {
        showUsage();
        cerr << "\nNo image/dataset provided" << endl;
        return -1;
    }


    // The image/dataset
    const string root(args.File(0));
    const string oroot = root + "_OUTPUT";
    const string object_list = root + "/info.txt";
    const string file = root + "/tests.txt";
    const size_t lastDot = file.find_last_of('.');

    vector<string> names;
    makeNameList(object_list, names);
    detectMany(file,model, result, root, oroot,interval, overlap, padding , threshold, images);

    /*
    int splits[] = {0,40,40,60};
    vector<Mixture> mixtures;
    vector<vector< string > > database;
    vector<vector< string > > queries;
    vector<float> precisions;

    prepareDB(database, queries, mixtures, names, root, splits);
    retrieve(mixtures,queries,database,precisions);
    */
}
