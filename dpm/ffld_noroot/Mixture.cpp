//--------------------------------------------------------------------------------------------------
// Implementation of the papers "Exact Acceleration of Linear Object Detectors", 12th European
// Conference on Computer Vision, 2012 and "Deformable Part Models with Individual Part Scaling",
// 24th British Machine Vision Conference, 2013.
//
// Copyright (c) 2013 Idiap Research Institute, <http://www.idiap.ch/>
// Written by Charles Dubout <charles.dubout@idiap.ch>
//
// This file is part of FFLDv2 (the Fast Fourier Linear Detector version 2)
//
// FFLDv2 is free software: you can redistribute it and/or modify it under the terms of the GNU
// Affero General Public License version 3 as published by the Free Software Foundation.
//
// FFLDv2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with FFLDv2. If
// not, see <http://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------------------------------

#include "Intersector.h"
#include "LBFGS.h"
#include "Mixture.h"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include "Common.h"

using namespace Eigen;
using namespace FFLD;
using namespace std;

Mixture::Mixture() : cached_(false), zero_(true)
{
}

Mixture::Mixture(const vector<Model> & models) : models_(models), cached_(false), zero_(true)
{
}

Mixture::Mixture(int nbComponents, const vector<Scene> & scenes, Object::Name name) :
    cached_(false), zero_(true)
{
    // Create an empty mixture if any of the given parameters is invalid
    if ((nbComponents <= 0) || scenes.empty()) {
        cerr << "Attempting to create an empty mixture" << endl;
        return;
    }

    // Compute the root filters' sizes using Felzenszwalb's heuristic
    //const vector<pair<int, int> > sizes = FilterSizes(nbComponents, scenes, name);

    // Early return in case the root filters' sizes could not be determined
    ///if (sizes.size() != nbComponents)
    //    return;

    // Initialize the models (with symmetry) to those sizes
    models_.resize(2 * nbComponents);

    for (int i = 0; i < nbComponents; ++i) {
        models_[2 * i    ] = Model();//Model(sizes[i]);
        models_[2 * i + 1] = Model();//Model(sizes[i]);
    }
}

bool Mixture::empty() const
{
    return models_.empty();
}

const vector<Model> & Mixture::models() const
{
    return models_;
}

vector<Model> & Mixture::models()
{
    return models_;
}

pair<int, int> Mixture::minSize() const
{
    pair<int, int> size(0, 0);

    if (!models_.empty()) {
        size = models_[0].rootSize();

        for (int i = 1; i < models_.size(); ++i) {
            size.first = min(size.first, models_[i].rootSize().first);
            size.second = min(size.second, models_[i].rootSize().second);
        }
    }

    return size;
}

pair<int, int> Mixture::maxSize() const
{
    pair<int, int> size(0, 0);

    if (!models_.empty()) {
        size = models_[0].rootSize();

        for (int i = 1; i < models_.size(); ++i) {
            size.first = max(size.first, models_[i].rootSize().first);
            size.second = max(size.second, models_[i].rootSize().second);
        }
    }

    return size;
}

double Mixture::train(const vector<Scene> & scenes, Object::Name name, int padx, int pady,
                      int interval, int nbRelabel, int nbDatamine, int maxNegatives, double C,
                      double J, double overlap, int startLabel, int startDatamine)
{
    if (empty() || scenes.empty() || (nbRelabel < 1) || (nbDatamine < 1) || (maxNegatives < models_.size()) || (C <= 0.0) ||
            (J <= 0.0) || (overlap <= 0.0) || (overlap >= 1.0)) {
        cerr << "Invalid training parameters" << endl;
        return numeric_limits<double>::quiet_NaN();
    }

    double loss = numeric_limits<double>::infinity();

    for (int relabel = startLabel; relabel < nbRelabel; ++relabel) {
        cout << "relabeling ..." << relabel << endl;
        // Sample all the positives
        vector<pair<Model, int> > positives;
        vector<pair<Model, int> > negatives;

        posLatentSearch(scenes, name, padx, pady, interval, overlap, positives);

        // Left-right clustering at the first iteration
        //if (zero_)
        //    Cluster(static_cast<int>(models_.size()), positives);

        // Previous loss on the cache
        double prevLoss = -numeric_limits<double>::infinity();

        for (int datamine = startDatamine; datamine < nbDatamine; ++datamine) {
            cout << "datamine ..." << datamine << endl;
            // Remove easy samples (keep hard ones)
            int j = 0;

            for (int i = 0; i < negatives.size(); ++i)
                if ((negatives[i].first.parts()[0].deformation(3) =
                            models_[negatives[i].second].dot(negatives[i].first)) > -1.01)
                    negatives[j++] = negatives[i];

            negatives.resize(j);

            cout << "Negative latent search..." << endl;
            // Sample new hard negatives
            negLatentSearch(scenes, name, padx, pady, interval, maxNegatives, negatives);

            // Stop if there are no new hard negatives
            if (datamine && (negatives.size() == j))
                break;

            // Merge the left / right samples for more efficient training
            vector<int> posComponents(positives.size());

            for (int i = 0; i < positives.size(); ++i) {
                posComponents[i] = positives[i].second;

                if (positives[i].second & 1)
                    positives[i].first = positives[i].first.flip();

                positives[i].second >>= 1;
            }

            vector<int> negComponents(negatives.size());

            for (int i = 0; i < negatives.size(); ++i) {
                negComponents[i] = negatives[i].second;

                if (negatives[i].second & 1)
                    negatives[i].first = negatives[i].first.flip();

                negatives[i].second >>= 1;
            }

            // Merge the left / right models for more efficient training
            for (int i = 1; i < models_.size() / 2; ++i)
                models_[i] = models_[i * 2];

            models_.resize(models_.size() / 2);

            const int maxIterations =
                min(max(10.0 * sqrt(static_cast<double>(positives.size())), 100.0), 1000.0);

            loss = train(positives, negatives, C, J, maxIterations);

            cout << "Relabel: " << relabel << ", datamine: " << datamine
                 << ", # positives: " << positives.size() << ", # hard negatives: " << j
                 << " (already in the cache) + " << (negatives.size() - j) << " (new) = "
                 << negatives.size() << ", loss (cache): " << loss << endl;

            // Unmerge the left / right samples
            for (int i = 0; i < positives.size(); ++i) {
                positives[i].second = posComponents[i];

                if (positives[i].second & 1)
                    positives[i].first = positives[i].first.flip();
            }

            for (int i = 0; i < negatives.size(); ++i) {
                negatives[i].second = negComponents[i];

                if (negatives[i].second & 1)
                    negatives[i].first = negatives[i].first.flip();
            }

            // Unmerge the left / right models
            models_.resize(models_.size() * 2);

            for (int i = static_cast<int>(models_.size()) / 2 - 1; i >= 0; --i) {
                models_[i * 2    ] = models_[i];
                models_[i * 2 + 1] = models_[i].flip();
            }

            // The filters definitely changed
            filterCache_.clear();
            cached_ = false;
            zero_ = false;

            // Save the latest model so as to be able to look at it while training
            const string outname = Object::NameMap[name] + "_tmp_" + boost::lexical_cast<string>(relabel) + "_"
                                   + boost::lexical_cast<string>(datamine) + (models_[0].parts().size() == 1 ? "_" : "")  + ".txt" ;
            ofstream out(outname.c_str());

            out << (*this);

            // Stop if we are not making progress
            if ((0.999 * loss < prevLoss) && (negatives.size() < maxNegatives))
                break;

            prevLoss = loss;
        }
    }

    return loss;
}

void Mixture::initializeParts(int nbParts, pair<int, int> partSize, const std::vector<std::vector<HOGPyramid::Level> > & parts,const std::vector<FeaturePyramid::Cell>& texs)
{
    for (int i = 0; i < models_.size(); i += 2) {
        std::vector<FeaturePyramid::Cell> model_texs;
        model_texs.push_back(texs[i/2]);
        models_[i].initializeParts(parts[i/2], partSize, model_texs);

        models_[i + 1] = models_[i].flip();
    }

    // The filters definitely changed
    filterCache_.clear();
    cached_ = false;
    zero_ = false;
}

void Mixture::convolve(const HOGPyramid & pyramid, const FeaturePyramid & fpyramid, vector<vector<double> > & scores,
                       vector<int> & argmaxes,
                       vector<vector<vector<Model::Position> > > * positions) const { // position: model - part - z  - (y, x)
    if(empty() || pyramid.empty()) {
        scores.clear();
        argmaxes.clear();
        if(positions)
            positions->clear();
        return;
    }

    const int nbModels = static_cast<int>(models_.size());
    const int nbLevels = static_cast<int>(pyramid.levels().size());

    vector<vector<vector<double> > > convolutions;
    convolve(pyramid, fpyramid, convolutions, positions);

    if(convolutions.empty()) {
        cerr << "Convolution Failed!" << endl;
        scores.clear();
        argmaxes.clear();
        if(positions)
            positions->clear();
        return;
    }

    scores.resize(nbLevels);
    argmaxes.resize(nbLevels);

    for (int z = 0; z < nbLevels; ++z) {
        //We store the TOTAL SCORE in scores[z][0], and parts' score in 1 ... nbParts
        scores[z].resize(models_[0].parts().size() + 1);

        int argmax = 0;
        for (int i = 1; i < nbModels; ++i)
            if ( convolutions[i][z][0] > convolutions[argmax][z][0])
                argmax = i;

			cout <<  "best scores: ";
        for (int i = 0; i < models_[0].parts().size() + 1; ++i){
            scores[z][i] = convolutions[argmax][z][i];
			cout  << scores[z][i] << ", ";
		}
		cout << endl;
        argmaxes[z] = argmax;
    }
}

void Mixture::cacheFilters() const
{
    // Count the number of filters
    int nbFilters = 0;

    for (int i = 0; i < models_.size(); ++i)
        nbFilters += models_[i].parts().size();

    // Transform all the filters
    filterCache_.resize(nbFilters);

    for (int i = 0, j = 0; i < models_.size(); ++i) {
        #pragma omp parallel for
        for (int k = 0; k < models_[i].parts().size(); ++k)
            Patchwork::TransformFilter(models_[i].parts()[k].filter, filterCache_[j + k]);

        j += models_[i].parts().size();
    }

    cached_ = true;
}

void Mixture::posLatentSearch(const vector<Scene> & scenes, Object::Name name, int padx, int pady,
                              int interval, double overlap,
                              vector<pair<Model, int> > & positives) const
{
    if (scenes.empty()) {
        positives.clear();
        cerr << "Invalid training paramters" << endl;
        return;
    }

    positives.clear();

    for (int i = 0; i < scenes.size(); ++i) {

        // Skip negative scenes
        bool negative = true;

        for (int j = 0; j < scenes[i].objects().size(); ++j)
            if ((scenes[i].objects()[j].name() == name) && !scenes[i].objects()[j].difficult())
                negative = false;

        if (negative)
            continue;

        cout << "scene " << scenes[i].filename() << endl;
        /*
        const JPEGImage image(iRoot + "/" + scenes[i].filename());

        if (image.empty()) {
            positives.clear();
            return;
        }
        //Automatic Scaling for HOG (0.125)
        const HOGPyramid pyramid(image, padx, pady, interval);
		*/
        const HOGPyramid pyramid(oRoot + "/" + scenes[i].filename(), padx, pady, interval);
#ifndef NO_TEXTURE
        const FeaturePyramid fpyramid(oRoot + "/" + scenes[i].filename());//(scenes[i].featureFiles(), padx, pady, interval, );
#else
        const FeaturePyramid fpyramid;
#endif
        if (pyramid.empty()) {
            positives.clear();
            return;
        }

        vector<vector<double> > scores;
        vector<int> argmaxes;
        vector<vector<vector<Model::Position> > > positions; //part - model - z

        convolve(pyramid,fpyramid,scores, argmaxes, &positions);

        for (int j = 0; j < scenes[i].objects().size(); ++j) {
            // Ignore objects with a different name or difficult objects
            if ((scenes[i].objects()[j].name() != name) || scenes[i].objects()[j].difficult())
                continue;

            // The model, level, position, score, of the best example
            int argModel = -1;
            int argZ = -1;
            double maxScore = -numeric_limits<double>::infinity();
            double maxInter = 0.0;

            //Find best model & best z
            for (int z = 0; z < pyramid.levels().size(); ++z) {
                if( scores[z][0] > maxScore )
                {
                    maxScore = scores[z][0];
                    argModel = argmaxes[z];
                    argZ = z;
                }
            }

            Model sample;

            models_[argModel].initializeSample(scenes[i].objects()[j] , scores[argZ], pyramid, fpyramid, argZ, sample,&positions[argModel]);

            if (!sample.empty()) {
                positives.push_back(make_pair(sample, argModel));
                const string outname = scenes[i].filename() +  ".txt" ;
                ofstream out(outname.c_str());
                out << sample;
            }

        }
    }
}

static inline bool operator==(const Model & a, const Model & b)
{
    return (a.parts()[0].offset == b.parts()[0].offset) &&
           (a.parts()[0].deformation(0) == b.parts()[0].deformation(0)) &&
           (a.parts()[0].deformation(1) == b.parts()[0].deformation(1));
}

static inline bool operator<(const Model & a, const Model & b)
{
    return (a.parts()[0].offset(0) < b.parts()[0].offset(0)) ||
           ((a.parts()[0].offset(0) == b.parts()[0].offset(0)) &&
            ((a.parts()[0].offset(1) < b.parts()[0].offset(1)) ||
             ((a.parts()[0].offset(1) == b.parts()[0].offset(1)) &&
              ((a.parts()[0].deformation(0) < b.parts()[0].deformation(0)) ||
               ((a.parts()[0].deformation(0) == b.parts()[0].deformation(0)) &&
                ((a.parts()[0].deformation(1) < b.parts()[0].deformation(1))))))));
}

void Mixture::negLatentSearch(const vector<Scene> & scenes, Object::Name name, int padx, int pady,
                              int interval, int maxNegatives,
                              vector<pair<Model, int> > & negatives) const
{
    // Sample at most (maxNegatives - negatives.size()) negatives with a score above -1.0
    if (scenes.empty() || (maxNegatives <= 0) ||
            (negatives.size() >= maxNegatives)) {
        negatives.clear();
        cerr << "Invalid training paramters" << endl;
        return;
    }

    // The number of negatives already in the cache
    const int nbCached = static_cast<int>(negatives.size());

    for (int i = 0, j = 0; i < scenes.size(); ++i) {
        // Skip positive scenes
        bool positive = false;

        for (int k = 0; k < scenes[i].objects().size(); ++k)
            if (scenes[i].objects()[k].name() == name)
                positive = true;

        if (positive)
            continue;

        cout << "scene " << scenes[i].filename() << endl;
		/*
        const JPEGImage image(iRoot + "/" + scenes[i].filename());

        if (image.empty()) {
            negatives.clear();
            return;
        }

        const HOGPyramid pyramid(image, padx, pady, interval);
		*/
		const HOGPyramid pyramid(oRoot + "/" + scenes[i].filename(), padx, pady, interval);
#ifndef	NO_TEXTURE
        FeaturePyramid  fpyramid;
        if(models_[0].parts().size() > 1)
            fpyramid = FeaturePyramid(oRoot + "/" + scenes[i].filename());
#else
        const FeaturePyramid fpyramid;
#endif

        if (pyramid.empty()) {
            negatives.clear();
            return;
        }

        vector<vector<double> > scores;
        vector<int> argmaxes;
        vector<vector<vector<Model::Position> > > positions;

        convolve(pyramid, fpyramid, scores, argmaxes, &positions);

        for (int z = 0; z < pyramid.levels().size(); ++z) {
            const int argmax = argmaxes[z];
            if (scores[z][0] > -1) {
                Model sample;
                Object obj;
				if(scenes[i].objects().size() > 0)
                	models_[argmax].initializeSample(scenes[i].objects()[0], scores[z], pyramid, fpyramid,  z, sample,
                                                 &positions[argmax]);
				else
					models_[argmax].initializeSample(obj, scores[z], pyramid, fpyramid,  z, sample,
													 &positions[argmax]);

                if (!sample.empty()) {
                    // Store all the information about the sample in the offset and
                    // deformation of its root

                    sample.parts()[0].offset(0) = i;
                    sample.parts()[0].offset(1) = z;
                    sample.parts()[0].deformation(0) = 0;
                    sample.parts()[0].deformation(1) = 0;
                    sample.parts()[0].deformation(2) = argmax;
                    sample.parts()[0].deformation(3) = scores[z][0];

                    // Look if the same sample was already sampled
                    while ((j < nbCached) && (negatives[j].first < sample))
                        ++j;

                    // Make sure not to put the same sample twice
                    if ((j >= nbCached) || !(negatives[j].first == sample)) {
                        negatives.push_back(make_pair(sample, argmax));

                        if (negatives.size() == maxNegatives)
                            return;
                    }
                }
            }

        }

    }
}


namespace FFLD
{
namespace detail
{
class Loss : public LBFGS::IFunction
{
public:
    Loss(vector<Model> & models, const vector<pair<Model, int> > & positives,
         const vector<pair<Model, int> > & negatives, double C, double J, int maxIterations) :
        models_(models), positives_(positives), negatives_(negatives), C_(C), J_(J),
        maxIterations_(maxIterations)
    {
    }

    virtual int dim() const
    {
        int d = 0;

        for (int i = 0; i < models_.size(); ++i) {
            for (int j = 0; j < models_[i].parts().size(); ++j) {
                d += models_[i].parts()[j].filter.size() * HOGPyramid::NbFeatures; // Filter

                //if (j)
                //    d += 6; // Deformation
            }
#ifndef NO_TEXTURE
            d += models_[i].ifvs().size() * FeaturePyramid::NbFeatures;
#endif
            ++d; // Bias
        }

        return d;
    }

    virtual double operator()(const double * x, double * g = 0) const
    {
        // Recopy the features into the models
        ToModels(x, models_);

        // Compute the loss and gradient over the samples
        double loss = 0.0;

        vector<Model> gradients;

        if (g) {
            gradients.resize(models_.size());

            for (int i = 0; i < models_.size(); ++i)
                gradients[i] = Model(models_[i].ifvs().size(), static_cast<int>(models_[i].parts().size()),	 models_[i].partSize());
            /*
            gradients[i] = Model(models_[i].rootSize(), models_[i].ifvs().size(),
                                 static_cast<int>(models_[i].parts().size()) - 1,
                                 models_[i].partSize());
            */
        }

        vector<double> posMargins(positives_.size());

        #pragma omp parallel for
        for (int i = 0; i < positives_.size(); ++i)
            posMargins[i] = models_[positives_[i].second].dot(positives_[i].first);

        for (int i = 0; i < positives_.size(); ++i) {
            if (posMargins[i] < 1.0) {
                loss += 1.0 - posMargins[i];

                if (g)
                    gradients[positives_[i].second] -= positives_[i].first;
            }
        }

        // Reweight the positives
        if (J_ != 1.0) {
            loss *= J_;

            if (g) {
                for (int i = 0; i < models_.size(); ++i)
                    gradients[i] *= J_;
            }
        }

        vector<double> negMargins(negatives_.size());

        #pragma omp parallel for
        for (int i = 0; i < negatives_.size(); ++i)
            negMargins[i] = models_[negatives_[i].second].dot(negatives_[i].first);

        for (int i = 0; i < negatives_.size(); ++i) {
            if (negMargins[i] > -1.0) {
                loss += 1.0 + negMargins[i];

                if (g)
                    gradients[negatives_[i].second] += negatives_[i].first;
            }
        }

        // Add the loss and gradient of the regularization term
        double maxNorm = 0.0;
        int argNorm = 0;

        for (int i = 0; i < models_.size(); ++i) {
            if (g)
                gradients[i] *= C_;

            const double norm = models_[i].norm();

            if (norm > maxNorm) {
                maxNorm = norm;
                argNorm = i;
            }
        }

        // Recopy the gradient if needed
        if (g) {
            // Regularization gradient
            gradients[argNorm] += models_[argNorm];
            //TODO: Regularize Texture 4 times more.
            for (int i = 0; i < gradients[argNorm].ifvs().size(); ++i) {
                gradients[argNorm].ifvs()[i] +=	19.0 * models_[argNorm].ifvs()[i];
            }

            // Regularize the deformation 10 times more
            //TODO: Regularize deformation ?
            /*
            for (int i = 1; i < gradients[argNorm].parts().size(); ++i)
                gradients[argNorm].parts()[i].deformation +=
                    9.0 * models_[argNorm].parts()[i].deformation;
            */
            // Do not regularize the bias
            gradients[argNorm].bias() -= models_[argNorm].bias();
            /*
            // In case minimum constraints were applied
            for (int i = 0; i < models_.size(); ++i) {
                for (int j = 1; j < models_[i].parts().size(); ++j) {
                    if (models_[i].parts()[j].deformation(0) >= -0.005)
                        gradients[i].parts()[j].deformation(0) =
                            max(gradients[i].parts()[j].deformation(0), 0.0);

                    if (models_[i].parts()[j].deformation(2) >= -0.005)
                        gradients[i].parts()[j].deformation(2) =
                            max(gradients[i].parts()[j].deformation(2), 0.0);

                    if (models_[i].parts()[j].deformation(4) >= -0.005)
                        gradients[i].parts()[j].deformation(4) =
                            max(gradients[i].parts()[j].deformation(4), 0.0);
                }
            }
            */
            FromModels(gradients, g);
        }

        return 0.5 * maxNorm * maxNorm + C_ * loss;
    }

    static void ToModels(const double * x, vector<Model> & models)
    {
        for (int i = 0, j = 0; i < models.size(); ++i) {
            for (int k = 0; k < models[i].parts().size(); ++k) {
                const int nbFeatures = static_cast<int>(models[i].parts()[k].filter.size()) *
                                       HOGPyramid::NbFeatures;

                copy(x + j, x + j + nbFeatures, models[i].parts()[k].filter.data()->data());

                j += nbFeatures;
                /*
                if (k) {
                    // Apply minimum constraints
                    models[i].parts()[k].deformation(0) = min((x + j)[0],-0.005);
                    models[i].parts()[k].deformation(1) = (x + j)[1];
                    models[i].parts()[k].deformation(2) = min((x + j)[2],-0.005);
                    models[i].parts()[k].deformation(3) = (x + j)[3];
                    models[i].parts()[k].deformation(4) = min((x + j)[4],-0.005);
                    models[i].parts()[k].deformation(5) = (x + j)[5];

                    j += 6;
                }
                */
            }
#ifndef NO_TEXTURE
            for (int k = 0; k < models[i].ifvs().size(); ++k) {
                const int nbFeatures = FeaturePyramid::NbFeatures;
                copy(x + j, x + j + nbFeatures, models[i].ifvs()[k].data());
                j += nbFeatures;
            }
#endif
            models[i].bias() = x[j];
            ++j;
        }
    }

    static void FromModels(const vector<Model> & models, double * x)
    {
        for (int i = 0, j = 0; i < models.size(); ++i) {
            for (int k = 0; k < models[i].parts().size(); ++k) {
                const int nbFeatures = static_cast<int>(models[i].parts()[k].filter.size()) *
                                       HOGPyramid::NbFeatures;

                copy(models[i].parts()[k].filter.data()->data(),
                     models[i].parts()[k].filter.data()->data() + nbFeatures, x + j);

                j += nbFeatures;
                /*
                if (k) {
                    copy(models[i].parts()[k].deformation.data(),
                         models[i].parts()[k].deformation.data() + 6, x + j);

                    j += 6;
                }
                */
            }
#ifndef NO_TEXTURE
            for (int k = 0; k < models[i].ifvs().size(); ++k) {
                const int nbFeatures = FeaturePyramid::NbFeatures;
                copy(models[i].ifvs()[k].data(),models[i].ifvs()[k].data() + nbFeatures,x + j);
                j += nbFeatures;
            }
#endif
            x[j] = models[i].bias();

            ++j;
        }
    }

private:
    vector<Model> & models_;
    const vector<pair<Model, int> > & positives_;
    const vector<pair<Model, int> > & negatives_;
    double C_;
    double J_;
    int maxIterations_;
};
}
}

double Mixture::train(const vector<pair<Model, int> > & positives,
                      const vector<pair<Model, int> > & negatives, double C, double J,
                      int maxIterations)
{
    detail::Loss loss(models_, positives, negatives, C, J, maxIterations);
    LBFGS lbfgs(&loss, 0.001, maxIterations, 20, 20);

    // Start from the current models
    VectorXd x(loss.dim());

    detail::Loss::FromModels(models_, x.data());

    const double l = lbfgs(x.data());

    detail::Loss::ToModels(x.data(), models_);

    return l;
}

void Mixture::convolve(const HOGPyramid & pyramid, const FeaturePyramid & fpyramid,
                       vector<vector<vector<double> > > & scores,
                       vector<vector<vector<Model::Position> > > * positions) const {
    if (empty() || pyramid.empty()) {
        scores.clear();
        if (positions)
            positions->clear();

        return;
    }
    const int nbModels = static_cast<int>(models_.size());
    scores.resize(nbModels);
    if(positions)
        positions->resize(nbModels);

#ifndef FFLD_MIXTURE_STANDARD_CONVOLUTION
    #pragma omp critical
    if(!cached_)
        cacheFilters();
    // Create a patchwork
    const Patchwork patchwork(pyramid);

    // Convolve the patchwork with the filters
    vector<vector<HOGPyramid::Matrix> > convolutions(filterCache_.size());
    patchwork.convolve(filterCache_, convolutions);

    // In case of error
    if (convolutions.empty()) {
        scores.clear();
        cerr << "Patchwork Convolution Failed" << endl;
        if (positions)
            positions->clear();

        return;
    }
    vector<int> offsets(nbModels);

    for (int i = 0, j = 0; i < nbModels; ++i) {
        offsets[i] = j;
        j += models_[i].parts().size();
    }

    // For each model
    #pragma omp parallel for
    for (int i = 0; i < nbModels; ++i) {
        vector<vector<HOGPyramid::Matrix> > tmp(models_[i].parts().size());

        for (int j = 0; j < tmp.size(); ++j)
            tmp[j].swap(convolutions[offsets[i] + j]);

        models_[i].convolve(pyramid, fpyramid, scores[i], positions ? &(*positions)[i] : 0, &tmp);
    }

    // In case of error
    for (int i = 0; i < nbModels; ++i) {
        if (scores[i].empty()) {
            scores.clear();
            cerr << "Patchwork Convolution Failed" << endl;
            if (positions)
                positions->clear();
        }
    }
#endif
}

ostream & FFLD::operator<<(ostream & os, const Mixture & mixture)
{
    // Save the number of models (mixture components)
    os << mixture.models().size() << endl;

    // Save the models themselves
    for (int i = 0; i < mixture.models().size(); ++i)
        os << mixture.models()[i] << endl;

    return os;
}

istream & FFLD::operator>>(istream & is, Mixture & mixture)
{
    int nbModels;

    is >> nbModels;

    if (!is || (nbModels <= 0)) {
        mixture = Mixture();
        return is;
    }

    vector<Model> models(nbModels);

    for (int i = 0; i < nbModels; ++i) {
        is >> models[i];

        if (!is || models[i].empty()) {
            mixture = Mixture();
            return is;
        }
    }

    mixture.models().swap(models);

    return is;
}
