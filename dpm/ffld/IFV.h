#ifndef IFV_H
#define IFV_H

//#include "dsift.h"
//#include "fisher.h"
#include <iostream>
#include <vector>
#include <Eigen/Core>
#include "FeaturePyramid.h"

namespace FFLD
{
 class IFV {
 public:
   static const int NbFeatures = 64;
   static const int NbClusters = 200;
   static const int NbClustersX2 = 400;
   typedef Eigen::Matrix<float, NbClusters * 2, NbFeatures, Eigen::RowMajor> Level;
   IFV(){}
   IFV(std::vector<FeaturePyramid::Level> patches);
   
   void convolve(std::vector<FeaturePyramid::Level> patches);
 private:
    //std::vector<int> dims_;
   
 };
  
}
#endif