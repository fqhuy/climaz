#include "FeaturePyramid.h"
//#include "dsift.h"

#include <iostream>
#include <limits>
#include "cnpy/cnpy.h"
#include <string>
#include <boost/lexical_cast.hpp>

using namespace Eigen;
using namespace FFLD;
using namespace std;

FeaturePyramid::FeaturePyramid(): padx_(1), pady_(1), interval_(5), step_(0), bin_size_(0)
{

}

FeaturePyramid::~FeaturePyramid()
{
    //for(int i = 0; i < levels_.size(); ++i){


    //}
}

FeaturePyramid::FeaturePyramid(string filename, int padx, int pady, int interval, int step, int bin_size)
    : padx_(padx),    pady_(pady), interval_(interval), step_(step), bin_size_(bin_size)
{
    string root = filename.substr(0,filename.find(".jpg"));
    vector<string> files;
    for(int i = 0; i < 1; i++) {
        files.push_back(root + "_l" + boost::lexical_cast<string>(i) + "_ifv_65.npy");
    }

    for(int i =0; i < files.size(); i++) {
        string filename = files[i];
        cnpy::NpyArray arr = cnpy::npy_load(filename);
        const int rows = arr.shape[0];
        const int cols = arr.shape[1];
        //double * data = (double *) arr.data;
		float * data = (float *) arr.data;

        //if(arr.shape.size() == 3) {
        //Cell zero(25600);
        FeaturePyramid::Level level = Level::Constant(rows,cols, Cell::Zero());
        for(int y = 0; y < rows; ++y) {
            for(int x = 0; x < cols; ++x) {
                for(int z = 0; z < FeaturePyramid::NbFeatures; ++z) {
                    //Eigen::Map<FeaturePyramid::Cell, Aligned > m( &data[y * rows * cols + x * cols] , NbFeatures, 1) ;
					level(y,x)(z) = (float) data[y * cols * FeaturePyramid::NbFeatures + x * FeaturePyramid::NbFeatures + z];
                }
            }
        }
        levels_.push_back(level);
        //}
        arr.destruct();

    }
}

FeaturePyramid::FeaturePyramid(const JPEGImage& image, int padx, int pady, int interval, int step, int bin_size): padx_(padx),
    pady_(pady), interval_(interval), step_(step), bin_size_(bin_size)
{

}


void FeaturePyramid::SIFT(const JPEGImage& image, FeaturePyramid::Level& level, int padx, int pady)
{
    /*
    VlDsiftFilter* dsift = vl_dsift_new_basic(image.width(),image.height(),step_, bin_size_);
    vl_dsift_process(dsift,image.bits());
    int size = vl_dsift_get_descriptor_size(dsift);
    //	float * features = new float[size * 128];
    float * features  = vl_dsift_get_descriptors(dsift);
    vl_dsift_delete(dsift);
    */
}

void FeaturePyramid::convolve(const FeaturePyramid::Cell& filter, vector< FeaturePyramid::Matrix >& convolutions) const
{
    convolutions.resize(levels_.size());

    //#pragma omp parallel for
    for (int i = 0; i < levels_.size(); ++i)
        Convolve(levels_[i], filter, convolutions[i]);
}


void FeaturePyramid::Convolve(const FeaturePyramid::Level& x, const FeaturePyramid::Cell& y, FeaturePyramid::Matrix& z)
{
    // Nothing to do if x is smaller than y
    //if ((x.rows() < y.rows()) || (x.cols() < y.cols())) {
    //    z = Matrix();
    //    return;
    //}

    //z = Matrix::Zero(x.rows() - y.rows() + 1, x.cols() - y.cols() + 1);
    z = Matrix::Zero(x.rows() , x.cols() );
    #pragma omp parallel for
    for(int i = 0; i < z.rows(); ++i) {
        for(int j = 0; j < z.cols(); ++j) {
            /*
			float score = 0;
            for(int k = 0; k < NbFeatures; k++) {
                score += x(i,j)(k) * y(k);
            }*/
            z(i,j) += x(i,j).dot(y);
        }
    }
    /*
    for (int i = 0; i < z.rows(); ++i) {
        for (int j = 0; j < y.rows(); ++j) {
            const Eigen::Map<const Matrix, Aligned, OuterStride<NbFeatures> >
            mapx(reinterpret_cast<const Scalar *>(x.row(i + j).data()), z.cols(),
                 y.cols() * NbFeatures);
            const Eigen::Map<const RowVectorXd, Aligned>
            mapy(reinterpret_cast<const Scalar *>(y.row(j).data()), y.cols() * NbFeatures);

            z.row(i).noalias() += mapy * mapx.transpose();
        }
    }
    */
}

FeaturePyramid::Level FeaturePyramid::Flip(const FeaturePyramid::Level& level)
{
	/*
    // Symmetric features
	const int symmetry[FeaturePyramid::NbFeatures] =
    {
        9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 17, 16, 15, 14, 13, 12, 11, 10, // Contrast-sensitive
        18, 26, 25, 24, 23, 22, 21, 20, 19, // Contrast-insensitive
        28, 27, 30, 29, // Texture
#ifndef FFLD_HOGPYRAMID_EXTRA_FEATURES
        31 // Truncation
#else
        31, 32, 33, 34, 35, 36, 37, 38, 39, 40, // Uniform LBP
        41, 42, 43, 44, 45, 46, // Color
        47 // Truncation
#endif
    };

    // Symmetric filter
    FeaturePyramid::Level result(level.rows(), level.cols());

    for (int y = 0; y < level.rows(); ++y)
        for (int x = 0; x < level.cols(); ++x)
            for (int i = 0; i < NbFeatures; ++i)
                result(y, x)(i) = level(y, level.cols() - 1 - x)(symmetry[i]);
			*/ 
	FeaturePyramid::Level result(level.rows(), level.cols());
    return result;
}


Eigen::Map< FeaturePyramid::Matrix, Eigen::Aligned > FeaturePyramid::Map(FeaturePyramid::Level& level)
{
    return Eigen::Map<Matrix, Aligned>(level.data()->data(), level.rows(),
									   level.cols() * FeaturePyramid::NbFeatures);
}

const Eigen::Map< const FeaturePyramid::Matrix, Eigen::Aligned > FeaturePyramid::Map(const FeaturePyramid::Level& level)
{
    return Eigen::Map<const Matrix, Aligned>(level.data()->data(), level.rows(),
											 level.cols() * FeaturePyramid::NbFeatures);
}

const vector< FeaturePyramid::Level >& FeaturePyramid::levels() const
{
    return levels_;
}


