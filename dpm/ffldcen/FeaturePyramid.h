#ifndef FFLD_SIFTPYRAMID_H
#define FFLD_SIFTPYRAMID_H

#include "JPEGImage.h"
#include "Eigen/Core"
#include <string>
#include <vector>

namespace FFLD
{
class FeaturePyramid {
public:
    static const int NbClustersX2 = 400;   
    static const int NbFeatures = 8 * NbClustersX2;
    static const int NbClusters = 200;
       
    typedef float Scalar;

    /// Type of a matrix.
    typedef Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;

    /// Type of a pyramid level cell (fixed-size array of length NbFeatures).
    typedef Eigen::Matrix<Scalar, NbFeatures, 1> Cell;

    /// Type of a pyramid level (matrix of cells).
    typedef Eigen::Matrix<Cell, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Level;
    
    FeaturePyramid();
    
    ~FeaturePyramid();
    
    FeaturePyramid(std::string filename, int padx = 1, int pady = 1, int interval = 5, int step = 8, int bin_size = 5);
    
    /// Constructs a pyramid from the JPEGImage of a Scene.
    /// @param[in] image The JPEGImage of the Scene.
    /// @param[in] padx Amount of horizontal zero padding (in cells).
    /// @param[in] pady Amount of vertical zero padding (in cells).
    /// @param[in] step Number of levels per octave in the pyramid.
    /// @param[in] bin_size Bin size for the dense SIFT
    /// @note The amount of padding and the interval should be at least 1.
    
    FeaturePyramid(const JPEGImage & image, int padx, int pady, int interval, int step,int bin_size);
	
    /// Returns whether the pyramid is empty. An empty pyramid has no level.
	inline bool empty() const {	return levels().empty();}
    
    /// Returns the amount of horizontal zero padding (in cells).
    inline int padx() const {return padx_;}
    
    /// Returns the amount of vertical zero padding (in cells).
    inline int pady() const {return pady_;}
    
    /// Returns the number of levels per octave in the pyramid.
    inline int interval() const {return interval_;}
    
    inline int step() const {return step_;}
    
    inline int bin_size() const {return bin_size_;}
    
    /// Returns the pyramid levels.
    /// @note Scales are given by the following formula: 2^(1 - @c index / @c interval).
    const std::vector<Level> & levels() const;
    
    /// Returns the convolutions of the pyramid with a filter.
    /// @param[in] filter Filter.
    /// @param[out] convolutions Convolution of each level.
    void convolve(const Cell & filter, std::vector<Matrix> & convolutions) const;
    
    /// Returns the flipped version (horizontally) of a level.
    static FeaturePyramid::Level Flip(const FeaturePyramid::Level & level);
    
    /// Maps a pyramid level to a simple matrix (useful to apply standard matrix operations to it).
    /// @note The size of the matrix will be rows x (cols * NbFeatures).
    static Eigen::Map<Matrix, Eigen::Aligned> Map(Level & level);
    
    /// Maps a const pyramid level to a simple const matrix (useful to apply standard matrix
    /// operations to it).
    /// @note The size of the matrix will be rows x (cols * NbFeatures).
    static const Eigen::Map<const Matrix, Eigen::Aligned> Map(const Level & level);    

protected:
    // Computes the 2D convolution of a pyramid level with a filter
    static void Convolve(const Level & x, const Cell & y, Matrix & z);
private:
    void SIFT(const JPEGImage & image, Level & level, int padx = 1, int pady = 1);
    
    int padx_;
    int pady_;
    int interval_;
    int bin_size_;
    int step_;
    std::vector<Level> levels_;
};
}

#endif
