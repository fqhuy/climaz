#include "SimpleOpt.h"
#include "Mixture.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <math.h>
#include "cnpy/cnpy.h"
#include <cstdlib>
#include "FeaturePyramid.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace FFLD;
using namespace std;

// SimpleOpt array of valid options
enum
{
    OPT_C, OPT_DATAMINE, OPT_INTERVAL, OPT_HELP, OPT_J, OPT_RELABEL, OPT_MODEL, OPT_NAME,
    OPT_PADDING, OPT_RESULT, OPT_SEED, OPT_OVERLAP, OPT_NB_COMP, OPT_NB_NEG, OPT_NB_TRAIN
};

CSimpleOpt::SOption SOptions[] =
{
    { OPT_C, "-c", SO_REQ_SEP },
    { OPT_C, "--C", SO_REQ_SEP },
    { OPT_DATAMINE, "-d", SO_REQ_SEP },
    { OPT_DATAMINE, "--datamine", SO_REQ_SEP },
    { OPT_INTERVAL, "-e", SO_REQ_SEP },
    { OPT_INTERVAL, "--interval", SO_REQ_SEP },
    { OPT_HELP, "-h", SO_NONE },
    { OPT_HELP, "--help", SO_NONE },
    { OPT_J, "-j", SO_REQ_SEP },
    { OPT_J, "--J", SO_REQ_SEP },
    { OPT_RELABEL, "-l", SO_REQ_SEP },
    { OPT_RELABEL, "--relabel", SO_REQ_SEP },
    { OPT_MODEL, "-m", SO_REQ_SEP },
    { OPT_MODEL, "--model", SO_REQ_SEP },
    { OPT_NAME, "-n", SO_REQ_SEP },
    { OPT_NAME, "--name", SO_REQ_SEP },
    { OPT_PADDING, "-p", SO_REQ_SEP },
    { OPT_PADDING, "--padding", SO_REQ_SEP },
    { OPT_RESULT, "-r", SO_REQ_SEP },
    { OPT_RESULT, "--result", SO_REQ_SEP },
    { OPT_SEED, "-s", SO_REQ_SEP },
    { OPT_SEED, "--seed", SO_REQ_SEP },
    { OPT_OVERLAP, "-v", SO_REQ_SEP },
    { OPT_OVERLAP, "--overlap", SO_REQ_SEP },
    { OPT_NB_COMP, "-x", SO_REQ_SEP },
    { OPT_NB_COMP, "--nb-components", SO_REQ_SEP },
    { OPT_NB_NEG, "-z", SO_REQ_SEP },
    { OPT_NB_NEG, "--nb-negatives", SO_REQ_SEP },
    { OPT_NB_TRAIN, "-t", SO_REQ_SEP },
    { OPT_NB_TRAIN, "--nb-train", SO_REQ_SEP },
    SO_END_OF_OPTIONS
};

void showUsage()
{
    cout << "Usage: train [options] possitives.txt negatives.txt\n\n"
         "Options:\n"
         "  -c,--C <arg>             SVM regularization constant (default 0.002)\n"
         "  -d,--datamine <arg>      Maximum number of data-mining iterations within each "
         "training iteration  (default 10)\n"
         "  -e,--interval <arg>      Number of levels per octave in the HOG pyramid (default 5)"
         "\n"
         "  -h,--help                Display this information\n"
         "  -j,--J <arg>             SVM positive regularization constant boost (default 2)\n"
         "  -l,--relabel <arg>       Maximum number of training iterations (default 8, half if "
         "no part)\n"
         "  -m,--model <file>        Read the initial model from <file> (default zero model)\n"
         "  -n,--name <arg>          Name of the object to detect (default \"person\")\n"
         "  -p,--padding <arg>       Amount of zero padding in HOG cells (default 6)\n"
         "  -r,--result <file>       Write the trained model to <file> (default \"model.txt\")\n"
         "  -s,--seed <arg>          Random seed (default time(NULL))\n"
         "  -v,--overlap <arg>       Minimum overlap in latent positive search (default 0.7)\n"
         "  -x,--nb-components <arg> Number of mixture components (without symmetry, default 3)\n"
         "  -z,--nb-negatives <arg>  Maximum number of negative images to consider (default all)\n"
         "  -t,--nb-train <arg>	     Number of training samples"
         << endl;
}

// Train a mixture model
int main(int argc, char * argv[])
{
    // Default parameters
	// C = 0.1, bias = 0.0 Working Best So far for denim.
    double C = 0.1;//0.002
    int nbDatamine = 10;//10
    int interval = 5;
    double J = 2.0;
    int nbRelabel = 10;//8
    string model;
    Object::Name name = Object::UNKNOWN;
    string sname = "";
    int padding = 1;
    string result("model.txt");
    int seed = static_cast<int>(time(0));
    double overlap = 0.7;
    int nbComponents = 10;
    int nbTrain = 40; //40
    int nbNegativeScenes = 320;//200;

    // Parse the parameters
    CSimpleOpt args(argc, argv, SOptions);

    while (args.Next()) {
        if (args.LastError() == SO_SUCCESS) {
            if (args.OptionId() == OPT_C) {
                C = atof(args.OptionArg());

                if (C <= 0) {
                    showUsage();
                    cerr << "\nInvalid C arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_DATAMINE) {
                nbDatamine = atoi(args.OptionArg());

                if (nbDatamine <= 0) {
                    showUsage();
                    cerr << "\nInvalid datamine arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_INTERVAL) {
                interval = atoi(args.OptionArg());

                if (interval <= 0) {
                    showUsage();
                    cerr << "\nInvalid interval arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_HELP) {
                showUsage();
                return 0;
            }
            else if (args.OptionId() == OPT_J) {
                J = atof(args.OptionArg());

                if (J <= 0) {
                    showUsage();
                    cerr << "\nInvalid J arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_RELABEL) {
                nbRelabel = atoi(args.OptionArg());

                if (nbRelabel <= 0) {
                    showUsage();
                    cerr << "\nInvalid relabel arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_MODEL) {
                model = args.OptionArg();
            }
            else if (args.OptionId() == OPT_NAME) {
                string arg = args.OptionArg();
                transform(arg.begin(), arg.end(), arg.begin(), static_cast<int (*)(int)>(tolower));
                sname = arg;
            }
            else if (args.OptionId() == OPT_PADDING) {
                padding = atoi(args.OptionArg());

                if (padding <= 1) {
                    showUsage();
                    cerr << "\nInvalid padding arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_RESULT) {
                result = args.OptionArg();
            }
            else if (args.OptionId() == OPT_SEED) {
                seed = atoi(args.OptionArg());
            }
            else if (args.OptionId() == OPT_OVERLAP) {
                overlap = atof(args.OptionArg());

                if ((overlap <= 0.0) || (overlap >= 1.0)) {
                    showUsage();
                    cerr << "\nInvalid overlap arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_NB_COMP) {
                nbComponents = atoi(args.OptionArg());

                if (nbComponents <= 0) {
                    showUsage();
                    cerr << "\nInvalid nb-components arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_NB_NEG) {
                nbNegativeScenes = atoi(args.OptionArg());

                if (nbNegativeScenes < 0) {
                    showUsage();
                    cerr << "\nInvalid nb-negatives arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_NB_TRAIN) {
                nbTrain = atoi(args.OptionArg());
                if(nbTrain < 5) {
                    showUsage();
                    cerr << "\nInvalid number of training samples (at least 5)" << endl;
                    return -1;
                }
            }
        }
        else {
            showUsage();
            cerr << "\nUnknown option " << args.OptionText() << endl;
            return -1;
        }
    }

    if(sname == "") {
        showUsage();
        cerr << "\nObject name is not specified " << endl;
        return -1;
    }

    srand(seed);
    srand48(seed);

    if (!args.FileCount()) {
        showUsage();
        cerr << "\nNo dataset provided" << endl;
        return -1;
    }
    else if (args.FileCount() > 2) {
        showUsage();
        cerr << "\nMore than one dataset provided" << endl;
        return -1;
    }


    // Open the image set file
    const string root(args.File(0));
    const string oroot = root + "_OUTPUT";
    const string file(root + "/info.txt");
    const string neg_file(root + "/negatives_landscape.txt");

    ifstream in(file.c_str());

    if (!in.is_open()) {
        showUsage();
        cerr << "\nInvalid image set file " << file << endl;
        return -1;
    }

    // Find the annotations' folder (not sure that will work under Windows)
    // Load all the scenes
    int maxRows = 0;
    int maxCols = 0;

    vector<Scene> scenes;
    vector<string> names;

    while (in) {
        string line;
        getline(in, line);
        if(line != "")
            names.push_back(line);
    }

    in.close();
    Object::NameMap.setNames(names);
    vector<string> poses;
    poses.push_back(string("Frontal"));
    poses.push_back(string("Back"));
    Object::PoseMap.setNames(poses);

    vector<string>::iterator it = names.begin();

    map<string, string> pos2neg;

    //USE IMAGES FROM OTHER CLASSES AS NEGATIVES
    //"knit1 fleece fur denim corduroy lace leather boucle";
    pos2neg["knit1"] = "fleece fur denim corduroy lace leather boucle";
    pos2neg["fleece"] = "knit1 fur denim corduroy lace leather boucle";
	pos2neg["corduroy"] = "denim leather fleece knit1 fur lace boucle";
    pos2neg["denim"] = "leather corduroy knit1 fleece fur lace boucle";
    pos2neg["fur"] = "knit1 fleece denim corduroy lace leather boucle";
    pos2neg["lace"] = "knit1 fleece fur denim corduroy leather boucle";
    pos2neg["leather"] = "knit1 fleece fur denim corduroy lace boucle";
    pos2neg["boucle"] = "knit1 fleece fur denim corduroy lace leather";

    //READ POSITIVES ONLY
    while (it != names.end()) {
        if((*it) == sname) {
            ifstream in1((root + "/" +  (*it) + ".txt").c_str());
            if (!in1.is_open()) {
                cerr << "Cannot open class file " << (root + "/" + (*it) + ".txt") << endl;
            } else {
                int count = 0;
                while(in1 && count < nbTrain) {
                    // Check whether the scene is positive or negative
                    string line1;
                    getline(in1,line1);

                    if(line1 != "") {
                        const Scene scene(line1, 500, 500, 3, root);

                        if (scene.empty())
                            continue;

                        scenes.push_back(scene);

                        maxRows = max(maxRows, (scene.height() + 3) / 4 + padding);
                        maxCols = max(maxCols, (scene.width() + 3) / 4 + padding);

                    } else {
                        cerr << "empty line: " << count << "|" << (root + "/" + (*it) + ".txt") << endl;
                    }
                    ++count;
                }
            }
            in1.close();
        }
        ++it;

    }

    //READ NEGATIVES
    vector<string> negatives;
    boost::split(negatives, pos2neg[sname], boost::is_any_of(" "));

    it = negatives.begin();
    while (it != negatives.end()) {
		cout << "reading .." << (*it) << " ";
        //if((*it) != sname && pos2neg[sname].find((*it)) != string::npos) {
        ifstream in1((root + "/" +  (*it) + ".txt").c_str());
        if (!in1.is_open()) {
            cerr << "Cannot open class file " << (root + "/" + (*it) + ".txt") << endl;
        } else {
            int count = 0;
            while(in1 && count < nbTrain) {
                string line1;
                getline(in1,line1);

                if(line1 != "") {
                    //true now just means having an object (unknown name)
                    const Scene scene(line1, 500, 500, 3, root, "CLZ", true);

                    if (scene.empty())
                        continue;

                    if (nbNegativeScenes) {
                        scenes.push_back(scene);
                        maxRows = max(maxRows, (scene.height() + 3) / 4 + padding);
                        maxCols = max(maxCols, (scene.width() + 3) / 4 + padding);
                        --nbNegativeScenes;
                    }
                } else {
                    cerr << "empty line: " << count << "|" << (root + "/" + (*it) + ".txt") << endl;
                }
                ++count;
            }
        }
        in1.close();
        //}
        ++it;

    }

    //READ EXTRA NEGATIVES

    {
        ifstream nin(neg_file.c_str());

        if (!nin.is_open()) {
            showUsage();
            cerr << "\nInvalid negative set " << neg_file << endl;
            return -1;
        }

        while (nin) {

            string line;
            getline(nin, line);
            if(line != "" && nbNegativeScenes > 0) {
                //JPEGImage img(line);
                const Scene scene(line,500, 500, 3, root, "CLZ",false);

                scenes.push_back(scene);
                --nbNegativeScenes;
            }
        }
        nin.close();
    }

    //Actually set the name 'code' here.
    name = Object::NameMap[sname];

    // Initialize the Patchwork class
    if (!Patchwork::InitFFTW((maxRows + 15) & ~15, (maxCols + 15) & ~15)) {
        cerr << "Error initializing the FFTW library" << endl;
        return - 1;
    }

    vector<pair<int, int> > rootSizes;
    for(int i = 0; i < 10; ++i)
        rootSizes.push_back(make_pair<int,int>(8,8));

    // The mixture to train
    Mixture mixture(nbComponents, scenes, name, rootSizes);
    //mixture.setInitPositives(labels);
    mixture.iRoot = root;
    mixture.oRoot = oroot;
    result = sname + "_model.txt";

    if (mixture.empty()) {
        cerr << "Error initializing the mixture model" << endl;
        return -1;
    }



    if (model.empty())
        mixture.train(scenes, name, padding, padding, interval, nbRelabel, nbDatamine, 12000, C,
                      J, overlap);
    // Try to open the result file
    ofstream out(result.c_str(), ios::binary);

    if (!out.is_open()) {
        showUsage();
        cerr << "\nInvalid result file " << result << endl;
        cout << mixture << endl; // Print the mixture as a last resort
        return -1;
    }

    out << mixture;

    return EXIT_SUCCESS;
}

