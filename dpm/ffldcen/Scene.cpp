//--------------------------------------------------------------------------------------------------
// Implementation of the papers "Exact Acceleration of Linear Object Detectors", 12th European
// Conference on Computer Vision, 2012 and "Deformable Part Models with Individual Part Scaling",
// 24th British Machine Vision Conference, 2013.
//
// Copyright (c) 2013 Idiap Research Institute, <http://www.idiap.ch/>
// Written by Charles Dubout <charles.dubout@idiap.ch>
//
// This file is part of FFLDv2 (the Fast Fourier Linear Detector version 2)
//
// FFLDv2 is free software: you can redistribute it and/or modify it under the terms of the GNU
// Affero General Public License version 3 as published by the Free Software Foundation.
//
// FFLDv2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with FFLDv2. If
// not, see <http://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------------------------------

#include "Scene.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include "JPEGImage.h"
#include "cnpy/cnpy.h"
#include <boost/algorithm/string.hpp>
#include <vector>
//#include <libxml/parser.h>

using namespace FFLD;
using namespace std;
using namespace boost;


Scene::Scene() : width_(0), height_(0), depth_(0)
{
}

Scene::Scene(int width, int height, int depth, const string & filename,
             const vector<Object> & objects) : width_(width), height_(height), depth_(depth),
    filename_(filename), objects_(objects)
{
}

bool Scene::empty() const
{
    return ((width() <= 0) || (height() <= 0) || (depth() <= 0) || filename().empty()) &&
           objects().empty();
}
/*
template <typename Result>
static inline Result content(const xmlNodePtr cur)
{
    if ((cur == NULL) || (cur->xmlChildrenNode == NULL))
        return Result();

    istringstream iss(reinterpret_cast<const char *>(cur->xmlChildrenNode->content));
    Result result;
    iss >> result;
    return result;
}
*/
Scene::Scene(const string& filename, const int width, const int height, const int depth, const string& root, const string& filetype, const bool positive):
    root_(root), filename_(filename), width_(width), height_(height), depth_(depth)
{
    /*
    const string Names[10] =
    {
        "boucle","chiffon","corduroy","denim","fleece","fur","knit1","lace","leather","satin"
    };

    const string Poses[4] =
    {
        "Frontal", "Back"
    };
    */
    if(filetype == "CLZ") {
        string str = filename;
        if(filename.find(".txt") != filename.npos)
            str = filename.substr(0,filename.find(".txt") ) + ".jpg";
        this->setFilename(str);
        /*
        string str = filename;
        if(filename.find(".txt") != filename.npos)
        str = filename.substr(0,filename.find(".txt") ) + ".jpg";
        this->setFilename(str);
        JPEGImage img = JPEGImage(str);
        this->setWidth(img.width());
        this->setHeight(img.height());
        this->setDepth(img.depth());
        */

        if(positive) {
            vector<string> vec;
            split(vec,filename,is_any_of("/"));
            string object_name = vec[vec.size()-2];
            const int scale = 1000 / width;

            std::ifstream ifs;
            const string fullpath = root + "/" + filename ;

            string labelfile =  root + "_OUTPUT/" + filename.substr(0,filename.find(".txt") ) + "_parts_labels.npy";
			cnpy::NpyArray arr = cnpy::npy_load(labelfile);
            double* labels = (double*) arr.data;

            ifs.open (fullpath.c_str(), std::ifstream::in);
            if (!ifs.is_open()) {

                cerr << "\nInvalid image set file " << filename << endl;
                return;
            }

            string line;
            std::getline(ifs,line); //Throw away the root.
            initCluster_ = labels[0];
            for(int i = 0; i < 20; i++) {
				//texture
				/*
				string texfile = root + "_OUTPUT/" + filename;
				const int lastDot = texfile.find_last_of(".");
				texfile.replace(lastDot, texfile.length(), "_l0_ifv_65.npy");
				cnpy::NpyArray arr = cnpy::npy_load(texfile);
				float* arr_data = (float *) arr.data;
				Eigen::Map<FeaturePyramid::Cell> tex(arr_data);
				objects_.back().setTexture(tex);
				*/
				
				Rectangle bndbox;
                objects_.push_back(Object());
                objects_.back().setName(Object::NameMap[object_name.c_str()]);
                objects_.back().setPose(Object::PoseMap["Frontal"]);
                //string line;
                vector<string> vec1;
                std::getline(ifs,line);
                split(vec1, line, is_any_of(" "));
                //cout << vec1[0] << " " << vec1[1] << " " << vec1[2] << " " << vec1[3] << endl;
                Object::Part part;
                //TODO: padding & scale here!
                part << atoi(vec1[0].c_str())/scale,
                     atoi(vec1[1].c_str())/scale,
                     atoi(vec1[2].c_str())/scale - atoi(vec1[0].c_str())/scale,
                     atoi(vec1[3].c_str())/scale - atoi(vec1[1].c_str())/scale;

                objects_.back().parts().push_back(part);
                //if( i==0)
                bndbox = (Rectangle(atoi(vec1[0].c_str())/scale,atoi(vec1[1].c_str())/scale , \
                                        atoi(vec1[2].c_str())/scale - atoi(vec1[0].c_str())/scale, \
                                        atoi(vec1[3].c_str())/scale - atoi(vec1[1].c_str())/scale));

                objects_.back().setBndbox(bndbox);
                objects_.back().setTruncated(false);
                objects_.back().setDifficult(false);
                objects_.back().setInitCluster(labels[i + 1]);
            }
            arr.destruct();
            ifs.close();
        }

        if(!positive) {

        }
    }
}

int Scene::width() const
{
    return width_;
}

void Scene::setWidth(int width)
{
    width_ = width;
}

int Scene::height() const
{
    return height_;
}

void Scene::setHeight(int height)
{
    height_ = height;
}

int Scene::depth() const
{
    return depth_;
}

void Scene::setDepth(int depth)
{
    depth_ = depth;
}

const string & Scene::filename() const
{
    return filename_;
}

void Scene::setFilename(const string &filename)
{
    filename_ = filename;
}

const vector<Object> & Scene::objects() const
{
    return objects_;
}

void Scene::setObjects(const vector<Object> &objects)
{
    objects_ = objects;
}

ostream & FFLD::operator<<(ostream & os, const Scene & scene)
{
    os << scene.width() << ' ' << scene.height() << ' ' << scene.depth() << ' '
       << scene.objects().size() << ' ' << scene.filename() << endl;

    for (int i = 0; i < scene.objects().size(); ++i)
        os << scene.objects()[i] << endl;

    return os;
}

istream & FFLD::operator>>(istream & is, Scene & scene)
{
    int width, height, depth, nbObjects;

    is >> width >> height >> depth >> nbObjects;
    is.get(); // Remove the space

    string filename;
    getline(is, filename);

    vector<Object> objects(nbObjects);

    for (int i = 0; i < nbObjects; ++i)
        is >> objects[i];

    if (!is) {
        scene = Scene();
        return is;
    }

    scene = Scene(width, height, depth, filename, objects);

    return is;
}
