//--------------------------------------------------------------------------------------------------
// Implementation of the papers "Exact Acceleration of Linear Object Detectors", 12th European
// Conference on Computer Vision, 2012 and "Deformable Part Models with Individual Part Scaling",
// 24th British Machine Vision Conference, 2013.
//
// Copyright (c) 2013 Idiap Research Institute, <http://www.idiap.ch/>
// Written by Charles Dubout <charles.dubout@idiap.ch>
//
// This file is part of FFLDv2 (the Fast Fourier Linear Detector version 2)
//
// FFLDv2 is free software: you can redistribute it and/or modify it under the terms of the GNU
// Affero General Public License version 3 as published by the Free Software Foundation.
//
// FFLDv2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with FFLDv2. If
// not, see <http://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------------------------------

#include "SimpleOpt.h"

#include "Mixture.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <math.h>
#include "cnpy/cnpy.h"
//#include <Eigen/Core>
#include <cstdlib>
#include "FeaturePyramid.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
//#include "Logging.h"

using namespace FFLD;
using namespace std;

// SimpleOpt array of valid options
enum
{
    OPT_C, OPT_DATAMINE, OPT_INTERVAL, OPT_HELP, OPT_J, OPT_RELABEL, OPT_MODEL, OPT_NAME,
    OPT_PADDING, OPT_RESULT, OPT_SEED, OPT_OVERLAP, OPT_NB_COMP, OPT_NB_NEG, OPT_NB_TRAIN
};

CSimpleOpt::SOption SOptions[] =
{
    { OPT_C, "-c", SO_REQ_SEP },
    { OPT_C, "--C", SO_REQ_SEP },
    { OPT_DATAMINE, "-d", SO_REQ_SEP },
    { OPT_DATAMINE, "--datamine", SO_REQ_SEP },
    { OPT_INTERVAL, "-e", SO_REQ_SEP },
    { OPT_INTERVAL, "--interval", SO_REQ_SEP },
    { OPT_HELP, "-h", SO_NONE },
    { OPT_HELP, "--help", SO_NONE },
    { OPT_J, "-j", SO_REQ_SEP },
    { OPT_J, "--J", SO_REQ_SEP },
    { OPT_RELABEL, "-l", SO_REQ_SEP },
    { OPT_RELABEL, "--relabel", SO_REQ_SEP },
    { OPT_MODEL, "-m", SO_REQ_SEP },
    { OPT_MODEL, "--model", SO_REQ_SEP },
    { OPT_NAME, "-n", SO_REQ_SEP },
    { OPT_NAME, "--name", SO_REQ_SEP },
    { OPT_PADDING, "-p", SO_REQ_SEP },
    { OPT_PADDING, "--padding", SO_REQ_SEP },
    { OPT_RESULT, "-r", SO_REQ_SEP },
    { OPT_RESULT, "--result", SO_REQ_SEP },
    { OPT_SEED, "-s", SO_REQ_SEP },
    { OPT_SEED, "--seed", SO_REQ_SEP },
    { OPT_OVERLAP, "-v", SO_REQ_SEP },
    { OPT_OVERLAP, "--overlap", SO_REQ_SEP },
    { OPT_NB_COMP, "-x", SO_REQ_SEP },
    { OPT_NB_COMP, "--nb-components", SO_REQ_SEP },
    { OPT_NB_NEG, "-z", SO_REQ_SEP },
    { OPT_NB_NEG, "--nb-negatives", SO_REQ_SEP },
    { OPT_NB_TRAIN, "-t", SO_REQ_SEP },
    { OPT_NB_TRAIN, "--nb-train", SO_REQ_SEP },
    SO_END_OF_OPTIONS
};

void showUsage()
{
    cout << "Usage: train [options] possitives.txt negatives.txt\n\n"
         "Options:\n"
         "  -c,--C <arg>             SVM regularization constant (default 0.002)\n"
         "  -d,--datamine <arg>      Maximum number of data-mining iterations within each "
         "training iteration  (default 10)\n"
         "  -e,--interval <arg>      Number of levels per octave in the HOG pyramid (default 5)"
         "\n"
         "  -h,--help                Display this information\n"
         "  -j,--J <arg>             SVM positive regularization constant boost (default 2)\n"
         "  -l,--relabel <arg>       Maximum number of training iterations (default 8, half if "
         "no part)\n"
         "  -m,--model <file>        Read the initial model from <file> (default zero model)\n"
         "  -n,--name <arg>          Name of the object to detect (default \"person\")\n"
         "  -p,--padding <arg>       Amount of zero padding in HOG cells (default 6)\n"
         "  -r,--result <file>       Write the trained model to <file> (default \"model.txt\")\n"
         "  -s,--seed <arg>          Random seed (default time(NULL))\n"
         "  -v,--overlap <arg>       Minimum overlap in latent positive search (default 0.7)\n"
         "  -x,--nb-components <arg> Number of mixture components (without symmetry, default 3)\n"
         "  -z,--nb-negatives <arg>  Maximum number of negative images to consider (default all)\n"
         "  -t,--nb-train <arg>	     Number of training samples"
         << endl;
}

// Train a mixture model
int main(int argc, char * argv[])
{
    // Default parameters
    double C = 1;//0.002
    int nbDatamine = 4;//10
    int interval = 5;
    double J = 2.0;
    int nbRelabel = 4;//8
    string model;
    Object::Name name = Object::UNKNOWN;
    string sname = "";
    int padding = 1;
    string result("model.txt");
    int seed = static_cast<int>(time(0));
    double overlap = 0.7;
    int nbComponents = 6;
    int nbTrain = 40; //40
    int nbNegativeScenes = 40;//200;

    // Parse the parameters
    CSimpleOpt args(argc, argv, SOptions);

    while (args.Next()) {
        if (args.LastError() == SO_SUCCESS) {
            if (args.OptionId() == OPT_C) {
                C = atof(args.OptionArg());

                if (C <= 0) {
                    showUsage();
                    cerr << "\nInvalid C arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_DATAMINE) {
                nbDatamine = atoi(args.OptionArg());

                if (nbDatamine <= 0) {
                    showUsage();
                    cerr << "\nInvalid datamine arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_INTERVAL) {
                interval = atoi(args.OptionArg());

                if (interval <= 0) {
                    showUsage();
                    cerr << "\nInvalid interval arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_HELP) {
                showUsage();
                return 0;
            }
            else if (args.OptionId() == OPT_J) {
                J = atof(args.OptionArg());

                if (J <= 0) {
                    showUsage();
                    cerr << "\nInvalid J arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_RELABEL) {
                nbRelabel = atoi(args.OptionArg());

                if (nbRelabel <= 0) {
                    showUsage();
                    cerr << "\nInvalid relabel arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_MODEL) {
                model = args.OptionArg();
            }
            else if (args.OptionId() == OPT_NAME) {
                string arg = args.OptionArg();
                transform(arg.begin(), arg.end(), arg.begin(), static_cast<int (*)(int)>(tolower));
                /*
                        const string Names[10] =
                        {
                            "boucle","chiffon","corduroy","denim","fleece","fur","knit1","lace","leather","satin"
                        };

                        const string * iter = find(Names, Names + 20, arg);

                        if (iter == Names + 20) {
                            showUsage();
                            cerr << "\nInvalid name arg " << args.OptionArg() << endl;
                            return -1;
                        }
                */
                sname = arg;//static_cast<Object::Name>(iter - Names);
            }
            else if (args.OptionId() == OPT_PADDING) {
                padding = atoi(args.OptionArg());

                if (padding <= 1) {
                    showUsage();
                    cerr << "\nInvalid padding arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_RESULT) {
                result = args.OptionArg();
            }
            else if (args.OptionId() == OPT_SEED) {
                seed = atoi(args.OptionArg());
            }
            else if (args.OptionId() == OPT_OVERLAP) {
                overlap = atof(args.OptionArg());

                if ((overlap <= 0.0) || (overlap >= 1.0)) {
                    showUsage();
                    cerr << "\nInvalid overlap arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_NB_COMP) {
                nbComponents = atoi(args.OptionArg());

                if (nbComponents <= 0) {
                    showUsage();
                    cerr << "\nInvalid nb-components arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_NB_NEG) {
                nbNegativeScenes = atoi(args.OptionArg());

                if (nbNegativeScenes < 0) {
                    showUsage();
                    cerr << "\nInvalid nb-negatives arg " << args.OptionArg() << endl;
                    return -1;
                }
            }
            else if (args.OptionId() == OPT_NB_TRAIN) {
                nbTrain = atoi(args.OptionArg());
                if(nbTrain < 5) {
                    showUsage();
                    cerr << "\nInvalid number of training samples (at least 5)" << endl;
                    return -1;
                }
            }
        }
        else {
            showUsage();
            cerr << "\nUnknown option " << args.OptionText() << endl;
            return -1;
        }
    }

    if(sname == "") {
        showUsage();
        cerr << "\nObject name is not specified " << endl;
        return -1;
    }

    srand(seed);
    srand48(seed);

    if (!args.FileCount()) {
        showUsage();
        cerr << "\nNo dataset provided" << endl;
        return -1;
    }
    else if (args.FileCount() > 2) {
        showUsage();
        cerr << "\nMore than one dataset provided" << endl;
        return -1;
    }


    // Open the image set file
    const string root(args.File(0));
    const string oroot = root + "_OUTPUT";
    const string file(root + "/info.txt");
    const string neg_file(root + "/negatives1.txt");

    ifstream in(file.c_str());

    if (!in.is_open()) {
        showUsage();
        cerr << "\nInvalid image set file " << file << endl;
        return -1;
    }

    // Find the annotations' folder (not sure that will work under Windows)
    //const string folder = file.substr(0, file.find_last_of("/\\")) + "/../../Annotations/";
    // Load all the scenes
    int maxRows = 0;
    int maxCols = 0;

    vector<Scene> scenes;
    vector<string> names;

    while (in) {
        string line;
        getline(in, line);
        if(line != "")
            names.push_back(line);
    }

    in.close();
    Object::NameMap.setNames(names);
    vector<string> poses;
    poses.push_back(string("Frontal"));
    poses.push_back(string("Back"));
    Object::PoseMap.setNames(poses);

    vector<string>::iterator it = names.begin();

    map<string, string> pos2neg;
    //USE IMAGES FROM OTHER CLASSES AS NEGATIVES
    pos2neg["knit1"] = "denim";// fur fleece corduroy";
    pos2neg["fleece"] = "fur knit1 denim corduroy";
    pos2neg["corduroy"] = "fur fleece denim knit1";
    pos2neg["denim"] = "knit1";//fur knit1 corduroy fleece";
    pos2neg["fur"] = "corduroy knit1 denim fleece";
    //READ POSITIVES ONLY
    while (it != names.end()) {
        if((*it) == sname) {
            ifstream in1((root + "/" +  (*it) + ".txt").c_str());
            if (!in1.is_open()) {
                cerr << "Cannot open class file " << (root + "/" + (*it) + ".txt") << endl;
            } else {
                int count = 0;
                while(in1 && count < nbTrain) {
                    // Check whether the scene is positive or negative
                    string line1;
                    getline(in1,line1);

                    if(line1 != "") {
                        const Scene scene(line1, 250, 250, 3, root);

                        if (scene.empty())
                            continue;

                        scenes.push_back(scene);

                        maxRows = max(maxRows, (scene.height() + 3) / 4 + padding);
                        maxCols = max(maxCols, (scene.width() + 3) / 4 + padding);

                    } else {
                        cerr << "empty line: " << count << "|" << (root + "/" + (*it) + ".txt") << endl;
                    }
                    ++count;
                }
            }
            in1.close();
        }
        ++it;

    }
    //READ NEGATIVES
    it = names.begin();
    while (it != names.end()) {
        if((*it) != sname && pos2neg[sname].find((*it)) != string::npos) {
            ifstream in1((root + "/" +  (*it) + ".txt").c_str());
            if (!in1.is_open()) {
                cerr << "Cannot open class file " << (root + "/" + (*it) + ".txt") << endl;
            } else {
                int count = 0;
                while(in1 && count < nbTrain) {
                    string line1;
                    getline(in1,line1);

                    if(line1 != "") {
                        //true now just means having an object (unknown name)
                        const Scene scene(line1, 250, 250, 3, root, "CLZ", true);

                        if (scene.empty())
                            continue;

                        if (nbNegativeScenes) {
                            scenes.push_back(scene);
                            maxRows = max(maxRows, (scene.height() + 3) / 4 + padding);
                            maxCols = max(maxCols, (scene.width() + 3) / 4 + padding);
                            --nbNegativeScenes;
                        }
                    } else {
                        cerr << "empty line: " << count << "|" << (root + "/" + (*it) + ".txt") << endl;
                    }
                    ++count;
                }
            }
            in1.close();
        }
        ++it;

    }
    //READ EXTRA NEGATIVES

    {
        ifstream nin(neg_file.c_str());

        if (!nin.is_open()) {
            showUsage();
            cerr << "\nInvalid negative set " << neg_file << endl;
            return -1;
        }

        while (nin) {

            string line;
            getline(nin, line);
            if(line != "" && nbNegativeScenes > 0) {
                //JPEGImage img(line);
                const Scene scene(line,250, 250, 3, root, "CLZ",false);

                scenes.push_back(scene);
                --nbNegativeScenes;
            }
        }
        nin.close();
    }


    //READ INITIAL ROOT SIZES
    cnpy::NpyArray arr = cnpy::npy_load(oroot + "/" + sname + "_root_inits.npy" );
    const int rows = arr.shape[0];
    const int cols = arr.shape[1];
    const int ws = arr.word_size;
    double * data = (double * ) arr.data;
    std::vector<std::pair<int, int> > rootSizes;
    for(int row = 0; row < rows; row++) {
        const int w =  (data[row * cols + 2] - data[row * cols + 0]) / (8.0 * 4.0) + 0.5;
        const int h =  (data[row * cols + 3] - data[row * cols + 1]) / (8.0 * 4.0) +  0.5;
        rootSizes.push_back(std::make_pair(h, w) );
    }
    arr.destruct();

    //LOAD ALL IFV
    cnpy::NpyArray arr1 = cnpy::npy_load(oroot + "/"  + sname + "_pose_clusters_labels.npy");
    int* data1 = (int * ) arr1.data;
    cnpy::NpyArray parts = cnpy::npy_load(oroot + "/" + sname + "_allparts.npy");
    double* data3 = (double*) parts.data;

    vector<FeaturePyramid::Cell> ifvs(nbComponents);
    vector<vector<Object::Part> > allparts;
    vector<int> labels;

//for(int row = 0; row < rows; ++row)
    for(int i = 0; i < nbTrain; ++i)
        labels.push_back(data1[i]);

    for(int row = 0; row < rows; ++row) {

        FeaturePyramid::Cell ifv = FeaturePyramid::Cell::Zero();
        int count = 0;
        for(int i = 0; i < nbTrain; ++i) {
            if(data1[1] == row) {

                cnpy::NpyArray ifv_ = cnpy::npy_load(oroot + "/" + sname + "/image_" + boost::lexical_cast<string>(i) + "_allparts_ifv.npy");
                float* data2 = (float*) ifv_.data;

                for(int x = 0; x < FeaturePyramid::NbFeatures; ++x) {
                    ifv(x) += data2[x];
                }
                ifv_.destruct();

            }
            count++;
        }
        ifvs[row] = ifv/ (float) count;

        //ifvs[row] = FeaturePyramid::Cell::Zero();

        for(int i = 0; i < nbTrain; ++i) {

            if(data1[i] == row) {
                /*
                cnpy::NpyArray ifv_ = cnpy::npy_load(oroot + "/" + sname + "/image_" + boost::lexical_cast<string>(i) + "_allparts_ifv.npy");
                float* data2 = (float*) ifv_.data;

                for(int x = 0; x < FeaturePyramid::NbFeatures; ++x) {
                	ifvs[row](x) = data2[x];
                }
                ifv_.destruct();
                */
                vector<Object::Part> p;
                p.resize(21);

                for(int x = 0; x < 21; ++x) {

                    p[x] << (float) data3[i * 21 * 4 + x * 4 + 0],
                      (float) data3[i * 21 * 4 + x * 4 + 1],
                      (float) data3[i * 21 * 4 + x * 4 + 2],
                      (float) data3[i * 21 * 4 + x * 4 + 3];
                }
                allparts.push_back(p);
                break;
            }
        }
    }

    if(allparts.size() != nbComponents)
    {
        cerr << "Not Enough Initial Parts" << endl;
        return -1;
    }

    parts.destruct();
    arr1.destruct();

    //Actually set the name 'code' here.
    name = Object::NameMap[sname];

    if (scenes.empty()) {
        showUsage();
        cerr << "\nInvalid image_set file " << file << endl;
        return -1;
    }

    // Initialize the Patchwork class
    if (!Patchwork::InitFFTW((maxRows + 15) & ~15, (maxCols + 15) & ~15)) {
        cerr << "Error initializing the FFTW library" << endl;
        return - 1;
    }

    // The mixture to train
    Mixture mixture(nbComponents, scenes, name, rootSizes);
    //mixture.setInitPositives(labels);
    mixture.iRoot = root;
    mixture.oRoot = oroot;
    result = sname + "_model.txt";

    if (mixture.empty()) {
        cerr << "Error initializing the mixture model" << endl;
        return -1;
    }

    // Try to open the mixture

    if (!model.empty()) {
        ifstream in(model.c_str(), ios::in);
        int startL = 0;
        int startD = 0;

        if (!in.is_open()) {
            showUsage();
            cerr << "\nInvalid model file " << model << endl;
            return -1;
        }

        in >> mixture;
        int limL = nbRelabel;
        int limD = nbDatamine;
        bool initParts = false;

        if(model.find("_tmp_")!=string::npos) {
            vector<string> strs;
            boost::split(strs,model,boost::is_any_of("_."));
            startL = boost::lexical_cast<int>(strs[2]);
            startD = boost::lexical_cast<int>(strs[3]);

            if( mixture.models()[0].parts().size() == 1 ) {
                limD = nbDatamine ;
                limL = nbRelabel / 2;
            }

            if(startD < limD - 1)
                startD++;
            else {
                if (startL < limL - 1) {
                    startL++;
                    startD = 0;
                } else {
                    startL = 0;
                    startD = 0;
                    limL = nbRelabel;
                    limD = nbDatamine;
                    initParts = true;
                }
            }
            cout << "starting from relabel=" << startL << " to " << limL << ", datamine = " << startD << " to " << limD << endl;
            cout << "N parts = 20" << endl;
        }

        if (mixture.empty()) {
            showUsage();
            cerr << "\nInvalid model file " << model << endl;
            return -1;
        }

        if(!initParts) {
            mixture.train(scenes, name, padding, padding, interval, limL, limD, 24000, C, J,
                          overlap, startL, startD);
            startD = 0;
            startL = 0;
            limL = nbRelabel;
            limD = nbDatamine;
        }

        if (mixture.models()[0].parts().size() == 1 && initParts) {
            cout << "Inializing Parts" << endl;
            //mixture.initializeParts(20, make_pair(8, 8),allparts, ifvs);
            mixture.initializeParts(20, make_pair(8, 8),allparts, ifvs);
        }
        mixture.train(scenes, name, padding, padding, interval, limL, limD, 24000, C, J,
                      overlap, startL, startD);

    } else {

        if (model.empty())
            mixture.train(scenes, name, padding, padding, interval, nbRelabel/2, nbDatamine, 2400, C,
                          J, overlap);

        if (mixture.models()[0].parts().size() == 1)
            //mixture.initializeParts(20, make_pair(8, 8),allparts, ifvs);
            mixture.initializeParts(20, make_pair(8, 8),allparts, ifvs);

        mixture.train(scenes, name, padding, padding, interval, nbRelabel, nbDatamine, 24000, C, J,
                      overlap);

    }
    // Try to open the result file
    ofstream out(result.c_str(), ios::binary);

    if (!out.is_open()) {
        showUsage();
        cerr << "\nInvalid result file " << result << endl;
        cout << mixture << endl; // Print the mixture as a last resort
        return -1;
    }

    out << mixture;

    return EXIT_SUCCESS;
}

