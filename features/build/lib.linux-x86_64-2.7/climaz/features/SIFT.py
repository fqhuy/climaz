# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 09:51:37 2013

@author: phan
"""
from Feature import for_all_paches, normalize, FeatureExtractor
    
def extract_feature_sift(image,patch_size=(16,16),cell_size=(4,4),spacing=16):
    """
    """
    def _extract_feature_sift(patch):
        detector = cv2.FeatureDetector_create("SIFT")
        descriptor = cv2.DescriptorExtractor_create("SIFT")
        
        skp = detector.detect(img)
        skp, sd = descriptor.compute(img, skp)
        return skp
    return for_all_patches(image,_extract_feature_sift)