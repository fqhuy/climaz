# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 09:09:23 2013

@author: phan
"""

from Feature import for_all_paches, normalize, FeatureExtractor

def extract_feature_filter_banks(image,patch_size=(8,8),spacing=8,scales=[1], \
n_rotations=8,sigmas=[1],frequencies=[0.05]):
    """
    """
    def _compute_feats(image, kernels):
        feats = np.zeros((len(kernels), 2), dtype=np.double)
        for k, kernel in enumerate(kernels):
            filtered = nd.convolve(image, kernel, mode='wrap')
            feats[k, 0] = filtered.mean()
            feats[k, 1] = filtered.var()
        return feats
        
    def _extract_feature_filter_banks(patch):
        kernels = []
        for theta in range(n_rotations):
            theta = theta / n_rotations * np.pi
            for sigma in sigmas:
                for frequency in frequencies:
                    for scale in scales:
                        kernel = scale * np.real(gabor_kernel(frequency, theta=theta, \
                        sigma_x=sigma, sigma_y=sigma))
                        kernels.append(kernel)
        feats = _compute_feats(patch,kernels)
        #64d vector !
        return feats.flatten()
    
    return for_all_patches(image, _extract_feature_filter_banks)
    
class FilterBanks(FeatureExtractor):
    def __init__(self,patch_size,spacing,scales=[1], \
    n_rotations=8,sigmas=[1],frequencies=[0.05]):
        super(SRP, self).__init__(patch_size,spacing,None)
        self.scales = scales
        self.n_rotations = n_rotations
        self.sigmas = sigmas
        self.frequencies = frequencies
        
    def extract(image):
        return extract_feature_filter_banks(image, self.patch_size, \
        self.spacing, self.scales, self.n_rotations, self.sigmas, \
        self.frequencies)
        
    def size():
        return len(self.scales) * len(sigmas) * len(n_rotations) * 2