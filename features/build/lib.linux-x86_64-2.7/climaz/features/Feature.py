# -*- coding: utf-8 -*-
"""
Created on Sun Sep 22 13:51:00 2013

@author: phan
"""

import numpy as np
import cv2
from PIL import Image
from skimage import io, filter
import skimage as ski
#import matplotlib.pyplot as plt
from skimage import measure
from skimage import filter
from scipy import stats
from Utils import *
#from lss.lss import *
from scipy.fftpack import dct
import numpy as np
import math

class ColorDict(object):
    def __init__(self):
        self.color_channels = {'L' : 1, 'YCbCr': 3, 'RGB': 3, 'F': 1}
        
    def n_channels(self,color_mode):
        return self.color_channels[color_mode]
        
    def __getitem__(self,key):
        return self.color_channels[key]
        
color_dict = ColorDict()

class FeatureExtractor(object):
    def __init__(self,patch_size,spacing,normalizer,padding):
        '''
        noramlizer = 'weber', 'unit', 'none'
        '''
        self.patch_size = patch_size
        self.spacing = spacing
        self.normalizer = normalizer
        self.padding = padding
        
    def extract(self,image):
        return np.array([])
        
    def extract_all(self,images):
        FS = []
        for i in images:
            FS.append( self.extract(i) )
        return FS
            
    def size(self):
        raise NotImplementedError()
        
    def size(self,n_channels):
        return self.size() * n_channels

def for_all_patches(image,func,patch_size=(8,8),spacing=8,padding='none', \
skip_blank=False):
    
    if image.ndim == 3:
        (h, w, d) = image.shape
    elif image.ndim == 2:
        image = image[:,:,np.newaxis]
        h,w,d = image.shape
    if padding!='none':
        p1 = patch_size[0] / 2
        p2 = patch_size[1] / 2
        w += p1 * 2
        h += p2 * 2
        image = np.pad(image,((p1,p1),(p2,p2),(0,0)),padding)
        
    B = []
    A = []
    #edges = 
    #edges = filter.edges.binary_erosion(filter.sobel(image[:,:,0].clip(-1,1)))
    if skip_blank:
        edges = filter.sobel(image[:,:,0].clip(-1,1))
    # print "std: " + str(image.std()) + " mean: " + str(image.mean())
    #TODO: row or col first ?
    for row in range(0, h - patch_size[0], spacing):
        for col in range(0, w - patch_size[1], spacing):
            patch = image[row:row + patch_size[0], col:col + patch_size[1]]
            if skip_blank:
                epatch = edges[row:row + patch_size[0], col:col + patch_size[1]]
            #for every channel
            #avoid 'blank' patches
            #TODO: we are considering one channel only!
            #if (epatch == False).sum() < sum(patch_size)/5:
            skip = False
            if skip_blank:    
                if (epatch == 0).sum() < sum(patch_size) * 0.4:
                    skip = True
                
            if not skip:
                c0 = func(patch[:,:,0]).flatten()
                fblock = np.zeros((len(c0),d))
                fblock[:,0] = c0
                for c in range(1,d):
                    fblock[:, c] = func(patch[:,:,c]).flatten()
                #interleave the channel vectors to form a feature vector
                fblock = fblock.flatten()
                B.append(fblock)
                A.append((row,col))
            '''
            else:
                plt.ioff()
                fig = plt.figure(col * w + row)
                plt.imshow(patch[:,:,0])
                plt.savefig("/home/phan/workspace/climaz/data/SCM_CLIM/output/tmp/image_%d.jpg" \
                % (col * w + row))
                plt.close(fig)
            '''
    return A, B

def normalize(image,norm_method="weber"):
    """
    """
    #w,h,d = image.shape
    #TODO: need to take care of image dimensionality here, currently it's just a vector
    #image_  = image.flatten()
    if norm_method == "weber":
        #norm = np.linalg.norm(image,np.inf)
        norm = np.linalg.norm(image,2)
        normalizer = (norm) / (math.log(1. + norm/0.03))
    elif norm_method == "unit":
        #Unit norm
        normalizer = np.linalg.norm(image,2)
    else:
        normalizer = 1
    return image / normalizer

if __name__=="__main__":
    pass
    #im = np.array(Image.open("sponge.png").convert("RGB")) / 1.
    #im = im[:,:,np.newaxis]
    #f = extract_feature_lss(im,patch_size=(10,10),cor_size=100)
