# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 09:48:18 2013

@author: phan
"""

from Feature import for_all_patches, normalize, FeatureExtractor
import numpy as np

idx = [2,3,4,8,9,10,11,12,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30, \
31,32,33,34,36,37,38,39,40,44,45,46]

def extract_feature_raw(image, patch_size = (10,10), \
    spacing = 5, normalizer = 'weber', padding='none', \
    skip_blank=False, circular=False):

    def _extract_feature_raw(patch):
        if circular == True:
            p = patch.flatten()[idx]
        else:
            p = patch#.flatten()
        return normalize(p,normalizer)
    A,B = for_all_patches(image, _extract_feature_raw, patch_size, spacing, padding,skip_blank)
    return np.asfarray(B)

class Raw(FeatureExtractor):
    def __init__(self,patch_size,spacing,normalizer='weber',padding='none', \
    skip_blank=False,circular=False):
        
        super(Raw, self).__init__(patch_size,spacing,normalizer,padding)
        self.circular = circular
        self.skip_blank = skip_blank
        
    def extract(self,image):
        #extract_feature
        return extract_feature_raw(image, self.patch_size, \
        self.spacing,self.normalizer,self.padding,self.skip_blank,self.circular)
        
    def size(self):
        if not self.circular:
            return self.patch_size[0] * self.patch_size[1]
        else:
            return 37
