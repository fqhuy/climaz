# -*- coding: utf-8 -*-
# Improved Fisher Vectors.

from Feature import for_all_patches, normalize, FeatureExtractor
import numpy as np
import vlfeat as vl
from sklearn import mixture as mx

class IFV(FeatureExtractor):
    def __init__(self,patch_size,spacing,normalizer='weber',padding='none'):
        super(IFV, self).__init__(patch_size,spacing,normalizer,padding)
        
    def extract(self,image):
        img = image #img_as_float(data.lena()[:256, :256].mean(axis=2))
        sift = vl.DSIFT(img.shape[0], img.shape[1], self.spacing, self.patch_size)
        c_array  = np.ascontiguousarray(img[:,:,np.newaxis],dtype='f')
        data = sift.process(c_array)
        fisher = vl.FisherEncoder()
        gmm = mx.GMM(n_components=40)
        gmm.fit(data)
        ifv = fisher.encode(data.shape[0], gmm.n_components, data, \
        vl.FisherEncoder.VL_FISHER_FLAG_IMPROVED, \
        np.asarray(gmm.covars_[:,np.newaxis],dtype='d',order='C'), 
        np.asarray(gmm.means_,dtype='d',order='C'), 
        np.asarray(gmm.weights_,dtype='d',order='C'))
        
        return ifv
        
    def size(self):
        if not self.circular:
            return self.patch_size[0] * self.patch_size[1]
        else:
            return 37

if __name__ == "__main__":
    from skimage import data    
    from skimage.transform import pyramid_gaussian
    from skimage import img_as_float    
    
    image = img_as_float(data.lena()[:256, :256].mean(axis=2))    
    ifv = IFV(14,7)    
    pyramid = pyramid_gaussian(image, downscale=2)
    ifvs = []
    for layer in pyramid:
        ifvs.append(ifv.extract(layer))
    np.asarray(ifvs).save("ifvs.npy")