# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 09:38:51 2013

@author: phan
"""

from Feature import for_all_patches, normalize, FeatureExtractor
from lss import *


def extract_feature_lss(image, patch_size=(5,5), cor_size = 40, \
    nrad = 3, nang = 12, var_noise = 30000, saliency_thresh = 0.7, \
    homogeneity_thresh = 0.7, snn_thresh = 0.7,calc_rect=(0,0,0,0)):
        
    parms = {'patch_size' : patch_size[0],'cor_size' : cor_size, \
    'nrad' : nrad, 'nang' : nang, 'var_noise' : var_noise, \
    'saliency_thresh' : saliency_thresh, \
    'homogeneity_thresh' :  homogeneity_thresh, \
    'snn_thresh' : snn_thresh}
    #calc_rect = calc_rect
    features = calc_ssdescs_alt(image=image, parms=parms, calc_rect=calc_rect)
    np.savetxt("lss_features.txt",features)
    return features
    
class LSS(FeatureExtractor):
    def __init__(self,patch_size=(5,5), cor_size = 40, \
    nrad = 3, nang = 12, var_noise = 30000, saliency_thresh = 0.7, \
    homogeneity_thresh = 0.7, snn_thresh = 0.7,calc_rect=(0,0,0,0)):
        super(LSS, self).__init__(patch_size,0,'none')
        self.cor_size = cor_size
        self.nrad = nrad
        self.nang = nang
        self.var_noise = var_noise
        self.saliency_thresh = saliency_thresh
        self.homogeneity_thresh = homogeneity_thresh
        self.snn_thresh = snn_thresh
        self.calc_rect = calc_rect
        
    def extract(self,image):
        #extract_feature
        return extract_feature_lss(image, self.patch_size, self.cor_size, \
        self.nrad, self.nang, self.var_noise, self.saliency_thresh, \
        self.homogeneity_thresh, self.snn_thresh,self.calc_rect)
        
    def size(self):
        return 0