from sklearn.decomposition import PCA
from matplotlib import mpl, pyplot
from PIL import Image
import numpy as np

import DSIFT

im = np.array(Image.open('knit1/image_0.pgm').convert('F'))
p1 = np.array(Image.open('knit1/image_0_1.png').convert('F'))

sift = DSIFT.extract_feature_dsift(im, padding=0)
psift = DSIFT.extract_feature_dsift(p1, padding=0)

def visualize1(m):
    fig = pyplot.figure(1)
    cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap', ['black','white'], 256)
    img = pyplot.imshow(m, interpolation='nearest', cmap = cmap, origin='lower')
    pyplot.colorbar(img, cmap = cmap)
    fig.show()
    
scores = np.zeros((109, 109),dtype='f')
fsift = psift[0,0]
for i in range(0, 124 - 15):
    for j in range(0, 124 - 15):
        scores[i,j] = fsift.dot(sift[i, j])
        
visualize(scores)

from skimage.feature import hog
from skimage.io import imread
from skimage.transform import pyramid_reduce

def sk_hog(im,scale=2):
	im = imread(im,as_grey=True)
	im = pyramid_reduce(im, scale)
	hogmap = np.zeros(
	for y in range(0, im.shape[0] - 64, 8):
		for x in range(0, im.shape[1] - 64, 8):
			feat = hog(im[y:y+64, x:x+64],  orientations = 8, pixels_per_cell=(8,8), cells_per_block=(4,4), visualise=False)

