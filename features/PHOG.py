# -*- coding: utf-8 -*-
"""
Created on Fri Dec 27 10:44:53 2013

@author: phan
"""
from Feature import for_all_patches, normalize, FeatureExtractor
import numpy as np
import vlfeat as vl
from skimage.feature import hog as skhog

def visualize_hog(hog):
    from skimage import draw
    n_cellsx = hog.shape[1]
    n_cellsy = hog.shape[0]
    cx, cy = 8
    sx, sy = 64
    
    radius = min(cx, cy) // 2 - 1
    hog_image = np.zeros((sy, sx), dtype=float)
    for x in range(n_cellsx):
        for y in range(n_cellsy):
            for o in range(orientations):
                centre = tuple([y * cy + cy // 2, x * cx + cx // 2])
                dx = radius * cos(float(o) / orientations * np.pi)
                dy = radius * sin(float(o) / orientations * np.pi)
                rr, cc = draw.line(int(centre[0] - dx),
                                   int(centre[1] - dy),
                                   int(centre[0] + dx),
                                   int(centre[1] + dy))
                hog_image[rr, cc] += orientation_histogram[y, x, o]
                
def extract_feature_hog(image,cell_size=(8,8),spacing=8):
    hog = vl.HOG(vl.HOG.VlHogVariantDalalTriggs, cell_size[0], vl.VL_FALSE)
    data = hog.process(np.ascontiguousarray(image,dtype='f'), 8)
    return data
    
class PHOG(FeatureExtractor):
    def __init__(self,patch_size,spacing,normalizer='weber'):
        self.patch_size = patch_size
        
    def extract(self,image):
        pass
        
    def size(self):
        pass

def worker(root, saveroot, scales, classes, extract_only, levels):
    pd = (1, 3) #padding (before, after)
    cell_size = 8
    feat_size = cell_size * cell_size * 32
    for c in classes:
        print('processing ' + c)
        if not extract_only:
            patches = np.zeros((60 * 20, feat_size),dtype='f') #2048
        for i in range(60):
            im = imread(root + c + '/image_%d.pgm' % i ,as_grey=True)/255.
            #im = im[20:im.shape[0] - 20, 20:im.shape[1] - 20]
            ##TODO: This is only true for 1000x1000 images *****
            #im = (im - im.mean()) / im.std()
            im = pyramid_reduce(im, 4)
            for level in range(1, 6):
                scaled = pyramid_reduce(im, scales[level])
                sizes = levels[level]
                
                #hog = vl.HOG(vl.HOG.VlHogVariantDalalTriggs, 8, vl.VL_FALSE)
                #data = skhog.process(np.ascontiguousarray(im, dtype='f'), 8)
                data = skhog(scaled, orientations = 8, pixels_per_cell=(8,8), \
                cells_per_block=(2,2), visualise=False, normalise = True)
                sz = np.sqrt((data.shape[0] / 32))
                pd = (1, sizes[0] - sz - 1)
                data = data.reshape(sz, sz, 32)
                data = np.pad(data, (pd, pd, (0,0)),'constant')
                
                #np.save(saveroot + c + '/image_%d_hog_%d.npy' % (i,feat_size), data)
                np.save(saveroot + c + '/image_%d_l%d_hog_%d.npy' % (i,level,feat_size), data)
                
                if not extract_only:
                    txt = open(root + c + '/image_%d.txt' % i,'r')
                    txt.readline() #Throw away the 0th part
                    for p in range(1,21):
                        l = txt.readline()
                        rect = np.asarray([int(n) for n in l.split(' ')])
                        #pim = imread('/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/%s/image_%d_%d.pgm' % (c,i,p),as_grey=True)/255.
                        #pim = pyramid_reduce(pim, scale)
                        #pim = (pim - pim.mean()) / pim.std()
                        #patch = hog.process(np.ascontiguousarray(pim, dtype ='f'), 8)
                        x = (rect[0]) / (128 / cell_size) + pd[0] #16
                        y = (rect[1]) / (128 / cell_size) + pd[0]#16
                        if x + 8 >= sz: 
                            x = sz - 8
                        if y + 8 >= sz:
                            y = sz - 8
                            
                        patch = data[y : y + cell_size, x : x + cell_size] # 4 or 8
                        patches[i * (p - 1), :] = patch.flatten()
                    txt.close()
          
            if not extract_only:
                km = KMeans(n_clusters = 10)
                print('running kmeans..')
                km.fit(patches)
                np.savez(saveroot + c + '_hog_clusters_%d_10.npz' % 
                feat_size, km.labels_, km.cluster_centers_)
            '''
            try:
                his = [np.histogram(km.labels_[i * 20 : i * 20 + 20 ], bins=range(21))[0] for i in range(60)]
                km1 = KMeans(n_clusters = 6)
                km1.fit(np.vstack(his))
                np.savez(saveroot + c + '_img_hog_clusters.npz', km1.labels_, km1.cluster_centers_)
            except Exception as e:
                print(e)
            '''
        

if __name__ == '__main__':
    from skimage.io import imread, imsave
    from skimage.transform import pyramid_reduce
    from sklearn.cluster import KMeans
    import multiprocessing
    import pyramid as pyr

    
    #root
    levels, scales = pyr.build_indices((500,500))    
    root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/'
    saveroot = root[:-1] + '_OUTPUT/'    
    #classes = ['fleece', 'fur', 'corduroy', 'denim','knit1', 'leather', 'boucle', 'lace']  #
    #classes = [['fleece', 'fur'], ['corduroy', 'denim'],['knit1', 'leather'], ['boucle', 'lace']]  #
    classes= [['LANDSCAPE']]
    scale = pow(2.0, 1)
    for cc in classes:
        p = multiprocessing.Process(target=worker,args=(root, saveroot, scales, cc, True, levels))
        #worker(root, saveroot, scale, cc)
        p.start()    

if __name__ == '__main_':
    from skimage.io import imread
    from skimage.transform import pyramid_reduce
    from matplotlib import pyplot as plt
    from matplotlib import mpl
    
    im = imread('/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/corduroy/image_1.pgm',as_grey=True)/255.
    #im = imread('/home/phan/Pictures/image_1.png',as_grey=True)/255.
    scale = pow(2.0, 1)
    im = pyramid_reduce(im, scale)
    hog = vl.HOG(vl.HOG.VlHogVariantDalalTriggs,8,vl.VL_FALSE)
    data = hog.process(np.ascontiguousarray(im,dtype='f'), 8)
    #om = hog.visualize(data)
    patches = []
    #patches.append(data[45:53, 44:52])
    
    for p in range(1,21):
        pim = imread('/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/corduroy/image_1_%d.pgm' % p,as_grey=True)/255.
        pim = pyramid_reduce(pim, scale)
        patch = hog.process(np.ascontiguousarray(pim, dtype ='f'), 8)
        patches.append(patch)
       
    pw = patches[0].shape[1]
    ph = patches[0].shape[0]
    
    scores = np.zeros((data.shape[0] - ph, data.shape[1] - pw), dtype='f')
    maxscore = 0
    pos = (0,0)
    for p in range(1):
        for y in range(data.shape[0] - ph):
            for x in range(data.shape[1] - pw):
                scores[y,x] += np.dot(data[y:y+ph, x:x+pw].flatten(), patches[p].flatten())
                if(scores[y,x] > maxscore):
                    maxscore = scores[y,x]
                    pos = (y, x)
    
    print("max " + str(pos) + ' ' + str(maxscore) )

    plt.imshow(scores)
    #plt.imshow(hog.visualize(data))
    #plt.imshow(hog.visualize(patch))
    plt.show()
   

if __name__ == "__main_":
    from skimage import data    
    from skimage.transform import pyramid_gaussian, pyramid_expand
    from skimage import img_as_ubyte    
    import os
    root = sys.argv[1]
    if len(sys.argv) == 2:
        n_scales = int(sys.argv[2])
    else:
        n_scales = 21
        
    for d in os.listdir(root):
        if os.path.isdir(d):
            for i in range(60):
                fname = d + '/image_' + str(i)
                image = img_as_ubyte(fname + '.jpg')    
                hogs = []
                #hog = vl.HOG(vl.HOG.VlHogVariantUoctti,8,vl.VL_FALSE)
                level0 = pyramid_expand(image, upscale=2)
                hogs.append(extract_feature_hog(level0))
                
                pyramid = pyramid_gaussian(image, downscale=2, max_layer=n_scales)
                for layer in pyramid:
                    hogs.append(extract_feature_hog(layer))
                    #h = hog.process(np.ascontiguousarray(layer[:,:,np.newaxis],dtype='f'), \
                    #layer.shape[0], layer.shape[1], 1, 8)
                    #ifvs.append(h)
                    
                np.save(fname + '_hog.npy', np.asarray(hogs))    