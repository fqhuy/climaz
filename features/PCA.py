# -*- coding: utf-8 -*-

from Feature import for_all_patches, normalize, FeatureExtractor
import numpy as np
from sklearn.decomposition import PCA as Pca
import math

idx = [2,3,4,8,9,10,11,12,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30, \
31,32,33,34,36,37,38,39,40,44,45,46]

def extract_feature_pca(image, pca, patch_size = (10,10), \
    spacing = 5, normalizer = 'weber', padding='none', \
    skip_blank=False, circular=True):

    def _extract_feature_pca(patch):
        if circular == True:
            p = patch.flatten()[idx]
        else:
            p = patch.flatten()
        pca.fit(p)
        
        return normalize(p,normalizer)
        
    return np.asfarray(for_all_patches(image, _extract_feature_pca, \
    patch_size, spacing, padding,skip_blank))

class PCA(FeatureExtractor):
    def __init__(self,patch_size,spacing,n_components=0,normalizer='weber',padding='none', \
    skip_blank=False,circular=True):
        
        super(PCA, self).__init__(patch_size,spacing,normalizer,padding)
        if n_components <= 0:
            self.n_components = math.trunc(sum(patch_size) / 2)
        else:
            self.n_components = n_components
        self.pca = Pca(n_components = n_components)
        self.circular = circular
        self.skip_blank = skip_blank
        
    def extract(self,image):
        #extract_feature
        return extract_feature_pca(image, self.pca, self.patch_size, \
        self.spacing, self.normalizer,self.padding,self.skip_blank,self.circular)
        
    def size(self):
        if not self.circular:
            return self.patch_size[0] * self.patch_size[1]
        else:
            return 37
