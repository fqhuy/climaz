# -*- coding: utf-8 -*-
"""
Created on Fri Mar 14 19:04:03 2014

@author: phan
"""
cimport rfs
cimport numpy as np
import numpy as np

cpdef extract_mr8(np.ndarray[float,ndim=2,mode='c'] image, normalize=True):
    cdef np.ndarray[float,ndim=2,mode='c'] inp = (image - image.mean()) / image.std()
    #np.zeros((image.shape[0] * image.shape[1]),dtype='f',order='C')

    cdef int m = image.shape[0]
    cdef int n = image.shape[1]
    cdef np.ndarray[float, ndim=3, mode = 'c'] ims = np.zeros((8, m , n),dtype='f',order='C')  
    cdef np.ndarray[float, ndim=3, mode = 'c'] ims_ = np.zeros((8, m - 50 , n - 50),dtype='f',order='C')
    cdef int i = 0
    cdef double sfac = 1.0
    cdef double mulfac = 2.0
    
    cdef double s1 = 3. * sfac
    cdef double s2 = 1. * sfac
    cdef double phi = 0.0
    
    cdef np.ndarray[float,ndim=2,mode='c'] maxim1
    cdef np.ndarray[float,ndim=2,mode='c'] maxim2
    cdef np.ndarray[float,ndim=2,mode='c'] im1
    cdef np.ndarray[float,ndim=2,mode='c'] im2
    for j in range(0, 3):
        for k in range(0, 6):
            phi = (k / 6.0) * 180.0
            im1 = _anigauss(inp, s1, s2, phi, 0, 1)
            im2 = _anigauss(inp, s1, s2, phi, 0, 2)
            im1 = np.abs(im1)
            
            if k == 0:
                maxim1 = im1
                maxim2 = im2
            else:
                maxim1 = np.maximum(maxim1, im1)
                maxim2 = np.maximum(maxim2, im2)
        ims[i] = maxim1
        i+=1
        ims[i] = maxim2
        i+=1
        s1 *= mulfac
        s2 *= mulfac
    cdef double sigma = 10.0 * sfac
    im1 = _anigauss(inp, sigma, sigma, 0.0, 2, 0)
    im2 = _anigauss(inp, sigma, sigma, 0.0, 0, 2)
    ims[i] = (im1 + im2);
    i += 1
    ims[i] = _anigauss(inp, sigma, sigma, 0.0, 0, 0)
    
    cdef tuple t = ims[0].shape
    for j in range(0,i):
        #just throw away 25 pixel border...(half support of sigma=10 filter)
        ims_[j] = ims[j][25: t[0] - 25, 25: t[1] - 25]
    return np.vstack([ims_[7].flatten(),ims_[6].flatten(),ims_[0].flatten(),ims_[2].flatten(),ims_[4].flatten(),
    ims_[1].flatten(),ims_[3].flatten(),ims_[5].flatten()]).transpose()
    
cdef _anigauss(np.ndarray[float, ndim=2, mode='c'] inp, double sigmav = 0.0, \
double sigmau = 0.0, double phi = 0.0, int orderv = 0, int orderu = 0):
    if orderu < 0 or orderv < 0:
        return 0
    cdef int m = inp.shape[0]
    cdef int n = inp.shape[1]
    cdef np.ndarray[float, ndim=2, mode='c'] out = np.zeros((m,n),dtype='float32',order='C')
    rfs.anigauss(&inp[0,0], &out[0,0], m, n, sigmav, sigmau, phi - 90.0, orderv, orderu)
    return out