# -*- coding: utf-8 -*-

import vlfeat as vl
from skimage.io import imread
import numpy as np
import cv2

def extract_feature_sift(image):
    #sift = vl.SIFT(1000 ,  1000, 1, 5, 0)
    sift = cv2.SIFT(20, 5)
    kp = sift.detect(image)
    
    #keypoints = sift.detect(np.ascontiguousarray(image, dtype='f'))

if __name__ == '__main__':
    url = "/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/corduroy/image_53.pgm"
    image = imread(url, as_grey=True) / 1.
    extract_feature_sift(image)