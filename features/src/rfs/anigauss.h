/* define the input buffer type, e.g. "float" */
#define SRCTYPE float

/* define the output buffer type, should be at least "float" */
#define DSTTYPE float

/* the function prototypes */
void anigauss(SRCTYPE *input, DSTTYPE *output, int sizex, int sizey,
	double sigmav, double sigmau, double phi, int orderv, int orderu);