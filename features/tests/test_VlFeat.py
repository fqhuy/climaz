# -*- coding: utf-8 -*-

import warnings
import numpy as np
import unittest
from skimage import data
from skimage import img_as_float
from sklearn import mixture
from sklearn.datasets.samples_generator import make_spd_matrix
from numpy.testing import (assert_array_equal, assert_array_almost_equal,
                           assert_almost_equal)
from nose.tools import assert_raises, assert_true, assert_equal, assert_false

from climaz.features import vlfeat as vl

rng = np.random.RandomState(0)


def test_dsift():
    img = img_as_float(data.lena()[:256, :256].mean(axis=2))
    sift = vl.DSIFT(img.shape[0], img.shape[1], 14, 7)
    c_array  = np.ascontiguousarray(img[:,:,np.newaxis],dtype='f')
    sift.process(c_array)

class GMMTester():
    def _setUp(self):
        self.n_components = 10
        self.n_features = 4
        self.weights = rng.rand(self.n_components)
        self.weights = self.weights / self.weights.sum()
        self.means = rng.randint(-20, 20, (self.n_components, self.n_features))
        self.threshold = -0.5
        self.I = np.eye(self.n_features)
        self.covars = {
            'spherical': (0.1 + 2 * rng.rand(self.n_components,
                                             self.n_features)) ** 2,
            'tied': (make_spd_matrix(self.n_features, random_state=0)
                     + 5 * self.I),
            'diag': (0.1 + 2 * rng.rand(self.n_components,
                                        self.n_features)) ** 2,
            'full': np.array([make_spd_matrix(self.n_features, random_state=0)
                              + 5 * self.I for x in range(self.n_components)])}

    def test_gmm(self):
        gmm = vl.GMM(2,self.n_features,self.n_components)
        
        g = self.model(n_components=self.n_components, covariance_type=self.covariance_type)
        g.weights_ = self.weights
        g.means_ = self.means
        g.covars_ = 20 * self.covars[self.covariance_type]
        X = g.sample(n_samples=100)
        X = np.ascontiguousarray(X,dtype='d')
        gmm.cluster(X)
        
        means = np.reshape(gmm.means,(self.n_components,self.n_features))
        assert_equal(means.shape, self.means.shape)
        #assert_array_almost_equal(means, self.means)
        
class TestGMMWithDiagonalCovars(unittest.TestCase, GMMTester):
    covariance_type = 'diag'
    model = mixture.GMM
    setUp = GMMTester._setUp

def test_fisher_encoder():
    fisher = vl.FisherEncoder()
    gmm = GMMTester()
    gmm._setUp()
    g = mixture.GMM(n_components=gmm.n_components, covariance_type='full')
    g.weights_ = gmm.weights
    g.means_ = gmm.means
    g.covars_ = 20 * gmm.covars['full']
    X = np.asarray(g.sample(n_samples=100),dtype='d',order='C')
    
    ifv = fisher.encode(gmm.n_features, gmm.n_components, X, \
    vl.FisherEncoder.VL_FISHER_FLAG_IMPROVED, \
    np.asarray(gmm.covars['full'],dtype='d',order='C'), 
    np.asarray(gmm.means,dtype='d',order='C'), 
    np.asarray(gmm.weights,dtype='d',order='C'))


if __name__ == '__main__':
    import nose
    nose.runmodule()