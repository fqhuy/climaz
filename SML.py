#for each semantic class w
import numpy as np
import scipy as sp
#import matplotlib.pyplot as plt
from sklearn import mixture as mx
#from skimage import io
import sklearn as skl
from PIL import Image
from features import *
from Dataset import *
from sklearn import neighbors
from Model import ClassifierModel
from viz import TextonViz as tvz
import os.path 
import sys
import logging as lgg

class HGMM(mx.GMM):
    def __init__(self, n_components=1,covariance_type='diag',random_state=None,thresh=1e-2, min_covar=1e-3, \
    n_iter=1000, n_init=1, params='wmc', init_params='wmc',N=1000):
        ##TODO: Need to assign init_params here
        super(HGMM, self).__init__(n_components=n_components,covariance_type=covariance_type, \
                 random_state=random_state, thresh=thresh, min_covar=min_covar, \
                 n_iter=n_iter, n_init=n_init, params=params, init_params=init_params)
        self.N = N
        self.means_ = []
        self.covars_ = []
        self.weights_ = []

    def _do_mstep(self, X, responsibilities, params, min_covar=0):
        """ Perform the Mstep of the EM algorithm and return the class weihgts.
        """
        weights = responsibilities.sum(axis=0)
        #DK x M
        (xw,xh) = X.shape
        ww = (responsibilities * self.sweights_[:,np.newaxis]) \
        / (np.dot(responsibilities.T,self.sweights_[:,np.newaxis]).flatten() + mx.gmm.EPS) #maybe with np.newaxis ?
        if 'w' in params:
            #self.weights_ = (weights / (weights.sum() + 10 * mx.gmm.EPS))
            self.weights_ = (weights / xw) #xw
        if 'm' in params:
            self.means_ = np.dot(ww.T, self.smeans_) #weighted_X_sum * inverse_weights
        if 'c' in params:
             for m in range(self.n_components):
                xmu = self.smeans_ - self.means_[m]
                wj = ww[:,m]
                cv = np.sum(wj[:,np.newaxis] * (self.scovars_ + xmu ** 2),axis=0)
                cv = np.maximum(cv,np.repeat(1e-6,xh))
                self.covars_[m] = cv
        return weights

    def e_step(self, X):
        X = np.asarray(X)
        if X.ndim == 1:
            X = X[:, np.newaxis]
        if X.size == 0:
            raise ValueError("X is empty")
        if X.shape[1] != self.means_.shape[1]:
            raise ValueError('The shape of X  is not compatible with self')
            
        #lpr.shape  = (n_samples x n_components)
        #lgauss.shape = (n_samples * n_scomponents x n_components)
        #lgauss = muLLcomp
        lgauss = mx.gmm.log_multivariate_normal_density( \
        X, self.means_, self.covars_, self.covariance_type)
        
        if self.covariance_type=='diag':
            #lgauss = muLLcomp
            #lgauss.shape = (n_samples * n_scomponents x n_components)                
            tr = np.dot(1. / self.covars_ , self.scovars_.T).T
            
            lpr = (( lgauss - 0.5 * tr) * self.N * self.sweights_[:,np.newaxis]) \
            + np.log(self.weights_) #np.tile(np.log(self.weights_),(X.shape[0],1))
                        
        elif self.covariance_type=='spherical':
            pass
        elif self.covariance_type=='full':
            pass
            
        #DK length, logtrick
        #logprob = skl.utils.extmath.logsumexp(lpr, axis=1)
        logprob = self.logtrick2(lpr,axis=1)
        
        if X.shape[0] != 1:
            #pass
            lgg.info("sum lob probability: %s" % str(logprob.sum()))
        #responsibilities : array_like, shape (n_samples, n_components)
        #lpr - LLcomp, logprob - LL
        #respon.. - Y
        responsibilities = np.exp(lpr - logprob[:, np.newaxis])
        return logprob, responsibilities
        
    def logtrick2(self,X,axis=1):
        v, i = X.max(axis), X.argmax(axis)
        cterm = np.sum(np.exp(X - v[:,np.newaxis]),axis=axis)
        return v + np.log(cterm)
        
    def set_data(self, data):
        self.sweights_ = data[0]
        self.smeans_ = data[1]
        self.scovars_ = data[2]
        
    def fit(self, XX):
        self.set_data(XX)
        #X <- smeans_ (centers of all components of the lower layer)
        X = self.smeans_
        if X.ndim == 1:
            X = X[:, np.newaxis]
        if X.shape[0] < self.n_components:
            raise ValueError(
                'GMM estimation with %s components, but got only %s samples' %
                (self.n_components, X.shape[0]))

        max_log_prob = -np.infty

        for _ in range(self.n_init):
            if 'm' in self.init_params or not hasattr(self, 'means_'):
                self.means_ = skl.cluster.KMeans(
                    n_clusters=self.n_components,
                    random_state=self.random_state).fit(X).cluster_centers_

            if 'w' in self.init_params or not hasattr(self, 'weights_'):
                self.weights_ = np.tile(1.0 / self.n_components,
                                        self.n_components)

            if 'c' in self.init_params or not hasattr(self, 'covars_'):
                cv = np.cov(X.T) + self.min_covar * np.eye(X.shape[1])
                if not cv.shape:
                    cv.shape = (1, 1)
                self.covars_ = \
                    mx.gmm.distribute_covar_matrix_to_match_covariance_type(
                        cv, self.covariance_type, self.n_components)

            # EM algorithms
            log_likelihood = []
            # reset self.converged_ to False
            self.converged_ = False
            for i in range(self.n_iter):
                # Expectation step
                curr_log_likelihood, responsibilities = self.e_step(X)
                log_likelihood.append(curr_log_likelihood.sum())

                # Check for convergence.
                if i > 0 and abs(log_likelihood[-1] - log_likelihood[-2]) < \
                        self.thresh:
                    self.converged_ = True
                    break

                # Maximization step
                self._do_mstep(X, responsibilities, self.params,
                               self.min_covar)

            # if the results are better, keep it
            if self.n_iter:
                if log_likelihood[-1] > max_log_prob:
                    max_log_prob = log_likelihood[-1]
                    best_params = {'weights': self.weights_,
                                   'means': self.means_,
                                   'covars': self.covars_}
        # check the existence of an init param that was not subject to
        # likelihood computation issue.
        if np.isneginf(max_log_prob) and self.n_iter:
            raise RuntimeError(
                "EM algorithm was never able to compute a valid likelihood " +
                "given initial parameters. Try different init parameters " +
                "(or increasing n_init) or check for degenerate data.")
                
        # self.n_iter == 0 occurs when using GMM within HMM
        if self.n_iter:
            lgg.info("max log prob: %s" % max_log_prob)
            self.covars_ = best_params['covars']
            self.means_ = best_params['means']
            self.weights_ = best_params['weights']
        return self
        
    def dosplit(self):
        '''
        component splitting
        '''
     

class SML(ClassifierModel):
    def __init__(self,classes,n_components=64, \
    covariance_type='diag', n_scomponents=6, feature_extractors=[], \
    color_mode='L', N=1000, classifier = 'bayes'):
        """
        Supervised Multiclass Learning

        This class use the learning method described in the paper:
        Carneiro, G.; Chan, A.B.; Moreno, P.J.; Vasconcelos, N., "Supervised Learning of Semantic Classes for Image Annotation and Retrieval," Pattern Analysis and Machine Intelligence, IEEE Transactions on , vol.29, no.3, pp.394,410, March 2007
doi: 10.1109/TPAMI.2007.61
        http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=4069257&tag=1

        Parameters
        --------
        classes: a List of strings

        n_components: number of components
        """
        super(SML,self).__init__(classes,feature_extractors,color_mode)
        self.covariance_type = covariance_type
        self.n_components = n_components
        self.k = n_components
        self.n_scomponents = n_scomponents
        self.extracted_features_ = {}
        self.class_models_ = {}
        self.N = N
        self.classifier = classifier

    def fit(self, dataset):
        """
        Parameters:
        dataset: a List of List of Strings. \
        each List of Strings contains the images that \
        belong to a specific class.
        """
        #Learning Step
        #n_samples_ -> number of samples per class
        #n_samples -> mininum number of samples per class
        #I know this sucks but we need time to fix.
        self.n_samples_ = dataset.shape
        self.n_samples = min(dataset.shape)
        #B = [[]]
        if len(dataset) > 0:
            ppp = []
            nscom = self.n_scomponents
            data_size = sum(dataset.shape)
            ppp.append(np.empty((data_size * nscom)))
            ppp.append(np.empty((data_size * nscom, self.dim())))
            #TODO: this is for diag covariance only, remember to handle full cov as well.
            ppp.append(np.empty((data_size * nscom, self.dim())))
        
            for c, cn in enumerate(self.classes):
                lgg.info("processing class %d:  %s" % (c, cn))
                D = dataset.shape[c]
                from_c = c * self.n_samples * nscom
                to_c = c * self.n_samples * nscom + self.n_samples * nscom
                if cn not in self.class_models_.keys():
                    for i, image in enumerate(dataset[cn]):
                        lgg.info("extracting features from " \
                        + dataset.repo[cn][i][-35:])
                        B = self._extract_features(image)
                        g = mx.GMM(n_components = nscom, \
                        covariance_type=self.covariance_type)
                        g.fit(np.asarray(B))
                        #save the parameters
                        from_i = from_c + i * nscom
                        to_i = from_c + i * nscom + nscom
                        
                        ppp[0][from_i:to_i] = g.weights_ * (1. / D)
                        ppp[1][from_i:to_i] = g.means_
                        ppp[2][from_i:to_i] = g.covars_

                    hg = HGMM(n_components=self.n_components,thresh=0.1,n_init=1,N=self.N)
                else:
                    lgg.info('sub gmm preloaded!')
                    hg = self.class_models_[cn]
                    ppp[0][from_c:to_c] = (hg.sweights_)
                    ppp[1][from_c:to_c] = (hg.smeans_)
                    ppp[2][from_c:to_c] = (hg.scovars_)
                    
                #self.filter_bad_clusters(params)
                params = [ppp[0][from_c:to_c],ppp[1][from_c:to_c],ppp[2][from_c:to_c]]
                if 'bayes' in self.classifier or \
                'hist_loc' in self.classifier:
                    hg.fit(params)
                    if hg.converged_ == False:
                        lgg.warn("Not Converged!")
                elif 'hist_uni' in self.classifier:
                    #in case we don't fit class models, we still need to store the params
                    #for the 'save' function to work.
                    hg.set_data(params)
                #class_models must always be available as we need them to store
                # 'sub means'
                self.class_models_[cn] = hg
                
            #TODO: Experimental!!----------------------------------------------
            if 'hist' in self.classifier:
                lgg.info("---------------- Generating universal dict---------")
                #universal dict
                if 'uni' in self.classifier:
                    #''' using universal dict is better !
                    hgx = HGMM(n_components=self.n_components * self.n_classes, \
                    thresh=0.1,n_iter=1000, n_init=1,N=self.N)
                    hgx.fit(ppp)
                    self.dict_ = hgx.means_[:]
                #local dict
                elif 'loc' in self.classifier:
                    d = []
                    for c in self.classes:
                        d.append(self.class_models_[c].means_)  
                    self.dict_ = np.vstack(d)

                self.n_textons = len(self.dict_)
                #self.k = self.n_components
                #TODO: all n_samples_[] are equal
                self.histogram_ = np.empty((self.n_classes,self.n_samples, \
                self.n_textons),dtype=np.float32)                    
                lgg.info("------------- Generating models------------")
                for cx,c in enumerate(self.classes):
                    for ix, image in enumerate(dataset[c]):
                        self.histogram_[cx,ix] = self.calc_histogram(image)
                
                
    def filter_bad_clusters(self,params):
        blk = np.loadtxt("/home/phan/workspace/climaz/data/\
        SCM_CLIM/output/sml/raw/black_list.txt")
        indices = [idx for (idx,i) in enumerate(params[1]) \
        if all([np.convolve(i,j).max() < 135 for j in blk])]
        params[0] = params[0][indices] 
        params[1] = params[1][indices]
        params[2] = params[2][indices]    
        #return params[0][indices], params[1][indices], params[2][indices]
     
    def annotate(self, image, take=1, weights=[]):
        # Annotation Step
        # Find the class with highest log likelihood
        B = self._extract_features(image)

        scores = []
        total = float(sum(self.n_samples_))
        #total_scom = sum(self.class_models_)
        for c, cn in enumerate(self.classes):
            #TODO: we are asumming the numbers of images in each class are equal
            score = 0.
            score += np.sum(self.class_models_[cn].score(B)) #/ float(len(B))
            #score += np.sum(self.class_models_[cn].score(g.means_))
            #score += np.log(self.n_samples_[c] / total)
            if len(weights) == len(self.classes):
                score -= np.log(weights[c])
            scores.append( score )
        #scores.sort()
        return scores
    
    def annotate_map(self,image):
        f_vectors = self.feature_extractors[0].extract(image)[1]
        pos = self.feature_extractors[0].extract(image)[0]
        
        for c, cn in enumerate(self.classes):
            for f in f_vectors:
                score = self.class_models_[cn].score(f)
                
 
    def classify(self, image):
        if 'bayes' in self.classifier: 
            scores = self.annotate(image,take=self.n_classes)
        elif 'hist' in self.classifier:
            return self.hist_classify(image)
        return np.argmax(scores), scores

    def load(self,data_size=(0,0),prefix="sml_",suffix=".txt",load='df'):
        self.class_models_ = {}
        self.histogram_ = []
        for c,cn in enumerate(self.classes):
            self.extracted_features_[cn] = []
            self.histogram_.append([])
            if 'hist' in self.classifier:
                self.n_samples_ = []
                if os.path.isfile(prefix + "hist_class_" + cn + suffix):
                    self.histogram_[c] = np.loadtxt(prefix + "hist_class_" + cn + suffix)
                    self.n_samples_.append(len(self.histogram_[c]))
                else: self.histogram_ = []    
                self.n_samples = min(self.n_samples_)
                
            hg = HGMM(n_components=self.n_components,n_init=1,N=self.N)
            if 'bayes' in self.classifier or \
            'hist_loc' in s:
                hg.means_ = np.loadtxt(prefix + "means_" + cn + suffix)
                hg.weights_ = np.loadtxt(prefix + "weights_" + cn + suffix)
                hg.covars_ = np.loadtxt(prefix + "covars_" + cn + suffix)
                
            hg.sweights_ = np.loadtxt(prefix + "sweights_" + cn + suffix)
            hg.scovars_ = np.loadtxt(prefix + "scovars_" + cn + suffix)
            hg.smeans_ = np.loadtxt(prefix + "smeans_" + cn + suffix)
                
            self.class_models_[cn] = hg
                
        if 'hist' in self.classifier:
            if os.path.isfile(prefix + "dict" + suffix):
                self.dict_ = np.loadtxt(prefix + "dict" + suffix)
                self.n_textons = len(self.dict_)
            else: 
                self.dict_ = []
                self.n_textons = 0
        '''
        if 'f' in load:    
            if data_size != (0,0):
                for ix in range(data_size[1]):
                    f = np.loadtxt(prefix + "feature_" \
                    + cn + "_image_" + repr(ix) + suffix)
                    self.extracted_features_[cn].append(f)
        '''

    def save(self,prefix="sml_",suffix=".txt"):
        for c,cn in enumerate(self.classes):
            if 'bayes' in self.classifier or \
            'hist_loc' in self.classifier:
                np.savetxt(prefix + "means_" + cn + suffix,self.class_models_[cn].means_)
                np.savetxt(prefix + "weights_" + cn + suffix,self.class_models_[cn].weights_)
                np.savetxt(prefix + "covars_" + cn + suffix,self.class_models_[cn].covars_)
                
            np.savetxt(prefix + "scovars_" + cn + suffix,self.class_models_[cn].scovars_)
            np.savetxt(prefix + "sweights_" + cn + suffix,self.class_models_[cn].sweights_)
            np.savetxt(prefix + "smeans_" + cn + suffix,self.class_models_[cn].smeans_)
            
            if 'hist' in self.classifier:
                np.savetxt(prefix + "hist_class_" + cn + suffix, self.histogram_[c])
            
        if 'hist' in self.classifier:
            try:
                np.savetxt(prefix + "dict" + suffix, self.dict_)
            except IOError as e:
                lgg.error("I/O error({0}): {1}".format(e.errno, e.strerror))
            except:
                lgg.error("Unexpected error:", sys.exc_info()[0])
            #for ix,_ in enumerate(self.extracted_features_[cn]):
            #    np.savetxt(prefix + "feature_" + cn + "_image_" + repr(ix) + suffix, i)

def main(argv):
    """
    The main function
    """
if __name__ == "__main__":
    main(sys.argv)
    
