# -*- coding: utf-8 -*-
"""
Created on Sat Oct 19 16:19:50 2013

@author: phan
"""
from VZ import VZ
from SML import *
from Dataset import *
import numpy as np
from features import *
from viz import TextonViz as tvz
import os
import logging as lgg

class ClimazConfig:
    def __init__(self, classes=[], root="", color_mode='F',feature = 'raw', \
    mode = 'l',clfs = 'sml',ncom = 32, nscom = 16, p = 25, spacing = 25, \
    n_samples = 60, split = [20,40], classifier = "bayes"):
        self.classes = classes
        self.root = root
        self.color_mode = color_mode
        self.feature = feature
        self.run_mode = mode
        self.clfs = clfs
        self.ncom = ncom
        self.nscom = nscom
        self.spacing = spacing
        self.p = p
        self.n_samples = n_samples
        self.split = split
        self.classifier = classifier
        
    def loadtxt(self,file):
        pass
        
def batch_test(datasets,cfgs):
    '''
    datasets: tuples of train - test data
    cfgs: list of ClimazConfigs
    '''
    #TODO just one dataset now
    all_scores = []    
    accuracies = []
    for dx, dataset in enumerate(datasets):
        train, test = dataset
        for cfx, cfg in enumerate(cfgs):
            if cfg.feature == 'dct':
                fex = [DCT.DCT(patch_size=(cfg.p,cfg.p), \
                spacing = cfg.spacing,padding='none',normalizer='none')]
            elif cfg.feature == 'raw': 
                fex = [Raw.Raw(patch_size=(cfg.p,cfg.p), \
                spacing = cfg.spacing,padding='none',normalizer='weber', \
                skip_blank=False,circular=False)]
            elif cfg.feature == 'srp':
                fex = [SRP.SRP(r_dim = 80, \
                patch_size=(cfg.p,cfg.p), \
                spacing = cfg.spacing, \
                sorting_method="radial", \
                padding='none', \
                normalizer='unit')]
            elif cfg.feature == 'stripe':
                fex = [Stripe.Stripe(vector_length=cfg.p,spacing=cfg.spacing,padding='symmetric', \
                normalizer='weber')]
                
            if cfg.clfs == 'vz':
                clf = VZ(k=cfg.ncom, classes = cfg.classes, \
                feature_extractors = fex, color_mode=cfg.color_mode)
            elif cfg.clfs == 'sml':
                clf = SML(cfg.classes,n_components=cfg.ncom, \
                covariance_type='diag',n_scomponents=cfg.nscom, \
                feature_extractors = fex, color_mode = cfg.color_mode,N=1000, \
                classifier=cfg.classifier)
                
            path = cfg.root + \
                "output/%s_%s_%dclasses_%dsmpl_%dncom_%dnscom_%dp_%dspc_%s_%s/" \
                % (cfg.clfs, cfg.classifier, len(cfg.classes), train.shape[0], cfg.ncom,cfg.nscom, \
                cfg.p, cfg.spacing, cfg.feature, cfg.color_mode)
                
            lgg.info("running test for: " + path)
                
            if not os.access(path, os.F_OK):
                os.makedirs(path)
            
            if 'l' in cfg.run_mode:
                clf.load((len(cfg.classes),cfg.split[0]),prefix = path,load = 'deh')
                if 'f' in cfg.run_mode:
                    clf.fit(train)
                if 's' in cfg.run_mode:
                    clf.save(prefix = path)
                    
            if 's' in cfg.run_mode and 'l' not in cfg.run_mode:
                clf.fit(train)     
                clf.save(prefix = path)
                
            if 'v' in cfg.run_mode:
                nc = color_dict[cfg.color_mode]
                if cfg.clfs == 'sml':
                    if 'loc' in cfg.classifier:
                        for cx, c in enumerate(cfg.classes):
                            tvz.visualize(clf.class_models_[c].means_, \
                            (cfg.p,cfg.p,nc),c,root = path)
                    if 'uni' in cfg.classifier:
                        for cx, c in enumerate(cfg.classes):
                            tvz.visualize(clf.dict_[clf.k*cx : clf.k * cx + clf.k], \
                            (cfg.p,cfg.p,nc),c,root = path)  
                        '''
                        for sx in range(train.shape[0]):
                            tvz.visualize(clf.class_models_[c].smeans_[ \
                            cfg.nscom * sx : cfg.nscom * sx + cfg.nscom], \
                            (cfg.p,cfg.p,nc),c+str(sx),root = path)
                        '''
    
                elif cfg.clfs == 'vz':
                    for cx, c in enumerate(cfg.classes):
                        tvz.visualize(clf.dict_[clf.k*cx : clf.k * cx + clf.k], \
                        (cfg.p,cfg.p,nc),c,root = path)  
                        
            if 't' in cfg.run_mode:
                result,accuracy,scores = clf.test(test)
                #print 'result: ' + str(result) + 'accuracy: ' + str(accuracy)
                lgg.info('dataset: ' + str(dx) + '| algo:' + cfg.clfs + '_' + cfg.classifier + \
                '| result: ' + str(result) + '| accuracy: ' + str(accuracy))
                #all_scores.append(scores)
                accuracies.append(accuracy)
                
    accuracies = np.array(accuracies)
    accuracies = accuracies.reshape(len(datasets),len(cfgs))
    
    for cfg in range(len(cfgs)):
        lgg.info("ACCURACIES OF CONFIG %d (%s) ARE: %s" % (cfg, cfgs[cfg].clfs + '_' \
        + cfgs[cfg].classifier,str(accuracies[:,cfg])))
        lgg.info("MEAN ACCURACY IS: %s" % str(accuracies[:,cfg].mean()))
        lgg.info("STD ACCURACY IS: %s" % str(accuracies[:,cfg].std()))
    '''
    if all([c.clfs == 'sml' for c in cfgs] and len(all_scores)) != 0:
        sum_scores = np.sum(all_scores,axis=0)
        
        result = np.sum(np.equal(np.argmax(sum_scores,axis=2), \
        np.tile(np.arange(sum_scores.shape[0])[:,np.newaxis],sum_scores.shape[1])))
        
        accuracy = result.sum() / float(test.size()) * 100
    '''   
    #return result, accuracy

if __name__ == '__main__':
    '''Things to test:
        * number of training data
        * feature: raw vs dct vs srp
        * ncom 16 24 32 64
        * nscom 6 8 10 12 14 16
        * p = 8 12 16 20 24
    '''
    #box_vs_circle
    #classes = ["has_box", "has_circle","has_triangle"]
    #root = "/home/phan/workspace/climaz/data/box_vs_circle/"    
    
    #FMD
    #classes = ['water','foliage', 'glass','fabric','leather','paper','wood', \
    #'plastic', 'stone', 'metal']
    #classes = ['water','foliage','fabric','wood']
    #root = '/home/phan/workspace/climaz/data/FMD/image/' 
    
    #UIUC DB
    #classes = ["class" + str(i) for i in range(1,4)]
    #root = "/home/phan/workspace/climaz/data/uiuc/"
    
    #classes = ["fabric","foliage","glass","leather","metal","paper","plastic","stone","water","wood"]
    #root = "/home/phan/workspace/climaz/data/FMD/image/"

    #SCM_CLIM2: ncom = 40, nscom = 25, spacing = 10, p = 10
    #classes =  ["fleece","denim","knit1", "boucle","lace","leather", \
    #"corduroy","chiffon","satin","fur"]
    #classes =  ["fleece","denim","knit1", "boucle","lace","corduroy","chiffon","fur"]
    #classes = ["fleece","denim","knit1"]#, "boucle","corduroy","fur"]
    #root = "/home/phan/workspace/climaz/data/SCM_CLIM2/"
    
    #40, 12, 7
    #classes = ["orange_peel","sponge", "corduroy", "cracker", \
    #"sandpaper", "brown_bread", "styrofoam", "cotton", "aluminium_foil", "linen"]
    classes = ["cotton","sandpaper"]
    root = "/home/phan/workspace/climaz/data/KTH_TIPS_GRAY/"

    #KTH-TIPS2 - ~55% for universial hist, ~56% for combined hist,
    #classes = ["aluminium_foil","brown_bread","corduroy","cork","cotton", \
    #"cracker", "lettuce_leaf", "linen","white_bread", "wood", "wool"]
    
    #root = "/home/phan/workspace/climaz/data/KTH-TIPS2/"
    #---------------------- configs -------------------------------------------
    lgg.basicConfig(filename=root + "climaz.log", level=lgg.DEBUG, \
    format='%(asctime)s:%(levelname)s:%(message)s')
    lgg.info("-----------------------Started---------------------------------")
    cfg = [ClimazConfig()]
    cfg[0].classes = classes
    cfg[0].root = root
    cfg[0].color_mode = 'L'
    cfg[0].feature = 'raw'
    cfg[0].run_mode = 'stv' # or 's'
    cfg[0].clfs = 'sml'
    cfg[0].ncom = 36 #35, 40
    cfg[0].nscom = 12 #12, 15
    cfg[0].spacing = 7#15
    cfg[0].p = 7#15 for SCM_CLIM #7 for KTH-TIPS
    cfg[0].n_samples = 81
    cfg[0].split = [10,10]
    cfg[0].classifier = 'hist_loc'
        
    #add 3 more configs by copying the original one
    import copy

    cfg2 = copy.deepcopy(cfg[0])
    cfg2.clfs = "vz"
    cfg2.classifier = 'hist'
    #cfg.append(cfg2)
    
    cfg1 = copy.deepcopy(cfg[0])
    cfg1.clfs = "sml"
    cfg1.classifier = 'hist_uni'
    #cfg.append(cfg1)

    cfg3 = copy.deepcopy(cfg[0])
    cfg3.clfs = "sml"
    cfg3.feature = "raw"
    cfg3.spacing = 10
    cfg3.p = 10
    cfg3.classifier = 'hist_loc'
    #cfg.append(cfg3)
    '''
    for p, ncom in [(24,40),(30,48)]:
        import copy
        cfg1 = copy.deepcopy(cfg[0])
        cfg1.p = p
        cfg1.ncom = ncom
        cfg1.spacing = p/2
        #cfg.append(cfg1)
    '''
    #--------------------- preparing dataset-----------------------------------
    dss = []
    #Have tested the dataset so that multiple retrieval and feature extractor won't affect it
    for i in range(1):
        image_pattern = re.compile(r'.png')
        sort_key = lambda img: int(image_pattern.match(img).group(1))
        
        repo = DirRepo(cfg[0].root,cfg[0].classes,image_pattern, sort_key=sort_key)
        retrv = ImageRetriever(cfg[0].color_mode,normalized=True)
        ds = Dataset(cfg[0].classes,cfg[0].n_samples,repo,retrv,True)
        ds.shuffle()
        train, test = ds.split(cfg[0].split)
        dss.append((train,test))

    #-------------------- run and test ----------------------------------------
    batch_test(dss,cfg)
