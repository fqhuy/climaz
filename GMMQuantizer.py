# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 00:42:25 2014

@author: phan
"""
from Dataset import *
import numpy as np
from features import *
import logging as lgg
from sklearn import mixture as mx
from sklearn.decomposition import PCA
from features import vlfeat as vl
from SML import HGMM

def gmm_soft_quantize(root,classes):
    pass

def gmm_soft_quantize(ds,classes,n_components,method='hgmm',load='fp'):
    fss = []
    for cx,c in enumerate(classes):
        if 'f' in load:        
            fss.append(np.load(ds.repo.root + 'sift_' + c + '.npy'))
        else:
            fs = []
            lgg.info("-----class %s-----" % c)
            for ix, img in enumerate(ds[c]):
                lgg.info("image " + ds.repo[c][ix][-35:])
                f = DSIFT.extract_feature_dsift(img,spacing=10,padding=0)
                f = f.reshape(f.shape[0] * f.shape[1], f.shape[2])
                fs.append(f)
            data = np.concatenate(fs)
            np.save(ds.repo.root + 'sift_' + c + '.npy', data)
            fss.append(data)

    data = np.vstack(fss)
    #np.save(ds.repo.root + 'patches_sift_all.npy',data)
    lgg.info("begin PCA... data size: (%d, %d)" % (data.shape[0],data.shape[1]))
    pca = PCA(n_components=n_components,copy= True,whiten=True)
    if 'p' in load:
        pca_data = np.load(ds.repo.root + 'pca.npy')
        pca.components_ = pca_data[0]
        pca.explained_variance_ratio_ = pca_data[1]
        pca.mean_ = pca_data[2]
    else:
        pca.fit(data)
        arr = np.array([pca.components_, pca.explained_variance_ratio_, pca.mean_])
        np.save(ds.repo.root + 'pca.npy', arr)
    
    if method is 'local':
        for cx,cls in enumerate(classes):
            reduced_data = pca.transform(fss[cx])
            lgg.info("begin clustering... data size: (%d, %d)" \
            % (reduced_data.shape[0],reduced_data.shape[1]))
            
            #g = mx.GMM(n_components = 40, covariance_type='diag')
            #g.fit(reduced_data)
            g = vl.GMM(max_iter = 50, n_dimensions = 64, n_components = 40)
            reduced_data = np.ascontiguousarray(reduced_data,dtype=np.float32)
            g.cluster(reduced_data)
            
            arr = np.array([g.weights_,g.means_,g.covars_])
            np.save(ds.repo.root + 'gmm_' + cls + '.npy', arr)
            
    elif method is 'global':
        reduced_data = pca.transform(data)
        g = vl.GMM(max_iter = 50, n_dimensions = 64, n_components = 200)
        reduced_data = np.ascontiguousarray(reduced_data,dtype=np.float32)
        g.cluster(reduced_data)
        arr = np.array([g.weights_,g.means_,g.covars_])
        np.save(ds.repo.root + 'gmm.npy', arr)
        
    elif method is 'hgmm':
        dsift = DSIFT.DSIFT(spacing=10,padding=0)
        params = [None] * 3
        params[0] = []
        params[1] = [] 
        params[2] = []
        for cx, c in enumerate(classes):
            lgg.info('class %s' % c)
            current = 0
            reduced_data = pca.transform(fss[cx])
            for ix, i in enumerate(ds[c]):
                g = vl.GMM(max_iter = 50, n_dimensions = 64, n_components = 20)                
                lgg.info('image %d' % ix)
                shape = dsift.feature_vector_shape(i)
                nxt = current + shape[0] * shape[1]
                image_data = reduced_data[current: nxt]
                g.cluster(image_data)
                
                #g = mx.GMM(n_components = 20, n_iter = 100)
                #g.fit(image_data)
                current = nxt
                params[0].append(g.weights_ * (1. / ds.shape[cx]))
                params[1].append(g.means_)
                params[2].append(g.covars_)
            assert(current == reduced_data.shape[0])

        lgg.info('stacking')
        params[0] = np.concatenate(params[0])
        params[1] = np.concatenate((params[1]))
        params[2] = np.concatenate((params[2]))
        
        lgg.info('computing HGMM')
        hg = HGMM(n_components=200,thresh=0.1,n_init=1,N=1000)
        hg.fit(params)
        
        lgg.info('saving results')
        np.save(ds.repo.root + 'hgmm_weights.npy',hg.weights_)
        np.save(ds.repo.root + 'hgmm_means.npy',hg.means_)
        np.save(ds.repo.root + 'hgmm_covars.npy',hg.covars_)
        #arr = np.array([hg.weights_, hg.means_, hg.covars_])
        #np.save('hgmm.npy',arr)
        
        
if __name__ == '__main__':
    root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/'
    lgg.basicConfig(filename=root + "climaz_gmm_soft_quantizer.log", level=lgg.DEBUG, \
    format='%(asctime)s:%(levelname)s:%(message)s')
    lgg.info("-----------------------Started---------------------------------")    
    
    #classes =  ["fleece","denim","knit1", "boucle","lace","leather", \
    #"corduroy","chiffon","satin","fur"]
    classes = ['fleece','knit1','corduroy', 'fur','denim','leather','lace','boucle']
    image_pattern = re.compile(r'image_([0-9]+)_0.pgm')
    #split_pattern = re.compile(r'_|\.')
    #sort_key = lambda img: int(split_pattern.split(img)[1]) * 21 + int(split_pattern.split(img)[2])
    sort_key = lambda img: int(image_pattern.match(img).group(1))

    repo = DirRepo(root,classes,image_pattern,sort_key=sort_key)
    retrv = ImageRetriever('F')
    ds = Dataset(classes,60,repo,retrv,True)
    gmm_soft_quantize(ds,classes,64,load='fp',method='hgmm')