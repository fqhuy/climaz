#!/bin/bash
declare -a patterns=('hexagons' 'checkerboard' 'circles' 'leftshingle' 'horizontal')
declare -a shapes=('triangle' 'rectangle' 'ellipse')
declare -a colors=('red' 'green' 'blue' 'violet' 'purple' 'orange' 'black' 'brown' 'yellow' 'black')
for i in {1..200};
do 
 FNAME="data/image_$i.jpg"
 P=$(($RANDOM % 5))
 #pattern:${patterns[P]}
 #pattern:gray100
 convert -size 128x128 pattern:${patterns[P]}  $FNAME
 X=$(($RANDOM % 100))
 Y=$(($RANDOM % 100))
 
 for j in {1..5}
 do
     X=$(($RANDOM % 100))
     Y=$(($RANDOM % 100))
     S=$(($RANDOM % 3))
     C=$(($RANDOM % 10))
     #echo $P
     
     if [ $S == 0 ]
     then
       convert $FNAME  -fill ${colors[S]} -draw "path 'M $X,$Y  L $((X + 20)),$((Y+5))  L $((X + 20)),$((Y-15)) L $X,$Y Z'" $FNAME
     fi

     if [ $S == 1 ]
     then
       convert $FNAME  -fill ${colors[S]} -draw "circle $X,$Y $((X + 10)),$((Y + 10))" $FNAME
     fi

     if [ $S == 2 ]
     then
       convert $FNAME  -fill ${colors[S]} -draw "rectangle $X,$Y $((X + 10)),$((Y + 20))" $FNAME
     fi
 done
 #convert $FNAME -fill 'violet' -draw "circle $X,$Y $((X + 15)),$((Y + 15))" $FNAME
 convert $FNAME  -fill 'blue' -draw "path 'M $X,$Y  L $((X + 10)),$((Y+5))  L $((X + 20)),$((Y-15)) L $X,$Y Z'" $FNAME
done
	

