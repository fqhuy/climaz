# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 15:43:43 2013

@author: phan
"""
import numpy as np
import features.Feature as ft
from PIL import Image
from sklearn import cluster
import scipy.stats as stat
import os, sys
import os.path 
from sklearn import neighbors
from Model import ClassifierModel
from viz import TextonViz as tvz
from features import *
from Dataset import *
import pylab as P
import logging as lgg

class VZ(ClassifierModel):
    def __init__(self, k, classes, feature_extractors=[],color_mode="L"):
        """
        data: class - image
        k: number of textons per class
        color_mode: PIL modes L = grayscale
        """
        super(VZ,self).__init__(classes,feature_extractors,color_mode)
        self.dict_ = []
        self.n_samples = 0
        self.data = None
        self.k = k
        self.n_textons = k * self.n_classes
        self.histogram_ = []
        self._extracted_data = []
        
    def fit(self,dataset):
        #print "fitting data: n_classes = %d" % (len(data))
        self.data = dataset
        #TODO: assuming n_samples are equal
        self.n_samples = dataset[self.classes[0]].n
        lgg.info("generating textons...\n")
        lgg.info("===============================================================")
        
        #if feature vectors are not preloaded        
        if len(self.dict_) == 0:
            self.generate_textons()
            #self.save(prefix = "output/vz/vz_")
        else:
            print "dict_ is preloaded!"

        lgg.info("calculating the histograms of all images...")
        lgg.info("===============================================================")
        
        if len(self.histogram_) == 0:
            for cx,c in enumerate(self.classes):
                self.histogram_.append([])
                for ix, image in enumerate(self.data[c]):
                    self.histogram_[cx].append(self.calc_histogram(image)) #,(cx,ix)
            
            #self.save(prefix = "output/vz/vz_")
        else:
            lgg.info("histograms are preloaded!")
                
        #self.save(prefix="output/vz/vz_")
    
    def generate_textons(self):
        """
        """
        for cx, c in enumerate(self.classes):
            fs = []
            #self._extracted_data.append([])
            lgg.info("-----class %s-----" % c)
            for i, image in enumerate(self.data[c]):
                lgg.info("image " + self.data.repo[c][i][-35:])
                f = self._extract_features(image)
                #save the extracted features to use later
                fs.append(f)
            
            #TODO: we can have 'f' with different numbers of feature vectors, 
            # remember this place.
            self._extracted_data.append(fs)
            #data = np.asfarray(fs)
            data = np.vstack(fs)
            #aggregate all feature vectors from all images belonging to the
            # same class
            
            #data = data.reshape(data.shape[0] * data.shape[1],data.shape[2])
            lgg.info("begin clustering... data size: (%d, %d)" \
            % (data.shape[0],data.shape[1]))
            centers = cluster.KMeans(
                    n_clusters= self.k, max_iter=10000,
                    precompute_distances=True,tol = 1e-2, n_jobs=-1
                    ).fit(data).cluster_centers_
            
            #TODO: should we add a second K-means here to reduce the number of
            #clusters
            for d in centers: self.dict_.append(d)
   
    def classify(self,image):
        """
        classify a new image using the internal statistics
        """
        return self.hist_classify(image)
        
    def save(self,prefix="vz_",suffix=".txt"):
        #save the dictionary
        np.savetxt(prefix+"dict"+suffix, np.asfarray(self.dict_))
        #save histograms
        for cx,c in enumerate(self.histogram_):
            np.savetxt(prefix+"hist_class_%d" % cx + suffix, np.asfarray(c))
        #save extracted features
        for cx,c in enumerate(self._extracted_data):
            for ix,i in enumerate(c):
                np.savetxt(prefix+"class_%d" % cx \
                + "_image_%d" % ix + suffix,self._extracted_data[cx][ix])
            
    def load(self,data_size,prefix="vz_",suffix=".txt", load="deh"):
        """
        data_size = (n_classes, n_samples)
        """
        if 'd' in load:
            dict_ = np.loadtxt(prefix+"dict"+suffix)
            for d in dict_:
                self.dict_.append(d)
            
        n_classes, n_samples = data_size
        self.n_classes = n_classes
        self.n_samples = n_samples
        self.n_textons = n_classes * self.k
        
        if 'e' in load:
            for c in range(n_classes):
                self._extracted_data.append([])
                for s in range(n_samples):
                    self._extracted_data[c].append( \
                    np.loadtxt(prefix+"class_%d" % c + "_image_%d" % s + suffix))
                    
        if 'h' in load:
            files = [f for f in os.listdir(prefix) if f.find("hist_class_") != -1]
            
            if len(files) > 0:
                for c in range(n_classes):
                    self.histogram_.append([])
                    chist = np.loadtxt(prefix+"hist_class_%d" % c + suffix)
                    for s in range(n_samples):
                        self.histogram_[c].append(chist[s])
                    
if __name__ == "__main__":
    pass