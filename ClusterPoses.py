# -*- coding: utf-8 -*-
"""
Created on Sun Mar  2 20:00:14 2014

@author: phan
"""
import os
import numpy as np
from scipy.spatial.distance import euclidean
from sklearn.cluster import KMeans
import matplotlib
import matplotlib.pyplot as plt

def parts2features(parts):
    center = ((parts[0][2] + parts[0][0])/2., (parts[0][3] + parts[0][1])/2.)
    feat = np.zeros((len(parts) - 1 , 2))
    for i, part in enumerate(parts[1:]):
        pos = ((part[2] + part[0])/2., (part[3] + part[1])/2.)
        feat[i,0] = euclidean(pos, center)
        feat[i,1] = np.arctan2(pos[1] - center[1], pos[0] - center[0]);
    #feat = np.sort(feat)
    feat = feat[feat[:,1].argsort(axis=0)]
    return feat.flatten()
    
def sortParts(parts):
    center = ((parts[0][2] + parts[0][0])/2., (parts[0][3] + parts[0][1])/2.)
    feat = np.zeros((len(parts) - 1 , 3))
    for i, part in enumerate(parts[1:]):
        pos = ((part[2] + part[0])/2., (part[3] + part[1])/2.)
        feat[i,0] = i + 1
        feat[i,1] = euclidean(pos, center)
        feat[i,2] = np.arctan2(pos[1] - center[1], pos[0] - center[0]);
    #feat = np.sort(feat)
    feat = feat[feat[:,2].argsort(axis=0)]
    parts_ = parts[feat[:,0].astype(int)]
    return parts_
    
def visualize(images,labels,clusters,parts):
    for cl in clusters:
        fig = plt.figure('cluster ' + str(cl))
        ax = fig.add_subplot(111)
        
        rect = matplotlib.patches.Rectangle((parts[cl][0], parts[cl][1]),parts[cl][2] - parts[cl][0], \
        parts[cl][3] - parts[cl][1],fill=False,edgecolor='blue')
        ax.add_patch(rect)
        #for p in parts[cl][0:1]:
        #    rect = matplotlib.patches.Rectangle((p[0], p[1]),p[2] - p[0], \
        #    p[3] - p[1],fill=False,edgecolor='blue')
        #    ax.add_patch(rect)
            
        for ix, i in enumerate(images):
            if labels[ix] == cl:
                for part in i[1:10]:
                    rect = matplotlib.patches.Rectangle((part[0],part[1]),part[2] - part[0], part[3] - part[1],fill=False)
                    ax.add_patch(rect)
    
        plt.xlim([0, 1000])
        plt.ylim([0, 1000])
        plt.show()
        
def root_filters(classes, images, labels,n_clusters, saveroot):
    def _area(rect):
        return (rect[2] - rect[0]) * (rect[3] - rect[1])
        
    def _greater(rect1,rect2):
        if (rect1[0] < rect2[0]) and (rect1[1] < rect2[1]) and (rect1[2] > rect2[2]) and (rect1[3] > rect2[3]):
            return True
        else:
            return False;
        
    roots = np.zeros((len(classes), n_clusters,4))
    for cx, c in enumerate(classes):
        for clx in range(n_clusters):
            root = np.zeros(4)
            mx = 0
            for ix,i in enumerate(images[cx]):
                if labels[cx][ix] == clx:
                    #root += i[0]
                    #if _greater(i[0], root):
                    if (i[0][2] - i[0][0]) > root[2] - root[0]:
                        root[0] = i[0][0]
                        root[2] = i[0][2]
                    if (i[0][3] - i[0][1]) > root[3] - root[1]:
                        root[1] = i[0][1]
                        root[3] = i[0][3]
                                        
            #root /= float((labels[cx] == clx).sum())
            roots[cx, clx, :] = root
        np.save(saveroot + c + '_root_inits.npy',roots[cx,:,:])
    return roots
    #np.save(saveroot + 'root_inits_allclasses.npy',roots)       
    
def part_filters_(classes, images, labels,n_clusters, saveroot):
    parts = np.zeros((len(classes), n_clusters, 21, 4))
    for cx, c in enumerate(classes):
        for clx in range(n_clusters):
            cimg = []
            for ix,i in enumerate(images[cx]):
                if labels[cx][ix] == clx:
                    cimg.append(sortParts(i))
            for p in range(0,20):
                rec = np.zeros(4)
                for i in cimg:
                    rec += i[p]
                rec /= float((labels[cx] == clx).sum())
                parts[cx, clx, p, :] = rec
        np.save(saveroot + c + '_parts_inits.npy',parts[cx,:,:,:])
    return parts
    
def part_filters_(classes, images, labels,n_clusters, saveroot):
    parts = np.zeros((len(classes), n_clusters, 21, 4))
    for cx, c in enumerate(classes):
        for clx in range(n_clusters):
            cimg = []
            for ix,i in enumerate(images[cx]):
                if labels[cx][ix] == clx:
                    cimg.append(sortParts(i))
            for p in range(0,20):
                rec = np.zeros(4)
                for i in cimg:
                    rec += i[p]
                rec /= float((labels[cx] == clx).sum())
                parts[cx, clx, p, :] = rec
        np.save(saveroot + c + '_parts_inits.npy',parts[cx,:,:,:])
    return parts
    
if __name__=="__main_":
    root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/'
    saveroot = root[:-1] + '_OUTPUT/'

    #classes =  ["fleece","denim","knit1", "boucle","lace","leather", \
    #"corduroy","chiffon","satin","fur"]
    classes = ['fleece','knit1','corduroy', 'fur','denim','leather','boucle','lace']
    cimages = []
    labels = []
    clusters = []
    n_clusters = 6
    for c in classes:
        if os.path.isdir(root + c):
            samples = np.zeros((60, 20 * 2),dtype='f')
            #ifvs = np.zeros((60, 400 * 64 + 40*20),dtype='f')
            images = []
            for i in range(60):
                fname = root + c + '/image_' + str(i)    

    
if __name__=="__main_":
    root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/'
    saveroot = root[:-1] + '_OUTPUT/'

    #classes =  ["fleece","denim","knit1", "boucle","lace","leather", \
    #"corduroy","chiffon","satin","fur"]
    classes = ['fleece','knit1','corduroy', 'fur','denim','leather','boucle','lace']
    cimages = []
    labels = []
    clusters = []
    n_clusters = 4
    for c in classes:
        if os.path.isdir(root + c):
            #samples = np.zeros((60, 400 * 8),dtype='f')
            samples = np.zeros((60, 20 * 2 + 20),dtype='f')
            #ifvs = np.zeros((60, 400 * 8),dtype='f')
            images = []
            hoglabels = np.load(saveroot + c + '_hog_clusters_2048.npz')['arr_0']
            hiss = [np.histogram(hoglabels[i * 20 : i * 20 + 20 ], bins=range(21))[0] for i in range(60)]
            for i in range(60):
                fname = root + c + '/image_' + str(i)
                hoglabel = hoglabels[i * 20 : i * 20 + 20]
                
                #samples[i,:] = np.load(saveroot + c + '/image_' + str(i) + '_allparts_ifv.npy')
                #images = samples
                f = open(fname + '.txt', 'r')
                parts = []
                for line in f.readlines():
                    bbox = np.asarray([float(n) for n in line.split(' ')])
                    parts.append(bbox)
                images.append(np.vstack(parts))
                samples[i,0 : 40] = parts2features(parts)
                samples[i, 40 : 60] = hiss[i]
                #ifvs[i,0:40] = samples[i,:]
                f.close()
            #np.save(saveroot + c + '_allparts.npy',np.concatenate(images))
            cimages.append(images)
            kmeans = KMeans(n_clusters=n_clusters)
            kmeans.fit(samples)            
            #kmeans1 = KMeans(n_clusters=n_clusters)
            #kmeans1.fit(ifvs)
            
            labels.append(kmeans.labels_)
            clusters.append(kmeans.cluster_centers_)
            np.save(saveroot + c + '_pose_clusters.npy',kmeans.cluster_centers_)
            np.save(saveroot + c + '_pose_clusters_labels.npy',kmeans.labels_)
            
            #sort the indices according to theirs frequency
            initFilters =  np.array([np.array(sorted(zip(cen[40:60], range(20)),reverse=True))[:,1] for cen in kmeans.cluster_centers_])
            np.save(saveroot + c + '_init_filters.npy',np.asarray(initFilters,dtype='int32'))
            #np.save(saveroot + c + '_pose_clusters_ifv.npy',kmeans1.cluster_centers_)
            #np.save(saveroot + c + '_pose_clusters_labels_ifv.npy',kmeans1.labels_)            
    
    #roots = root_filters(classes, cimages, labels, n_clusters, saveroot)
    #parts = part_filters(classes, cimages, labels, n_clusters, saveroot)
    #cls = 2
    #for c in range(6):
    #    visualize(cimages[cls],labels[cls],[c], roots[cls])
def worker(classes, root, saveroot, n_clusters):
    #number of patch clusters. 
    n_patch_clusters = 5
    for c in classes:
        if os.path.isdir(root + c):
            print 'processing class ' + c
            pose_clusters_labels = np.load(saveroot + c + '_pose_clusters_labels.npy')
            i_to_l = zip(range(60), pose_clusters_labels)
            patch_labels = []
            for cluster in range(n_clusters):
                imgs = [iid for (iid,label) in i_to_l if label == cluster]
                parts = []
                for i in imgs:
                    fname = root + c + '/image_' + str(i)
                    img = imread(fname + '.pgm', as_grey=True) / 255.
                    img = pyramid_reduce(img, 2)
                    #img_hog = PHOG.extract_feature_hog(img)
                    img_hog =  skhog(img, orientations = 8, pixels_per_cell=(8,8), \
                    cells_per_block=(2,2), visualise=False, normalise = True)
                    sz = np.sqrt((img_hog.shape[0] / 32))
                    img_hog = img_hog.reshape(sz, sz, 32)
                    f = open(fname + '.txt', 'r')
                    #f.readline()
                    for line in f.readlines()[1:]:
                        bbox = np.asarray([float(n) for n in line.split(' ')])
                        bbox /= 16
                        if bbox[0] + 8 >= sz: 
                            bbox[0] = sz - 8
                        if bbox[1] + 8 >= sz:
                            bbox[1] = sz - 8
                        parts.append(img_hog[bbox[1]:bbox[1]+8,bbox[0]: bbox[0]+8].flatten())
                    f.close()
                print 'clustering...'
                km = KMeans(n_clusters=n_patch_clusters)
                km.fit(np.asarray(parts))
                kmlabels = km.labels_.copy() + cluster * n_patch_clusters
                for (ix, i) in enumerate(imgs):
                    labels = np.zeros(21)
                    labels[1:] = kmlabels[ix * 20: ix * 20 + 20] #km.labels_[ix * 20: ix * 20 + 20]
                    labels[0] = cluster
                    np.save(saveroot + c + '/image_' + str(i) + '_parts_labels.npy', labels)
                    
                patch_labels.append(kmlabels)
            np.savez(saveroot + c + '_parts_labels.npz', patch_labels)
            
if __name__=="__main__":
    from skimage.transform import pyramid_reduce
    from skimage.io import imread
    from sklearn.cluster import KMeans
    from skimage.feature import hog as skhog
    import multiprocessing
    #from features import PHOG
    n_clusters = 4
    root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/'
    saveroot = root[:-1] + '_OUTPUT/'
    #classes = ['fleece','knit1','corduroy', 'fur','denim','leather','boucle','lace']
    classes = [['fleece', 'fur'], ['corduroy', 'denim'],['knit1', 'leather'], ['boucle', 'lace']]
    jobs = []
    for cc in classes:
        p = multiprocessing.Process(target=worker,args=(cc, root, saveroot, n_clusters))
        #worker(root, saveroot, scale, cc)
        jobs.append(p)
        p.start()    
