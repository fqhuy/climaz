# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 16:40:55 2013

@author: phan
"""
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from networkx.algorithms.approximation import clique
class MRF:
    def __init__(self):
        pass
    
N = 20
def total_energy(x,y):

    h = 0
    eta = 2.1
    beta  = 1.0
    term1 = 0
    term2 = 0
    term3 = 0
    #for xnode in x.flatten():
    #    term1 += h * g.node[xnode]['value']
        
    for i in range(1,N-1):
        for j in range(1,N-1):
            term2 += beta * x[i,j] * x[i-1,j]
            term2 += beta * x[i,j] * x[i+1,j]
            term2 += beta * x[i,j] * x[i,j-1]
            term2 += beta * x[i,j] * x[i,j+1]
            
    for i in range(0,N-1):
        term2 += beta * x[0,i] * x[0,i+1]
        term2 += beta * x[i,0] * x[i+1,0]
        term2 += beta * x[N-1,i] * x[N-1,i+1]
        term2 += beta * x[i,N-1] * x[i+1,N-1] 
        
    for i in range(0,N):
        for j in range(0,N):
            term3 += eta * x[i,j] * y[i,j]
            
    Exy = term1 - term2 - term3
    #pxy = np.exp(-Exy)
    pxy = 0
    return pxy, Exy
    
if __name__=="__main__":
    #N = 200
    im1 = np.zeros((N, N)) + 1
    sx = np.random.random_integers(0,N-1,40)
    sy = np.random.random_integers(0,N-1,40)
    
    #a square
    '''
    im1[30:70,30:40] = -1
    im1[30:70,60:70] = -1
    im1[45:55,30:70] = -1
    '''
    im1[5:15,5:15] = -1
    
    im2 = im1.copy()
    for (x,y) in zip(sx,sy):
        im2[x,y] *= -1
    im3 = im2.copy()
    x = np.arange(0,N * N).reshape(N, N)
    #ajx = np.zeros(N * N,N * N)
    g = nx.Graph()
    
    for i in range(1,N-1):
        for j in range(1,N-1):
            g.add_edge(x[i,j],x[i-1,j])
            g.add_edge(x[i,j],x[i+1,j])
            g.add_edge(x[i,j],x[i,j-1])
            g.add_edge(x[i,j],x[i,j+1])
            
    for i in range(0,N-1):
        g.add_edge(x[0,i],x[0,i+1])
        g.add_edge(x[i,0],x[i+1,0])
        g.add_edge(x[N-1,i],x[N-1,i+1]) 
        g.add_edge(x[i,N-1],x[i+1,N-1])

    for i in range(0,N):
        for j in range(0,N):
            g.add_edge(x[i,j],'y' + str(x[i,j]))
            g.node[x[i,j]]['value'] = im2[i,j].copy()
            g.node['y' + str(x[i,j])]['value'] = im2[i,j].copy()

    #--------------energy function---------------
    count = 0
    #Iterated Conditional Models
    while(True):
        count += 1
        for i in range(0,N-1):
            for j in range(0,N-1):
                p1,e1 = total_energy(im3,im2)
                #print 'total energy: %d' % e1
                im3[i,j] *= -1
                p2,e2 = total_energy(im3,im2)
                #e = e1 + e2
                if e1 < e2:
                    im3[i,j] *= -1
        
        if count >= 4:
            break
    #nx.draw_graphviz(g)
    
    plt.imshow(im2)
    plt.show()
    plt.imshow(im3)
    plt.show()    