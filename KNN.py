# -*- coding: utf-8 -*-
"""
Created on Mon Mar 17 14:46:25 2014

@author: phan
"""
import numpy as np
from sklearn.neighbors import KNeighborsClassifier, DistanceMetric
from sklearn import mixture as mx
from features import IFV
from matplotlib import pyplot, mpl

def confusion_mat(conf_arr, alphabet):
    norm_conf = []
    for i in conf_arr:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = pyplot.figure()

    pyplot.clf()
    pyplot.xlabel("Predicted Category")
    pyplot.ylabel("Actual Category")
    ax = fig.add_subplot(111)
    ax.set_aspect(1)
    res = ax.imshow(np.array(norm_conf), cmap=pyplot.cm.jet, 
                    interpolation='nearest', origin = 'upper')
    
    width = len(conf_arr)
    height = len(conf_arr[0])
    
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(conf_arr[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center')
    
    cb = fig.colorbar(res)
    #alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    pyplot.xticks(range(width), alphabet[:width], rotation = 30)
    pyplot.yticks(range(height), alphabet[:height], rotation = 30)
    #plt.savefig('confusion_matrix.png', format='png')
    pyplot.show()

def extract(gmm, cl, i, saveroot):
    mr8 = np.load(saveroot + cl + '/image_%d.npy' % i).reshape((1000,1000,8))
    mr8 = mr8[25:975, 25:975]
    mr8 = mr8[::4, ::4].reshape(56644,8)
    data = IFV.extract_feature_ifv_once(mr8,gmm)
    return data

#if __name__ == '__main__':
def worker(N, C, test_classes):
    accuracies = {''}
    n_neighbors = 8
    TOTAL = 60 #ALL SAMPLES
    root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/'
    saveroot = root[:-1] + '_OUTPUT/'    
    classes = ['fleece', 'fur', 'corduroy', 'denim','knit1', 'leather', 'boucle', 'lace']  #
    zclasses = dict(zip(classes, range(8)))
    #'_features_SIFT.npy' #'_allparts_ifv.npy' #'_features_N%d_C%s_Standard.npy' % (N, C) # '_features_obj.npy'
    method = '_features_N%d_C%s_Standard.npy' % (N,C)
    #test_classes = ['fleece', 'fur', 'corduroy', 'denim','knit1', 'leather', 'boucle', 'lace']  #
    #test_classes = ['fleece', 'corduroy', 'knit1',  'boucle' ]  #
    
    gmm = mx.GMM(n_components=200)
    gmm.means_  = []
    gmm.covars_ = []
    gmm.weights_ = []
    for c in classes:
        gmmdata = np.load(saveroot + 'gmm_' + c + '.npz')
        gmm.means_.append(gmmdata['arr_1'])
        gmm.covars_.append(gmmdata['arr_2'])
        gmm.weights_.append(gmmdata['arr_0'])
        
    gmm.means_ = np.concatenate(gmm.means_)
    gmm.covars_ = np.concatenate(gmm.covars_)
    gmm.weights_ = np.concatenate(gmm.weights_)
    
    #Run the test for 10 different splits
    accuracies = {}
    accuracies['all'] = []
    for cl in classes:
        accuracies[cl] = []

    latex = ''
    for test in range(10):
        latex += str(test)
        print 'split %d' % test
        X = [] 
        y = []
        files = {}
        for cl in classes:
            files[cl] = []
            flist = open(root + cl + '_' + str(test) + '.txt')
            line = flist.readline()
            while line != '':
                files[cl].append(line)
                line = flist.readline()
            
        for cx,cl in enumerate(classes):
            #print('working on class ' + cl)
            for i in range(N):
                #print('.')
                #data = np.load(saveroot + cl + '/image_%d_allparts_ifv.npy' % i)#.reshape(400, 64)
                #data = np.load(saveroot + cl + '/image_%d_features_N%s_C%s_HOG.npy' % (i, N, C))
                #data = np.load(saveroot + str(cl) + '/image_%d_features_SIFT.npy' % (i))
                #data = np.load(saveroot + str(cl) + '/image_%d_features_obj.npy' % (i))
                try:
                    #print cl
                    data = np.load(saveroot + files[cl][i][:-5] + method)
                    
                except NameError:
                    print 'ERROR: ' + files[cl][i]
                #data = extract(gmm, cl, i, saveroot)
                #data = np.load(saveroot + cl + '/image_%d_whole_ifv.npy')
                #np.save(saveroot + cl + '/image_%d_whole_ifv.npy', np.asarray(data,dtype='float32'))
          
                X.append(data)
                y.append(cx)
                
        #X = np.asfarray(np.vstack(X).reshape((320, 400, 64)),dtype='f')
        y = np.array(y,dtype='int32')
        
        #metric = DistanceMetric.get_metric('haversine')
        clf = KNeighborsClassifier(n_neighbors, weights='distance', algorithm= 'auto', metric = 'euclidean')
        clf.fit(X, y)
        
        sm = 0
        #hists = []
        for cx, cl in enumerate(test_classes):
            #print('classifying class ' + cl)
            acc = []
            
            #hist = np.zeros(len(classes))
            for i in range(N, TOTAL):
                #data = extract(gmm, cl, i, saveroot)
                #data = np.load(saveroot + str(cl) + '/image_%d_allparts_ifv.npy' % i)#.reshape(400,64)
                #data = np.load(saveroot + str(cl) + '/image_%d_features_N%s_C%s_HOG.npy' % (i, N, C))
                #data = np.load(saveroot + str(cl) + '/image_%d_features_SIFT.npy' % (i))
                #data = np.load(saveroot + str(cl) + '/image_%d_features_obj.npy' % (i))
                data = np.load(saveroot + files[cl][i][:-5] + method)
                prediction =  clf.predict(data)
                #hist[prediction] += 1
                #print zip(classes, np.histogram(prediction, bins=range(len(classes) + 1))[0])
                acc.append(prediction == zclasses[cl])
            #hist /= 25
                
            #print zip(classes, hist)
            #hists.append(hist)
            sm += sum(acc)
            accuracies[cl].append(100 * (sum(acc) / float(TOTAL - N)))
            latex += ' & %4.2f' % accuracies[cl][-1]
            #print('accuracy for ' + str(cl) + ' = ' + str(sum(acc) / (60. - N)))
        
        accuracies['all'].append(100 * (sm/(float(TOTAL - N) * 8)))
        latex += ' & %4.2f \\\\\n' % accuracies['all'][-1]
        
    #hists = np.vstack(hists)
    #confusion_mat(hists, classes)
    texfile = open('latex_all_splits_N%d.tex' % N,'w')
    texfile.write(latex)
    texfile.close()
    
    '''
    fig = pyplot.figure(1)
    #pyplot.axis([0, 8,0,8])

    cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap', ['white','black'], 256)
    img = pyplot.imshow(hists, interpolation='nearest', cmap = cmap, origin='upper')
    pyplot.colorbar(img, cmap = cmap)
    
    pyplot.show()
    '''
    
    #65% for MR8 - IFV
    #71% for SIFT - IFV
    #45 % for MR8- IFV (Whole Image)
    #print('overall ' + str(sm/(float(60. - N) * 8)))
    
    latex = '%d & ' % N
    for cl in classes:
        latex += '%4.1f$\pm %4.1f$ & ' % (np.mean(accuracies[cl]), np.std(accuracies[cl]))
    latex += '\\textbf{%4.1f$\pm %4.1f$} \\\\' % (np.mean(accuracies['all']), np.std(accuracies['all']))
    print latex
    #print accuracies['all']

if __name__ == '__main__':
    import multiprocessing
    #test_classes = [['fleece', 'fur'], ['corduroy', 'denim'],['knit1', 'leather'], ['boucle', 'lace']]
    test_classes = ['fleece', 'fur', 'corduroy', 'denim','knit1', 'leather', 'boucle', 'lace']  #

    jobs = []
    N = 35
    C = '1'
    worker(N, C, test_classes)
    #for test in test_classes:
    #    p = multiprocessing.Process(target=worker,args=(N, C, test))
    #    jobs.append(p)
    #    p.start()