# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 11:23:35 2013

@author: phan
"""
from PIL import Image
import numpy as np
from random import shuffle
from PyQt4.QtGui import QPixmap
import os 
import re

class ProbError(Exception):
    def __init__(self, value):
        self.value = value
        
    def __str__(self):
        return 'Probabilities must sum to 1 ' + repr(self.value)
        
class LazyList(object):
    def __init__(self,n_samples,retriever,urls,lazy):
        self.i = 0
        self.n = n_samples
        self.urls = urls
        self.retriever = retriever
        self._data = []
        if lazy == False:
            for url in urls:
                self._data.append(self.retriever.get(url))
        self.lazy = lazy
        
    def __iter__(self):
        return self
        
    def next(self):
        if self.i < self.n:
            self.i += 1
            if self.lazy:
                im =  self.retriever.get(self.urls[self.i-1])
                #self._data.append(im)
                return im
            else:
                return self._data[self.i - 1]
        else:
            self.i = 0
            #self.lazy = False
            raise StopIteration()
            
    def curr_index(self):
        return self.i - 1
        
    def curr_url(self):
        return self.urls[self.i-1]
        
    def back(self):
        if self.i > 1:
            self.i -= 1
        else:
            self.i = 0
        return self._data[self.i-1]            
            
    def reset(self):
        self.i = 0
        
    def __len__(self):
        return len(self.urls)
        
    def shuffle(self):
        for d in self._data:
            shuffle(d)
            
    def data(self):
        if len(self._data) == 0:
            for i in self:
                self._data.append(i)
            self.lazy = False
        return self._data
        
class Dataset(object):
    '''
    n_samples: a list or a number
    '''
    def __init__(self,classes, n_samples,repo,retriever,lazy=True):
        self.repo = repo
        self.retriever = retriever
        self.n_samples = {}
        self.n_classes = len(classes)
        self.classes = classes
        self.lazy = lazy
        self.i = 0
        self.n = len(classes)
        
        #assert len(classes) < repo.n_classes
        if isinstance(n_samples, list) and len(n_samples) == repo.n_classes:
            for nx, n in enumerate(n_samples):
                self.n_samples[classes[nx]] = n
                
        elif isinstance(n_samples, int):
            for c in classes:
                self.n_samples[c] = n_samples

        self.data = {}
        if repo is not None and retriever is not None:
            for c in classes:
                if c not in self.n_samples:
                    self.n_samples[c] = len(repo[c])                
                self.data[c] = LazyList(self.n_samples[c],retriever,repo[c],lazy)

        self.shape = self.n_samples.values()
            
    def __getitem__(self,key):
        if isinstance(key, str):
            return self.data[key]
        elif isinstance(key, int):
            return self.data[self.classes[key]]
        
    def next(self):
        if self.i < self.n:
            self.i += 1
            return self.classes[self.i-1]
        else:
            self.i = 0
            raise StopIteration()
      
    def __iter__(self):
        return self
        
    def __len__(self):
        return len(self.classes)
        
    def size(self):
        return sum(self.n_samples.values())

    def shuffle(self):
        self.repo.shuffle()
        for c in self.classes:
            self.data[c] = LazyList(self.n_samples[c],self.retriever,self.repo[c],self.lazy)
    
    def split(self,props=[0.5,0.5]):
        '''
        works on 2 modes: percentage and absolute.
        the absolute mode assume that all classes have the same number of 
        samples.
        '''
        #list of datasets
        dss = []
        props = np.array(props)
        if (props < 1).any():
            if sum(props) != 1: raise ProbError(props)
                
        curr = np.zeros(len(self.classes),dtype=np.int32)
        for px, p in enumerate(props):
            ls = []
            for k,v in self.n_samples.items():
                if (props < 1).any():
                    l = math.trunc(v * p)
                else:
                    l = p
                ls.append(l)
            rep = self.repo.subset(curr,curr + ls,1)
            curr += np.array(ls)
            dss.append(Dataset(self.classes, ls, rep, self.retriever))
            
        if len(props) == 2:
            return (dss[0],dss[1])
        else:
            return dss

class DataRepo(object):
    def __init__(self,root,classes):
        self.root = root
        #assert isinstance(classes,dict)
        self.classes = classes
        self.n_classes = len(classes)
        self.data = {}
        
    def __getitem__(self,key):
        raise NotImplementedError()
        #return np.array([])
        
    def shuffle(self):
        for c in self.classes:
            shuffle(self.data[c])
            
    def subset(self,ubs,lbs,steps=1):
        raise NotImplementedError()
        
class DirRepo(DataRepo):
    def __init__(self,root,classes,pattern=None,sort_key=None):
        super(DirRepo,self).__init__(root,classes)
        #(image_[0-9]+_[0-9]+.jpg)
        if pattern is None:
            pattern = re.compile(r'(.jpg)|(.jpeg)|(.JPG)|(.png)|(.bmp)|(.tiff)|(.pgm)')
            
        for cx,c in enumerate(classes):
            path = root + c + "/"
            exts = ['.png', '.jpg','.pgm']
            files = [f for f in sorted(os.listdir(path)) if any([f.endswith(e) for e in exts])]
            #files = sorted([f for f in os.listdir(path) if pattern.match(f) is not None], key = sort_key)
            for fx,f in enumerate(files):
                files[fx] = path + f
            self.data[c] = files

    def __getitem__(self,key):
        if isinstance(key, str):
            return self.data[key]
        elif isinstance(key, int):
            return self.data[self.classes[key]]
        
    def subset(self,ubs,lbs,steps=1):
        rep = DirRepo(self.root,self.classes)
        
        for cx, c in enumerate(self.classes):
            rep.data[c][:] = self.data[c][ubs[cx] : lbs[cx] :steps]
        return rep
    
class Retriever(object):
    def __init__(self,url=''):
        self.url = url
        
    def get(self,url=''):
        raise NotImplementedError()

class ImageRetriever(Retriever):
    def __init__(self,color_mode, normalized = True, url=''):
        super(ImageRetriever,self).__init__(url)
        self.color_mode = color_mode
        self.normalized = normalized

    def get(self,url=''):
        if url != '':
            self.url = url
        if self.normalized:
            image = np.array(Image.open(self.url).convert(self.color_mode)) / 255.
            image = (image - image.mean()) / image.std()
        else:
            image = np.array(Image.open(self.url).convert(self.color_mode))
        return image

class QtImageRetriever(Retriever):
    def __init__(self):
        super(QtImageRetriever,self).__init__('')

    def get(self,url=''):
        image = QPixmap(url)
        return image
        
if __name__=='__main__':
    classes = ['corduroy','aluminium_foil','sponge']
    repo = DirRepo('/home/phan/workspace/climaz/data/KTH_TIPS/all/',classes)
    retrv = ImageRetriever('L',normalized=False)
    n_samples = [12,12,12]
    ds = Dataset(classes,n_samples,repo,retrv,True)
    dss = ds.split([7,5])
    for ix,i in enumerate(dss[1]['corduroy']):
        print ix