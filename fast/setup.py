# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 00:50:28 2013

@author: phan
"""
from distutils.core import setup
from Cython.Build import cythonize

setup(
    name = "fast",
    ext_modules = cythonize('*.pyx'),
)
