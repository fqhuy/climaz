# -*- coding: utf-8 -*-

import numpy as np
import histogram as h

features = np.random.rand(100,20)
textons = np.random.rand(10,20)

hist = h.calc_histogram(features, textons, 2)
print hist