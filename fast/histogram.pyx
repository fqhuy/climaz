import numpy as np
cimport numpy as np
from cython.operator cimport dereference as deref, preincrement as inc
import sys
from libc.math cimport abs,pow

DTYPE = np.double
ctypedef np.double_t DTYPE_t

cpdef np.ndarray[np.int_t,ndim=1] calc_histogram(np.ndarray[DTYPE_t,ndim=2] features, \
np.ndarray[DTYPE_t,ndim=2] textons,int order):
    cdef int f_length = features.shape[1]
    cdef int n_features = features.shape[0]
    cdef int n_textons = textons.shape[0]
    cdef int fx, ix, idx, i
    cdef double mx, norm
    cdef np.ndarray[np.int_t,ndim=1] limage = np.zeros(n_features,dtype=np.int)
    cdef np.ndarray[np.double_t,ndim=1] hist = np.zeros(n_textons)
    
    for fx in range(n_features):
        mx = sys.maxint
        ix = 0
        for idx in range(n_textons):
            #calculate norm
            norm = 0
            for i in range(f_length):
                norm += pow(abs(features[fx,i] - textons[idx,i]),order)
            norm = pow(norm,1./order)
            #norm = np.linalg.norm(f - texton,1)
            if mx > norm:
                mx = norm
                ix = idx + 1
        limage[fx] = ix
    
    hist,_ = np.histogram(limage,range(1, n_textons + 2 ),density=True)
    return hist
    
