# -*- coding: utf-8 -*-
"""
Created on Mon Mar 24 12:05:24 2014

@author: phan
"""
import numpy as np
from scipy.spatial import distance as d

if __name__ == '__main__':
    from matplotlib import pyplot as plt
    root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/'
    saveroot = root[:-1] + '_OUTPUT/'    
    #queries = zip(['denim'] * 2, range(2) )#[('denim' , 0), ('denim' , 1) ]
    #classes = ['fleece', 'corduroy', 'denim']
    classes = ['fleece', 'fur', 'corduroy', 'denim','knit1', 'leather', 'boucle', 'lace']
    test_classes = ['fleece', 'fur', 'corduroy', 'denim','knit1', 'leather', 'boucle', 'lace']
    database = []
    N = 35
    C = 1
    split = 35
    takes = range(10, 101, 10)
    for cc in classes:
        for i in range(0, split):
            feat = np.load(saveroot + cc + '/image_%d_features_N%s_C%s.npy' % (i, N, C) )
            #feat = np.load(saveroot + cc + '/image_%d_allparts_ifv.npy' % i)
            database.append((cc, i, feat))
    allrecall = 0.0
    
    allrecalls = {}
    plt.title('Recall versus Number of Results')
    plt.grid(True)
    plt.axis([10, 100,0,1])
    plt.xlabel('Take')
    plt.ylabel('Recall')
    
    averecall = np.zeros(len(takes),dtype='float64')
    styles = ['^:', 'p:', '*:','x:','^-.','p-.','*-.','x-.']
    for testx, test in enumerate(test_classes):
        recalls = []
        precisions = []
        for tx, take in enumerate(takes):
            recall = 0
            precision = 0
            for query in range(split, 60):
                feat = np.load(saveroot + test + '/image_%d_features_N%s_C%s.npy' % (query, N, C))
                #feat = np.load(saveroot + test + '/image_%d_allparts_ifv.npy' % i)
                results = []
                for cc, i, item_feat in database:
                    #if len(item_feat) == len(feat):
                    #results.append((cc, feat.dot(item_feat) / (np.linalg.norm(feat) * np.linalg.norm(item_feat))))
                    results.append((cc, feat.dot(item_feat) ))
                    #results.append((cc, d.euclidean(feat, item_feat) ))
                    
                    #else:
                    #    print 'matrices not aligned.' + cc  + '/image ' + str(i)
                #print results
                results = sorted(results, key = lambda x: x[1])[-take:]
                corrects = sum([re[0] == test for re in results])
                recall += corrects / float(split)
                precision += corrects / float(take)
                
            allrecall += recall
            #if take == split:
            averecall[tx] += recall /  float(60 - split)
                
            recalls.append(recall /  float(60 - split))
            precisions.append(precision / float(60 - split) )
            print 'test ' + test + ', n_query = ' + str(60 - split) + ', recall ' + str(recall / float(60 - split))
        
        allrecalls['test'] = recalls
        plt.plot(takes, recalls, styles[testx],linewidth=2,markersize=8, label = test)
        
    for tx, t in enumerate(takes):
        averecall[tx] = averecall[tx] / float(len(test_classes))
        print "average recall at take %s = " % t + str(averecall[tx])        
        
    plt.legend(loc=4)
    plt.show()
        #print 'class ' + test + ' query ' + str(query) + ': ' + str(corrects)
    
    #print 'average recall: ' + str(allrecall / ((60 - split) * 8.) )
    