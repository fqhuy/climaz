# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 15:24:17 2013

@author: phan
"""

print(__doc__)

# Author: Ron Weiss <ronweiss@gmail.com>, Gael Varoquaux
# License: BSD 3 clause

# $Id$

import pylab as plb
import matplotlib as mpl
import matplotlib.pyplot as pl
import numpy as np

from sklearn import datasets
from sklearn.cross_validation import StratifiedKFold
from sklearn.externals.six.moves import xrange
from sklearn.mixture import GMM
from sklearn import mixture

def generate_gauss(n_samples,rect=(0,0,40,40)):
    import time
    np.random.seed(int(time.time()))
    while(True):
        shift = np.random.rand(2) * [rect[2] - \
        rect[0],rect[3]-rect[1]] + [rect[0],rect[1]]
        #C = np.array([[-0.0, -0.7], [2.5, 0.7]])
        C = np.array([[0, 2], [2, 0]])        
        yield np.dot(np.random.randn(n_samples, 2), C) * 1 + shift        

def generate_gmm(n_com,n_samples,rect=(0,0,40,40)):
    g = generate_gauss(n_samples,rect)
    while(True):
        yield np.vstack([next(g) for _ in range(n_com)])

def generate_n_gmms(n_com, n_scom, n_samples):
    # np.random.multinomial(20, [1/float(n_com)]*n_com, size=1)
    rect = (0,0,40,40)
    g = generate_gmm(n_scom-1,n_samples,rect)
    ga = generate_gauss(n_samples,rect)
    
    ga1 = next(ga)
    for i in range(8):

        #X_train = np.vstack(next(g))
        X_train = np.vstack([next(g),ga1])
        clf = mixture.GMM(n_components=4, covariance_type='diag')
        clf.fit(X_train)
        
        fg = pl.figure(i)
        
        x = np.linspace(rect[0],rect[2])
        y = np.linspace(rect[1],rect[3])
        X, Y = np.meshgrid(x, y)
        XX = np.c_[X.ravel(), Y.ravel()]
        Z = np.log(-clf.score_samples(XX)[0])
        Z = Z.reshape(X.shape)
        
        CS = pl.contour(X, Y, Z)
        CB = pl.colorbar(CS, shrink=0.8, extend='both')
        pl.scatter(X_train[:, 0], X_train[:, 1], .8)
        
        pl.axis('tight')
        pl.show()
    
if __name__=="__main__":
    generate_n_gmms(10,4,100)